import traceback
import typing

from flask import current_app, flash, has_request_context
from flask_babel import gettext
from sqlalchemy import event, inspect
from sqlalchemy.orm import Session, attributes, scoped_session, sessionmaker
from sqlalchemy.orm.exc import ObjectDeletedError
from flask_sqlalchemy.session import Session as Flask_Sqlalchemy_Session

from .models.dicom import Project, Group, Server, Principal, Tag, Destination, Attribute
from .models.screening import ScreeningForm
from .models.credentials import GroupMembership, UserMembership
from .tasks import schedule_next_expiry
from .decorators import guard

if typing.TYPE_CHECKING:
    from sqlalchemy.orm import SessionTransaction


# noinspection PyUnusedLocal
@event.listens_for(Session, "after_transaction_create")
def after_transaction_create(session: Session, transaction: "SessionTransaction"):
    # Create the synchronization object for the transaction
    setattr(transaction, "sync", Synchronize())


# noinspection PyUnusedLocal,PyUnresolvedReferences
@event.listens_for(Session, "after_transaction_end")
def after_transaction_end(session: Session, transaction: "SessionTransaction"):
    # after the transaction ends all outstanding data must be synchronized
    # This should be none because everything should have been rolled back or synchronized after commit already
    if hasattr(transaction, "sync") and not transaction.sync.empty:
        current_app.logger.error(gettext("sync not empty after transaction end"))
        if transaction.parent:
            transaction.parent.sync.merge(transaction.sync)
        else:
            transaction.sync.synchronize()


def sync_transaction(session: Session, clear=False, check=False) -> None:
    transaction = session.get_nested_transaction() or session.get_transaction()

    if transaction and hasattr(transaction, "sync"):
        if clear:
            transaction.sync.clear()
        elif check:
            transaction.sync.check(session)
        elif transaction.parent:
            # parent may roll back a nested transaction, so the required synchronize for the nested transaction needs
            # to be merged with the parent's changes for later synchronization or rollback.
            transaction.parent.sync.merge(transaction.sync)
        else:
            # root transaction actually performs synchronization
            transaction.sync.synchronize()


# noinspection PyUnusedLocal
@event.listens_for(Session, "before_flush")
def before_flush(session: Session, flush_context, instances):
    """Change versioned table modify to add"""
    for instance in session.dirty:
        # noinspection PyUnresolvedReferences
        if (
            isinstance(instance, ScreeningForm)
            and session.is_modified(instance)
            and attributes.instance_state(instance).has_identity
        ):
            # make it transient
            instance.new_version(session)

    # Check the session for anything that needs to be synchronized. This will include all outstanding changes in the
    # root transaction and all nested transactions. It is not necessary to limit the check to only changes within a
    # nested transaction because changes are only synchronized when committing on a root transaction. Although a nested
    # transaction may find changes performed in the parent transaction these will necessarily already exist in the
    # parent transaction and therefore can safely be merged with the parent transaction
    sync_transaction(session, check=True)


@event.listens_for(Session, "after_rollback")
def after_rollback(session: Session):
    # rollback means outstanding changes need to be cleared
    sync_transaction(session, clear=True)


# noinspection PyUnusedLocal
@event.listens_for(Session, "after_commit")
def after_commit(session: Session):
    # After a DB changes have been committed, they need to be synchronized
    # This is required because multiple commits and rollbacks may occur within a single transaction
    # so only synchronizing at the end of the transaction is not acceptable
    sync_transaction(session)


class Synchronize:
    _collections = ("_arc_update", "_arc_remove", "_ldap_update", "_ldap_remove")

    def __init__(self):
        self._arc_update = set()
        self._arc_remove = set()
        self._ldap_update = set()
        self._ldap_remove = set()
        self.schedule = False
        self._enabled = current_app.config.get("SYNCHRONIZE_WITH_LDAP", True)

    @property
    def active(self):
        for attr in self._collections:
            if getattr(self, attr):
                return True
        return False

    @property
    def empty(self):
        return not self.schedule and not self.active

    def clear(self):
        self.schedule = False
        for attr in self._collections:
            getattr(self, attr).clear()

    def merge(self, sync):
        self.schedule |= sync.schedule
        for attr in self._collections:
            getattr(self, attr).update(getattr(sync, attr))
        sync.clear()

    @staticmethod
    def has_attribute_changes(target, attrs):
        info = inspect(target)
        for attr in attrs:
            if (
                hasattr(info.attrs, attr)
                and getattr(info.attrs, attr).history.has_changes()
            ):
                return True
        return False

    def check(self, session: Session):
        if not self._enabled:
            return
        try:
            # Remove deleted items
            for target in session.deleted:
                if isinstance(target, (Principal, Project, Server, Destination)):
                    self._arc_remove.add(target)
                elif isinstance(target, (Group,)):
                    self._ldap_remove.add(target)
                elif isinstance(target, (GroupMembership, UserMembership)):
                    try:
                        self._ldap_update.add(target.group)
                    except ObjectDeletedError:
                        pass

            # Need to create new groups in LDAP before processing other models
            for target in session.new:
                if isinstance(target, (Group,)):
                    self._ldap_update.add(target)

                    self._arc_update.add(target.project)
                    self._arc_update.add(target.principal)

                elif isinstance(target, (Principal, Project, Server, Destination)):
                    self._arc_update.add(target)
                elif isinstance(target, (UserMembership, GroupMembership)):
                    if target.expires:
                        self.schedule = True

                    self._ldap_update.add(
                        target.group or session.get(Group, target.group_id)
                    )

            # Process updates to models
            for target in session.dirty:
                if not session.is_modified(target):
                    continue

                if isinstance(target, Group) and self.has_attribute_changes(
                    target,
                    (
                        "id",
                        "name",
                        "description",
                        "project",
                        "principal",
                        "user_member",
                        "group_member",
                    ),
                ):
                    self._ldap_update.add(target)

                    # Needed on update
                    if self.has_attribute_changes(target, ("id", "name")):
                        self._arc_update.add(target.project)
                        self._arc_update.add(target.principal)
                elif isinstance(target, (UserMembership, GroupMembership)):
                    if target.expires:
                        self.schedule = True
                    if self.has_attribute_changes(
                        target, ("level", "expires", "member_id", "group_id")
                    ):
                        self._ldap_update.add(target.group)
                elif isinstance(target, Principal):
                    if self.has_attribute_changes(target, ("user_id", "group_id")):
                        self._ldap_update.add(target.group)

                    if self.has_attribute_changes(target, ("id", "name", "group_id")):
                        self._arc_update.add(target)

                elif isinstance(target, Project):
                    if self.has_attribute_changes(target, ("principal_id", "group_id")):
                        self._ldap_update.add(target.group)

                    if self.has_attribute_changes(
                        target, ("id", "name", "study", "attributes")
                    ):
                        self._arc_update.add(target)
                        for destination in target.destinations:
                            self._arc_update.add(destination)

                elif isinstance(target, Destination):
                    if self.has_attribute_changes(
                        target, ("delay", "server_id", "project_id")
                    ):
                        self._arc_update.add(target)

                elif isinstance(target, Attribute):
                    self._arc_update.add(target.project)

                elif isinstance(target, Tag):
                    for attribute in target.attributes:
                        self._arc_update.add(attribute.project)

                elif isinstance(target, Server):
                    if self.has_attribute_changes(
                        target,
                        ("aet", "host", "port", "cipher", "description", "installed"),
                    ):
                        self._arc_update.add(target)
                        for destination in target.destinations:
                            self._arc_update.add(destination)

        except Exception as e:
            guard()
            current_app.logger.critical(
                gettext(
                    "Failed during synchronization setup with LDAP and DCM4CHEE: {error}\n{tb}"
                ).format(error=str(e), tb=traceback.format_exc())
            )
            guard()

    def synchronize(self):
        if not self._enabled:
            return
        if self.active or self.schedule:
            db = current_app.extensions["sqlalchemy"]
            original_session = db.session

            def create_scoped_session(scope_name):
                factory = sessionmaker(
                    db=db, class_=Flask_Sqlalchemy_Session, query_cls=db.Query
                )
                return scoped_session(factory, lambda: scope_name)

            try:
                # Ensure any queries generated by synchronize actions use a separate session
                db.session = create_scoped_session("synchronize")

                if self.active:
                    try:
                        with db.session() as session:
                            targets = self._ldap_remove.copy()

                            for target in targets:
                                target.remove()
                                self._ldap_remove.remove(target)
                                # Removed target should not be updated
                                try:
                                    self._ldap_update.remove(target)
                                except KeyError:
                                    pass

                            targets = self._ldap_update.copy()
                            for target in targets:
                                if target is None:
                                    continue

                                # Perform synchronize action on an ORM object queried using a separate session
                                target_new_session = session.get(
                                    type(target), target.id
                                )
                                try:
                                    target_new_session.update()
                                    self._ldap_update.remove(target)
                                except (AttributeError, KeyError):
                                    current_app.logger.debug(
                                        f"Failed to find {target} in {self._ldap_update}"
                                    )
                                    current_app.logger.error(
                                        f"Traceback: {traceback.format_exc()}"
                                    )
                                    raise

                            # Re-query the objects in arc update set using separate session before using them
                            # Use the original session for removing arc objects, relying on `ArcUpdate` to not
                            # trigger a database query when accessing any attributes
                            current_app.extensions["arc"].update(
                                {
                                    session.get(type(v), v.id)
                                    for v in self._arc_update
                                    if v
                                },
                                self._arc_remove,
                            )
                            self._arc_update.clear()
                            self._arc_remove.clear()
                    except Exception as e:
                        error_message = gettext(
                            "Failed during synchronization with LDAP and DCM4CHEE"
                        )
                        current_app.logger.critical(
                            f"{error_message}: {str(e)}\n{traceback.format_exc()}"
                        )
                        if has_request_context():
                            flash(error_message, "error")
                        guard()

                if self.schedule:
                    self.schedule = False
                    schedule_next_expiry()
            finally:
                db.session.close()
                db.session = original_session
