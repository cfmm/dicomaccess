import ssl
import traceback
import typing
from itertools import chain

import ldap3
import requests
from authlib.common.errors import AuthlibBaseError
from authlib.integrations.requests_client import OAuth2Auth, OAuthError
from flask import current_app, flash, has_request_context
from flask_babel import gettext
from ldap3 import LEVEL, SUBTREE, BASE
from ldap3.core.exceptions import (
    LDAPException,
    LDAPNoSuchObjectResult,
    LDAPNotAllowedOnNotLeafResult,
)

from .arc_records import Record
from .arc_schema import update_schema
from .models.dicom import Project, Principal, Server, Destination
from .decorators import guard
from .proxy import current_datastore


class ReloadError(Exception):
    pass


class ArcConfigurationError(Exception):
    pass


class ArcConfig:
    def __init__(self, config: dict) -> None:
        self.url = config["url"].strip("/")
        self.verify = config["verify"]
        self._token = None
        self.conn = None
        self.config = None

    def connect(self):
        did_configure = False
        if self.conn is None:
            self.configure()
            did_configure = True

        if not self.conn.bound:
            try:
                if not self.conn.bind():
                    raise LDAPException()
            except LDAPException:
                if not did_configure:
                    # Try to re-configure and then connect again
                    self.conn = None
                    return self.connect()
                raise

    def _get_config(self):
        url = self.url + "/mgmt/access"
        try:
            rsp = requests.get(
                url=url,
                verify=self.verify,
                auth=OAuth2Auth(self.token),
            )
            assert rsp.status_code == 200
        except (
            requests.exceptions.ConnectionError,
            AuthlibBaseError,
            AssertionError,
            OAuthError,
        ) as err:
            current_app.logger.critical(
                gettext("Failed get access information from {url}:\n{error}").format(
                    url=url,
                    error=err,
                )
            )
            raise ArcConfigurationError(
                gettext("Unable to obtain arc configuration from {url}").format(url=url)
            )

        return rsp.json()

    def configure(self):
        self.config = self._get_config()

        tls = ldap3.Tls(validate=ssl.CERT_REQUIRED)
        server = ldap3.Server(self.config["ldapUrl"], tls=tls)

        try:
            update_schema(
                server,
                user=self.config["schemaDN"],
                password=self.config["schemaPassword"],
            )
        except KeyError:
            pass

        # noinspection PyTypeChecker
        self.conn = ldap3.Connection(
            server,
            user=self.config["userDN"],
            password=self.config["userPassword"],
            client_strategy=ldap3.SAFE_RESTARTABLE,
            raise_exceptions=True,
            auto_bind=False,
        )

        if self.config.get("start_tls", False):
            self.conn.start_tls()

    @property
    def token(self):
        if self._token is None or self._token.is_expired():
            client = current_datastore.client

            if client is None:
                return None

            self._token = client.fetch_access_token(grant_type="client_credentials")

        return self._token

    @property
    def aetitle(self):
        entry = self.search(
            self.device_base,
            "(objectClass=dcmArchiveDevice)",
            ("dcmExternalRetrieveAEDestination",),
            BASE,
        )
        try:
            return entry[0]["attributes"]["dcmExternalRetrieveAEDestination"]
        except (TypeError, KeyError, IndexError):
            return "DCM4CHEE"

    @property
    def export_queue(self):
        entries = self.search(
            self.device_base,
            "(objectClass=dcmQueue)",
            ("dcmQueueName", "dicomDescription"),
            LEVEL,
        )
        try:
            # First check for any queue name Export
            for entry in entries:
                attrs = entry["attributes"]
                if "dcmQueueName" in attrs and attrs["dcmQueueName"] == "Export":
                    return "Export"

            # Then check for any queue with Export in the description
            for entry in entries:
                attrs = entry["attributes"]
                if (
                    "dicomDescription" in attrs
                    and "Export" in attrs["dicomDescription"]
                ):
                    return attrs["dcmQueueName"]

            # Use the name of the first available queue
            return entries[0]["attributes"]["dcmQueueName"]
        except (TypeError, KeyError, IndexError):
            return "Export"

    def reload(self):
        # POST to the reload URL endpoint
        url = self.url + "/ctrl/reload"
        current_app.logger.debug(
            gettext("Posting to {url} to apply modifications.").format(url=url)
        )
        try:
            resp = requests.post(
                url=url,
                verify=self.verify,
                auth=OAuth2Auth(self.token),
            )
            assert resp.status_code == 204
        except (
            requests.exceptions.ConnectionError,
            AuthlibBaseError,
            AssertionError,
            OAuthError,
        ) as err:
            current_app.logger.critical(
                gettext("Failed reload at {url}:\n{error}").format(
                    url=url,
                    error=err,
                )
            )
            raise ReloadError(gettext("Unable to reload configuration"))

    @property
    def configuration_base(self) -> str:
        if self.config is None:
            self.configure()
        return self.config["configurationDN"]

    @property
    def devices_base(self) -> str:
        if self.config is None:
            self.configure()
        return self.config["devicesDN"]

    @property
    def aets_registry_dn(self) -> str:
        if self.config is None:
            self.configure()
        return self.config["aetsRegistryDN"]

    @property
    def device_base(self) -> str:
        """
        Returns the LDAP base for access control rules

        :return:
        """
        if self.config is None:
            self.configure()
        return self.config["deviceDN"]

    def remove_entries(self, entries: list) -> bool:
        modified = False
        for entry in entries:
            while True:
                try:
                    self.conn.delete(entry["dn"])
                    break
                except LDAPNotAllowedOnNotLeafResult:
                    children = self.search(
                        search_base=entry["dn"],
                        search_filter="(objectClass=*)",
                        search_scope=LEVEL,
                    )
                    self.remove_entries(children)
                except LDAPNoSuchObjectResult:
                    break

            current_app.logger.debug(
                f'Deleted {entry["dn"]} from {self.conn.server.host}'
            )
            modified = True
        return modified

    def search(
        self,
        search_base: str,
        search_filter: str,
        attributes: typing.Sequence[str] = None,
        search_scope: typing.Literal["BASE", "LEVEL", "SUBTREE"] = SUBTREE,
    ) -> list:
        try:
            status, response, entries, _ = self.conn.search(
                search_base=search_base,
                search_filter=search_filter,
                search_scope=search_scope,
                attributes=attributes,
            )
        except ldap3.core.exceptions.LDAPNoSuchObjectResult:
            return []

        if response["type"] != "searchResDone":
            current_app.logger.error(
                f"Invalid result for search of {search_base} for {search_filter} on {self.conn.server.host}"
            )
            raise LDAPException

        return entries

    def purge(self) -> bool:
        self.connect()
        current_app.logger.debug(f"Purging records from server {self.conn.server.host}")
        modified = False
        try:
            # Query for all management records
            entries = self.search(
                search_base=self.configuration_base,
                search_filter="(objectClass=mgmtRecord)",
                attributes=ldap3.ALL_ATTRIBUTES,
            )

            # Updating will remove entries, so all remaining entries need to be deleted
            modified |= self.remove_entries(entries)

        except LDAPException as err:
            current_app.logger.error(
                gettext("Error purging {server}: {err}").format(
                    server=self.conn.server.host, err=err
                )
            )
            raise

        current_app.logger.debug(
            f"Purging completed with server {self.conn.server.host}"
        )

        # Reload arc if necessary
        if modified:
            self.reload()

        return modified

    def sync(self) -> bool:
        self.connect()
        current_app.logger.debug(
            f"Synchronizing started with server {self.conn.server.host}"
        )
        modified = False
        try:
            # Query for all management records
            entries = self.search(
                search_base=self.configuration_base,
                search_filter="(objectClass=mgmtRecord)",
                attributes=ldap3.ALL_ATTRIBUTES,
            )

            for model in chain(
                Project.query.all(),
                Principal.query.all(),
                Server.query.all(),
                Destination.query.all(),
            ):
                self.update(model, entries=entries)

            # Updating will remove entries, so all remaining entries need to be deleted
            modified |= self.remove_entries(entries)

        except LDAPException as err:
            current_app.logger.error(
                gettext("Error synchronizing with {server}: {err}").format(
                    server=self.conn.server.host, err=err
                )
            )
            raise

        current_app.logger.debug(
            f"Synchronizing completed with server {self.conn.server.host}"
        )

        # Reload arc if necessary
        if modified:
            self.reload()

        return modified

    def update(
        self,
        model,
        remove: bool = False,
        entries: typing.Union[typing.List, None] = None,
    ) -> bool:
        record: Record = model.mgmt_record(self)
        if remove or not record.valid:
            return record.remove()
        return record.update(entries)


class ArcUpdate:
    servers: typing.List[ArcConfig]

    def __init__(self, config) -> None:
        if isinstance(config, list):
            self.servers = [ArcConfig(x) for x in config]
        elif config:
            self.servers = [ArcConfig(config)]
        else:
            self.servers = []

        self.enable = len(self.servers) > 0

    @staticmethod
    def flash(*args, **kwargs):
        if has_request_context():
            flash(*args, **kwargs)

    def update(
        self,
        update: typing.Iterable = None,
        remove: typing.Iterable = None,
    ) -> None:
        # Return if no models to update
        if not update and not remove:
            return
        update = update or tuple()
        remove = remove or tuple()

        for server in self.servers:
            model = None
            try:
                server.connect()

                modified = False
                for model in update:
                    modified |= server.update(model, remove=False)

                for model in remove:
                    modified |= server.update(model, remove=True)

                # Reload arc server if necessary
                if modified:
                    server.reload()
            except AttributeError as err:
                # Tried to update a model that did not have mgmt_record, ignore
                if not str(err).endswith("'mgmt_record'"):
                    raise
            except LDAPException as err:
                current_app.logger.error(
                    gettext(
                        'LDAP error updating {model}: {error}. Traceback: {traceback}"'
                    ).format(
                        model=str(model),
                        error=str(err),
                        traceback=traceback.format_exc(),
                    )
                )
                self.flash(
                    gettext(
                        "An LDAP exception occurred when attempting to sync configuration with the DICOM archive. "
                        "Changes have not been propagated to DICOM server {server}"
                    ).format(server=server.config["ldapUrl"]),
                    "error",
                )
                guard()
            except ReloadError:
                self.flash(
                    gettext(
                        "Configuration reload failed. Changes have not been propagated to DICOM server {server}"
                    ).format(server=server.config["ldapUrl"]),
                    "error",
                )
            except ArcConfigurationError as err:
                current_app.logger.info(str(err))
                self.flash(
                    gettext("Unable to sync data with {server}").format(
                        server=server.url
                    ),
                    "error",
                )

    def sync(self) -> bool:
        modified = False
        for server in self.servers:
            modified |= server.sync()
        return modified

    def purge(self) -> bool:
        modified = False
        for server in self.servers:
            modified |= server.purge()
        return modified

    def reload(self):
        for server in self.servers:
            server.reload()
