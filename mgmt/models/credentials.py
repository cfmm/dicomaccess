import enum
from functools import cached_property
from itertools import chain

import typing
from flask import current_app
from flask_babel import gettext
from flask_cfmm import UserMixin, RoleMixin
from flask_cfmm.models import DateTracking
from flask_cfmm.configure import get_role
from flask_cfmm.security import create_user
from sqlalchemy import Enum
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped
from tzlocal import get_localzone

from . import db
from . import dicom
from .tokens import TokenMixin
from ..proxy import current_ldap
from ..exceptions import UsernameExists, EmailExists, LdapNoMatchingEntry


class UserRole(db.Model, RoleMixin):
    pass


def _config():
    return current_app.config["USERMGMT"]


class MembershipType(enum.Enum):
    full = 0x01
    coordinator = 0x02

    def str(self):
        if self._value_ == 0x01:
            return "Full"
        elif self._value_ == 0x02:
            return "Coordinator"


class Membership:
    expires: Mapped[db.DateTime] = db.Column(
        "expires",
        db.DateTime,
        nullable=True,
    )

    @classmethod
    def expiry_clause(cls):
        return db.or_(cls.expires.is_(None), cls.expires > db.func.now())


class UserMembership(Membership, db.Model):
    __tablename__ = "user_membership"
    __table_args__ = (
        db.UniqueConstraint("member_id", "group_id"),
        {
            "mysql_collate": "utf8_general_ci",
            "mysql_charset": "utf8",
        },
    )
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    member_id = db.Column(
        "member_id",
        db.Integer,
        db.ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    group_id = db.Column(
        "group_id",
        db.Integer,
        db.ForeignKey("group.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    warned = db.Column(
        "warned", db.DateTime, nullable=False, server_default=db.func.now()
    )
    level = db.Column(
        "level",
        Enum(MembershipType, name="user_membership_level"),
        nullable=False,
        default=MembershipType.full,
        server_default=MembershipType.full.name,
    )
    member = db.relationship(
        "User",
        back_populates="member_group",
        overlaps="users",
    )
    group = db.relationship(
        "Group",
        back_populates="user_member",
        overlaps="users",
    )

    @classmethod
    def member_of(cls, user, level: typing.Union[MembershipType, None] = None):
        # Non-recursive SELECT for group.id AS member_of for all groups which a user is an active member
        level = level or MembershipType.full
        query = (
            cls.query.with_entities(cls.group_id.label("member_of"))
            .filter(cls.member_id == user.id)
            .filter(cls.expiry_clause())
            .filter(cls.level <= level)
        )

        # There is an implicit UserMembership for the member_id = Principal.user_id to group_id = Principal.group_id
        # with MembershipType,full and no expiry
        # SELECT principal.group_id AS member_of FROM principal WHERE principal.user_id = user.id
        return query.union(
            dicom.Principal.query.with_entities(
                dicom.Principal.group_id.label("member_of")
            ).filter(dicom.Principal.user_id == user.id)
        )


class GroupMembership(Membership, db.Model):
    __tablename__ = "group_membership"
    __table_args__ = (
        db.UniqueConstraint("member_id", "group_id"),
        {
            "mysql_collate": "utf8_general_ci",
            "mysql_charset": "utf8",
        },
    )
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    member_id = db.Column(
        "member_id",
        db.Integer,
        db.ForeignKey("group.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    group_id = db.Column(
        "group_id",
        db.Integer,
        db.ForeignKey("group.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    warned = db.Column(
        "warned", db.DateTime, nullable=False, server_default=db.func.now()
    )
    member = db.relationship(
        "Group",
        back_populates="member_group",
        foreign_keys="GroupMembership.member_id",
        overlaps="groups",
    )
    group = db.relationship(
        "Group",
        back_populates="group_member",
        foreign_keys="GroupMembership.group_id",
        overlaps="groups",
    )

    @db.validates("member_id", "group_id")
    def validates_notself(self, key, value):
        if key == "group_id":
            group_id = value
            member_id = self.member_id
        else:
            group_id = self.group_id
            member_id = value

        if member_id == group_id:
            raise ValueError(gettext("Group cannot be member of self"))
        return value

    @classmethod
    def member_of(cls, group_query):
        """
        Recursive SELECT for group.id of all groups any group in group_query is a member

        :param group_query: Query or iterable that returns the group.id of group for which member of is required
        :return: SELECT statement with member_of being the group.id
        """

        """
        https://dev.mysql.com/blog-archive/mysql-8-0-1-recursive-common-table-expressions-in-mysql-ctes-part-four-depth-first-or-breadth-first-traversal-transitive-closure-cycle-avoidance/

        WITH RECURSIVE all_groups AS (
            SELECT group_id AS member_of
            FROM {tablename}
            WHERE member_id IN ({group_query})
            AND (expires IS NULL OR expires < NOW())
        UNION DISTINCT
            SELECT g.group_id AS member_of
            FROM {tablename} g, all_groups a
            WHERE g.member_id = a.member_of
            AND (g.expires IS NULL OR g.expires < NOW())
            )
        SELECT member_of FROM all_groups
        """

        # There are implicit GroupMemberships with member_id = Principal.group_id and group_id = Project.group_id
        # SELECT principal.group_id AS member_id, project.group_id AS group_id, NULL AS expires
        # FROM project JOIN principal ON project.principal_id = principal.id
        implicit = dicom.Project.query.join(dicom.Principal).with_entities(
            dicom.Principal.group_id.label("member_id"),
            dicom.Project.group_id.label("group_id"),
        )

        # Limit to only active memberships
        expire_clause = cls.expiry_clause()

        # Limit explicit to memberships that have not expired
        explicit = cls.query.with_entities(
            cls.member_id.label("member_id"),
            cls.group_id.label("group_id"),
        ).filter(expire_clause)

        # Construct a union from the explicit and implicit GroupMembership
        all_memberships = explicit.union(implicit).subquery()

        # Select only the group_id from the combined memberships
        base = db.select(all_memberships.c.group_id.label("member_of"))

        # Recursive CTE with anchor including all groups for which membership is desired
        base_cte = base.filter(all_memberships.c.member_id.in_(group_query)).cte(
            "all_groups", recursive=True
        )

        # Recursive query adds all groups which anchor groups are active members
        recursive = base.filter(all_memberships.c.member_id == base_cte.c.member_of)

        # Return a select on union (UNION means UNION DISTINCT, i.e. DISTINCT is default for UNION)
        return base_cte.union(recursive).select()


class User(DateTracking, UserMixin, TokenMixin, db.Model):
    """
    User object for locally stored attributes
    """

    email = db.Column("email", db.String(320), nullable=True)
    first_name = db.Column("first_name", db.String(64), nullable=True)
    last_name = db.Column("last_name", db.String(64), nullable=True)

    task_name = db.synonym("username")
    membership = db.relationship(
        "Group", secondary=UserMembership.__table__.fullname, viewonly=True
    )

    servers = db.relationship("Server", back_populates="user", cascade_backrefs=False)
    operators = db.relationship(
        "Operator", back_populates="user", cascade_backrefs=False
    )

    member_group = db.relationship(
        "UserMembership",
        back_populates="member",
        overlaps="users",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
    )

    principals = db.relationship(
        "Principal", back_populates="user", cascade_backrefs=False
    )

    screening_forms_created = db.relationship(
        "ScreeningForm", back_populates="create_user", cascade_backrefs=False
    )

    #: Private property that stores LDAP information about the user
    info = dict()
    role_model = UserRole

    @property
    def timezone(self):
        return get_localzone()

    @property
    def identifier(self):
        config = _config()
        prefix = config.get("prefix")
        user = config.get("user_prefix")
        return f"{prefix}{user}{self.id}"

    @staticmethod
    def check_username(value):
        """
        Ensure username is not in use.

        :param value: string username
        :return: boolean
        """
        db_result = User.query.filter(User.username == value).one_or_none()

        if not db_result:
            return not current_ldap.user_exists(value)

        return False

    @staticmethod
    def find_by_dn(dn, create=False):
        if not current_ldap.is_user(dn):
            return None

        cn = current_ldap.extract_cn(dn)
        user = User.query.filter(User.username == cn).first()

        if not user and create:
            current_app.logger.debug(f"Creating user {cn}")
            user = create_user(username=cn)

        if user and user.dn != dn.lower():
            return None

        return user

    # noinspection PyUnusedLocal
    @db.validates("username")
    def validate_username(self, key, value):
        if self.username is not None and self.username != value:
            raise ValueError(gettext("Username cannot be changed"))
        return value

    @property
    def is_authenticated(self):
        return True

    def groups(self):
        """
        Return the groups of which user is a member

        :return: list
        """
        return (
            Group.query.filter(Group.id.in_(self.member_of()))
            .order_by(Group.name)
            .all()
        )

    @cached_property
    def ldap_attributes(self):
        try:
            entry = current_ldap.get_user_entry(
                self, ["cn", "sAMAccountName", "mail", "givenName", "sn"]
            )
        except LdapNoMatchingEntry:
            return dict()

        attrs = entry.get("attributes", dict())
        attrs = dict(
            (
                k,
                (
                    attrs[k][0]
                    if len(attrs[k]) == 1 and type(attrs[k]) is list
                    else attrs[k]
                ),
            )
            for k in attrs
        )
        attrs["dn"] = entry["dn"].lower()

        # Update the database from LDAP authority
        for a, b in [
            ("mail", "email"),
            ("givenName", "first_name"),
            ("sn", "last_name"),
        ]:
            value = attrs.get(a, None)
            # Only update on change to not trigger unnecessary synchronization
            if value and getattr(self, b) != value:
                setattr(self, b, value)

        return attrs

    @property
    def dn(self):
        return self.ldap_attributes.get("dn", None)

    @property
    def is_local(self):
        return self.ldap_attributes.get("local", False)

    @property
    def is_datacare(self):
        return self.has_role(get_role("datacare_role")) or self.is_admin

    @cached_property
    def is_coordinator(self):
        return (
            self.is_datacare
            or dicom.Project.accessible_to(
                self, level=MembershipType.coordinator
            ).count()
            > 0
        )

    @property
    def has_access(self):
        return super().has_access or self.is_datacare or self.is_coordinator

    @cached_property
    def has_projects(self):
        return self.is_datacare or dicom.Project.accessible_to(self).count() > 0

    def member_of(self, level=None):
        # groups that the user is a member of
        user_member_of = UserMembership.member_of(self, level)

        # return the union of the above groups and any memberships computed via GroupMembership table
        return GroupMembership.member_of(user_member_of).union(user_member_of)

    def is_active(self):
        """
        Check if the user is active

        Required for Flask-login.


        :return: boolean
        """
        return self.active

    def __str__(self):
        return str(self.username)

    def remove(self):
        current_ldap.remove(self)

    def update(self, *args, **kwargs):
        # TODO: The email address of a non-local user will be invalid when it becomes a local user
        try:
            current_ldap.make_user(self)
            if "ldap_attributes" in self.__dict__:
                del self.__dict__["ldap_attributes"]
        except UsernameExists:
            pass
        except EmailExists:
            current_app.logger.error(
                gettext(
                    "Cannot make {username}. Another user with the email address {mail} already exists"
                ).format(username=self.username, mail=self.email)
            )
            pass


class Group(DateTracking, db.Model):
    """
    Access group
    """

    __tablename__ = "group"
    #: Primary key id
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: current_app.extensions['ldap'].config['group_attribute'] for group in AD
    name = db.Column(
        "name",
        db.String(255).with_variant(
            db.String(255, collation="utf8_general_ci"), "mysql"
        ),
        index=True,
        unique=True,
        nullable=False,
    )
    task_name = db.synonym("name")

    #: Description
    description = db.Column(
        "description",
        db.String(4096),
        index=False,
        unique=False,
        nullable=True,
    )

    project = db.relationship(
        "Project",
        back_populates="group",
        uselist=False,
    )
    principal = db.relationship(
        "Principal",
        back_populates="group",
        uselist=False,
    )

    users = db.relationship(
        "User",
        secondary=UserMembership.__table__.fullname,
        primaryjoin=id == UserMembership.group_id,
        secondaryjoin=db.and_(
            User.id == UserMembership.member_id,
            UserMembership.expiry_clause(),
            User.active,
        ),
    )
    groups = db.relationship(
        "Group",
        secondary=GroupMembership.__table__.fullname,
        primaryjoin=id == GroupMembership.group_id,
        secondaryjoin=db.and_(
            id == GroupMembership.member_id, GroupMembership.expiry_clause()
        ),
    )
    membership = db.relationship(
        "Group",
        secondary=GroupMembership.__table__.fullname,
        primaryjoin=db.and_(
            id == GroupMembership.member_id, GroupMembership.expiry_clause()
        ),
        secondaryjoin=id == GroupMembership.group_id,
        viewonly=True,
    )

    user_member = db.relationship(
        UserMembership,
        back_populates="group",
        overlaps="users",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
    )
    member_group = db.relationship(
        GroupMembership,
        back_populates="member",
        foreign_keys=GroupMembership.member_id,
        overlaps="groups",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
    )

    group_member = db.relationship(
        GroupMembership,
        back_populates="group",
        foreign_keys=GroupMembership.group_id,
        overlaps="groups",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
    )

    @hybrid_property
    def is_dicom(self):
        return self.project is not None or self.principal is not None

    @is_dicom.inplace.expression
    def _is_dicom_expr(cls):
        query1 = (
            dicom.Project.query.with_entities(dicom.Project.group_id)
            .filter(dicom.Project.group_id == cls.id)
            .limit(1)
        )
        query2 = (
            dicom.Project.query.with_entities(dicom.Principal.group_id)
            .filter(dicom.Principal.group_id == cls.id)
            .limit(1)
        )
        return db.or_(db.exists(query1.subquery()), db.exists(query2.subquery()))

    @hybrid_property
    def is_principal(self):
        return self.principal is not None

    @is_principal.inplace.expression
    def _is_principal_expr(cls):
        return db.exists(
            dicom.Principal.query.with_entities(dicom.Principal.group_id)
            .filter(cls.id == dicom.Principal.group_id)
            .limit(1)
            .subquery()
        )

    @hybrid_property
    def is_project(self):
        return self.project is not None

    @is_project.inplace.expression
    def _is_project_expr(cls):
        return db.exists(
            dicom.Project.query.with_entities(dicom.Project.group_id)
            .filter(cls.id == dicom.Project.group_id)
            .limit(1)
            .subquery()
        )

    @property
    def type(self):
        if self.principal:
            return gettext("Principal")
        elif self.project:
            return gettext("Project")
        else:
            return ""

    @staticmethod
    def pattern():
        config = _config()
        prefix = config.get("prefix")
        suffix = config.get("suffix")
        if prefix or suffix:
            return f"{prefix}*{suffix}"

    @property
    def identifier(self):
        if self.id is None:
            db.session.add(self)
            db.session.commit()
        config = _config()
        prefix = config.get("prefix")
        suffix = config.get("suffix")
        return f"{prefix}{self.id}{suffix}"

    @property
    def ou(self):
        if self.is_dicom:
            return _config().get("dicom_ou")
        return _config().get("group_ou")

    @property
    def path(self):
        ou = self.ou
        ou = f",ou={ou}" if ou else ""
        return f"cn={self.name}{ou}"

    @property
    def dn(self):
        return current_ldap.get_group_entry(self)["dn"].lower()

    @staticmethod
    def find_by_dn(dn):
        # Lookup gidNumber of group, returns empty dict if dn not found or is not group
        attrs = current_ldap.get_group_attributes(dn, ["gidNumber", "sAMAccountName"])
        if not attrs.get("gidNumber", None):
            return None

        group = db.session.get(Group, attrs["gidNumber"])
        if group and group.dn.lower() == dn.lower():
            return group

        return None

    def remove(self):
        current_ldap.remove(self)

    def update(self, *args, make_group=True, **kwargs):
        if make_group:
            current_ldap.make_group(self)
        members = set()

        if self.project:
            members.add(self.project.principal.group)
        if self.principal:
            members.add(self.principal.user)

        active_users = (
            User.query.filter(User.active)
            .join(
                UserMembership.query.filter(
                    UserMembership.group_id == self.id,
                    db.and_(
                        db.or_(
                            UserMembership.expires.is_(None),
                            UserMembership.expires > db.func.now(),
                        ),
                        UserMembership.level == MembershipType.full,
                    ),
                ).subquery()
            )
            .all()
        )

        membership = GroupMembership.query.filter(
            GroupMembership.group_id == self.id,
            db.or_(
                GroupMembership.expires.is_(None),
                GroupMembership.expires > db.func.now(),
            ),
        ).subquery("membership")

        active_groups = Group.query.join(
            membership, Group.id == membership.c.member_id
        ).all()

        current_ldap.set_members(self, chain(members, active_users, active_groups))

    def __repr__(self):
        """
        Text representation of group composed of name and key

        :returns: string
        :raises: None
        """
        if self.description:
            return "{0} - {1}".format(self.name, self.description)
        else:
            return self.name

    def __str__(self):
        """
        Simple string representation composed of only name

        :returns: string
        :raises: None
        """
        return self.name
