import enum

from datetime import timezone
from flask import current_app
from flask_babel import gettext
from flask_login import current_user
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy_utils import IPAddressType

from .credentials import TokenMixin
from .dicom import Project
from . import db
from ..mrbs.models import Entry as MRBSEntry

screening_form_devices = db.Table(
    "screening_form_devices",
    db.Model.metadata,
    db.Column("id", db.Integer, primary_key=True),
    db.Column("device_id", db.Integer, db.ForeignKey("device.id"), index=True),
    db.Column(
        "screening_form_id", db.Integer, db.ForeignKey("screening_form.id"), index=True
    ),
)


class QBoolean(db.SmallInteger):
    pass


class QBooleanNoPrefill(QBoolean):
    pass


class QBooleanNoValidate(QBoolean):
    pass


def qboolean_column():
    return db.Column(QBoolean, nullable=False, default=2)


class QString(db.String):
    pass


class QText(db.Text):
    pass


class Subject(db.Model):
    """
    MRI study subject
    """

    __tablename__ = "subject"
    #: Primary key id
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: Given Name
    given_name = db.Column(
        "given_name",
        db.String(64),
        unique=False,
        index=False,
        nullable=False,
    )
    #: Surname
    surname = db.Column(
        "surname",
        db.String(64),
        unique=False,
        index=False,
        nullable=False,
    )
    #: Subject ID
    subjectid = db.Column(
        "subjectid", db.String(64), unique=False, index=False, nullable=True
    )
    #: Birthdate
    birthdate = db.Column(
        "birthdate",
        db.Date,
        unique=False,
        index=False,
        nullable=False,
    )

    #: Active flag
    active = db.Column(
        "active",
        db.Boolean,
        default=True,
        nullable=False,
    )

    screening_forms = db.relationship("ScreeningForm", back_populates="subject")

    def data(self):
        """
        Return subject data in fixed length string

        :returns: string
        :raises: None
        """
        return "{0: <32}{1: <32}{2: <64}{3}".format(
            self.given_name,
            self.surname,
            self.subjectid or "",
            str(self.birthdate),
        )

    def __unicode__(self):
        return "{} {}".format(self.given_name, self.surname)

    def __str__(self):
        return str(self.__unicode__())

    def as_dict(self):
        return {
            "id": self.id,
            "given_name": self.given_name,
            "surname": self.surname,
            "subjectid": self.subjectid,
            "birthdate": self.birthdate.strftime("%Y"),
        }


class ScreeningState(enum.Enum):
    """
    Screening Form states

    """

    # -> created : The form is created by coordinator and sent to the participant
    created = "created"

    #      created -> coord_rev : The form is filled out by the participant and sent to coordinator for review
    #              -> coord_rev : The form is created by coordinator, filled out with participant present, saved
    # datacare_rev -> coord_rev : The form is reviewed by datacare and returned to coordinator for review/revision
    coordinator_review = "coordinator_review"

    # coord_rev -> datacare_rev : The filled out form is reviewed by coordinator and submitted for datacare review
    # created   -> datacare_rev : The created form is filled out by coordinator with participant present, submitted
    #           -> datacare_rev : The form is created by coordinator, filled out with participant present, submitted
    datacare_review = "datacare_review"

    # datacare_rev -> approved : The form has been reviewed by datacare and approved for the exam
    approved = "approved"

    # approved -> completed : The form has been confirmed by datacare with participant present on day of the exam
    completed = "completed"

    # approved -> rejected : The form has been rejected by datacare with participant present on day of the exam
    rejected = "rejected"

    # (any state) -> archived: The form has been archived and will not be visible by default.
    archived = "archived"

    def str(self):
        return dict(
            created=gettext("Created"),
            coordinator_review=gettext("Coordinator Review"),
            datacare_review=gettext("MR Tech Review"),
            approved=gettext("Approved"),
            completed=gettext("Completed"),
            rejected=gettext("Rejected"),
            archived=gettext("Archived"),
        )[self._name_]


class ScreeningForm(db.Model, TokenMixin):
    __tablename__ = "screening_form"

    # Form metadata

    id = db.Column("id", db.Integer, primary_key=True, nullable=False)

    #: Subject
    subject = db.relationship("Subject", back_populates="screening_forms")

    #: Foreign key for subject
    subject_id = db.Column(
        db.Integer, db.ForeignKey("subject.id"), nullable=True, index=True
    )

    #: One-to-Many relationship with :class:`ScreeningForm`s that belong to :class:`Project`
    project = db.relationship(
        Project,
        back_populates="screening_forms",
        order_by=Project.name,
    )
    #: Foreign key for defining relationship with :class:`Project`
    project_id = db.Column(
        "project_id",
        db.Integer,
        db.ForeignKey("project.id", ondelete="RESTRICT"),
        nullable=False,
        index=True,
    )

    create_user = db.relationship("User", back_populates="screening_forms_created")

    create_user_id = db.Column(
        db.Integer,
        db.ForeignKey("user.id", ondelete="RESTRICT"),
        nullable=False,
        index=True,
    )

    create_ip = db.Column(IPAddressType)

    parent_form_id = db.Column(
        db.Integer, db.ForeignKey("screening_form.id"), index=True
    )

    parent_form = db.relationship(
        "ScreeningForm",
        uselist=False,
        remote_side=[id],
        back_populates="children_forms",
        foreign_keys=[parent_form_id],
    )

    children_forms = db.relationship("ScreeningForm", back_populates="parent_form")

    create_time = db.Column(
        db.DateTime().with_variant(db.DateTime(timezone=True), "postgresql"),
        nullable=False,
    )

    state = db.Column(
        db.Enum(ScreeningState, name="screening_form_state"),
        name="state",
        nullable=False,
    )

    version = db.Column(db.String(11), nullable=False)

    entry_id = db.Column(
        db.Integer,
        db.ForeignKey("mrbs_entry.id", ondelete="SET NULL", onupdate="CASCADE"),
        nullable=True,
        index=True,
    )
    entry = db.relationship(MRBSEntry)

    # Form contents

    q_prior_surgery = qboolean_column()
    q_prior_surgery_text = db.Column(QString(255), nullable=True)

    q_eye_injury = qboolean_column()
    q_eye_injury_text = db.Column(QString(255), nullable=True)

    q_injury_metallic = qboolean_column()
    q_injury_metallic_text = db.Column(QString(255), nullable=True)

    q_pregnant = qboolean_column()
    q_contrast_agent = qboolean_column()

    q_aneurysm = qboolean_column()
    q_pacemaker = qboolean_column()
    q_icd = qboolean_column()
    q_electronic_device = qboolean_column()
    q_neurostimulation = qboolean_column()
    q_bio_stimulator = qboolean_column()
    q_infusion_pump = qboolean_column()
    q_infusion_device = qboolean_column()
    q_prosthesis = qboolean_column()
    q_shunt = qboolean_column()
    q_vascular_catheter = qboolean_column()
    q_radiation_seeds = qboolean_column()
    q_swan_ganz_catheter = qboolean_column()
    q_medication_patch = qboolean_column()
    q_wire_mesh_implant = qboolean_column()
    q_surgical_metallic = qboolean_column()
    q_joint_replacement = qboolean_column()
    q_bone_joint_hardware = qboolean_column()
    q_tissue_expander = qboolean_column()
    q_iud_diaphragm = qboolean_column()
    q_tattoo_cosmetics = qboolean_column()
    q_body_piercing = qboolean_column()
    q_dentures = qboolean_column()
    q_hearing_aid = qboolean_column()
    q_diabetic_sensors = qboolean_column()
    q_braces = qboolean_column()

    q_other_implants = qboolean_column()
    q_other_implants_text = db.Column(QString(255), nullable=True)

    q_breathing_motion = qboolean_column()
    q_claustrophobia = qboolean_column()

    q_covid_vaccination_status = db.Column(
        QBooleanNoValidate, nullable=False, default=2
    )
    q_covid_test_confirm = db.Column(QBooleanNoValidate, nullable=False, default=2)

    # MR Tech fields
    q_mr_tech_comments = db.Column(QText, nullable=True)

    # Coordinator fields
    q_coordinator_comments = db.Column(QText, nullable=True)

    # Participant fields
    q_participant_comments = db.Column(QText, nullable=True)
    q_participant_name = db.Column(QString(255), nullable=True)

    #: Many-to-Many relationship with Devices
    devices = db.relationship("Device", secondary=screening_form_devices)

    @property
    def last_coordinator(self):
        row = self
        while row.state not in (
            ScreeningState.created,
            ScreeningState.coordinator_review,
            ScreeningState.datacare_review,
        ):
            row = row.parent_form
        return row.create_user

    @hybrid_property
    def latest(self):
        return len(self.children_forms) == 0

    # noinspection PyMethodParameters
    @latest.expression
    def latest(cls):
        return db.not_(cls.children_forms.any())

    @property
    def original_create_time(self):
        return self.original_form.create_time

    @property
    def original_form(self):
        row = self
        while row.parent_form:
            row = row.parent_form
        return row

    @property
    def create_time_local(self):
        return (
            self.create_time.astimezone(timezone.utc)
            .astimezone(current_user.timezone)
            .strftime("%B %d %Y %H:%M %Z")
        )

    @property
    def history(self):
        history = [self]
        form = self
        while form.parent_form:
            form = form.parent_form
            history.append(form)
        return history

    @property
    def create_child_version(self):
        """
        Used to decide if the current version of the table should be preserved or overwritten when a new version is
        being flushed into the database

        Don't preserve the history of the initial unedited form sent to the participant, or any edits while the form
        is in coordinator_review state

        :return: False if form remains in created or coordinator_review state, or is transitioning from created to
            coordinator_review.
        """
        state_history = db.inspect(self).attrs.state.history
        return not (
            (
                state_history.unchanged
                and (
                    state_history.unchanged[0] == ScreeningState.created
                    or state_history.unchanged[0] == ScreeningState.coordinator_review
                )
            )
            or (
                state_history.added
                and state_history.added[0] == ScreeningState.coordinator_review
                and state_history.deleted
                and state_history.deleted[0] == ScreeningState.created
            )
        )

    def new_version(self, session):
        if not self.create_child_version:
            return

        copy = ScreeningForm()
        for col in self.__table__.columns:
            if col.name not in ("id", "parent_form_id"):
                copy.__setattr__(col.name, getattr(self, col.name))

        # for all non-self-referential relationships,
        # make sure the relationship field of the new form is marked as dirty
        # this will trigger an insert of that relationship's id column into the association table (if many-to-many)
        # or an update of the id column in this table (otherwise)
        for rel in [
            r
            for r in db.inspect(self.__class__).relationships
            if r.target != self.__table__
        ]:
            copy.__setattr__(rel.key, getattr(self, rel.key))

        # Assign values that are overwritten on new version creation
        copy.parent_form_id = self.id
        copy.create_user = current_user

        # preserve self as parent
        session.expunge(self)

        # add copy to session as a new version
        session.add(copy)

    _token_purpose = "participant_send"

    def get_token(self, email=None, **kwargs):
        return super().get_token(
            max_age=current_app.config["SCREENING"]["token_expiry"],
            purpose=self._token_purpose,
            email=email,
        )

    @classmethod
    def verify_token(cls, token, **kwargs):
        return super().verify_token(
            token,
            max_age=current_app.config["SCREENING"]["token_expiry"],
            purpose=cls._token_purpose,
            email=None,
        )


class Device(db.Model):
    __tablename__ = "device"

    # Form metadata

    id = db.Column("id", db.Integer, primary_key=True, nullable=False)

    name = db.Column(
        "name",
        db.String(255),
        index=True,
        unique=True,
        nullable=False,
    )

    active = db.Column(
        "active",
        db.Boolean,
        default=True,
        nullable=False,
    )

    #: flag for devices which are applicable to human participants
    clinical = db.Column(
        "clinical",
        db.Boolean,
        default=False,
        nullable=False,
    )

    def __str__(self):
        """
        String representation of Device

        :returns: string
        :raises: None
        """
        return self.name
