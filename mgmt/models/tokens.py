import datetime

from flask import current_app
from itsdangerous import URLSafeTimedSerializer as Serializer, BadData
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound  # noqa
from flask_babel import gettext


class InvalidToken(Exception):
    pass


class MalformedToken(InvalidToken):
    pass


# noinspection PyUnresolvedReferences
class TokenMixin:
    def get_token(self, max_age=1800, **kwargs):
        """
        Get a token associated with this user

        :param max_age: ticket validity duration in seconds (default: 30 minutes)
        :param kwargs: dictionary to put in the payload, in addition to user's id
        :return: a tuple containing the token and its expiry timestamp
        """

        s = Serializer(current_app.config["SECRET_KEY"])
        kwargs["id"] = self.id
        token = s.dumps(kwargs)
        return token, s.loads(token, return_timestamp=True)[1] + datetime.timedelta(
            seconds=max_age
        )

    @classmethod
    def get(cls, object_id):
        return cls.query.filter(cls.id == int(object_id)).one()

    @classmethod
    def verify_token(cls, token, max_age=None, **kwargs):
        """
        Verify token and return values in it

        Confirms that the token is valid and unexpired; that object id is valid; optionally checks purpose field and
        gets other fields.

        :param token: The token to check
        :param max_age: If provided, ensures the signature is not older than that time in seconds. In
        case the signature is outdated, :exc:`SignatureExpired` exception is raised.
        :param kwargs: dictionary whose keys will be extracted from the token
        :return: dictionary containing all kwargs requested, as well as reference to object
        """

        # noinspection PyBroadException
        s = Serializer(current_app.config["SECRET_KEY"])
        try:
            data = s.loads(token, max_age=max_age)
        except BadData:
            if "SECRET_KEY_ALTERNATE" in current_app.config:
                s = Serializer(current_app.config["SECRET_KEY_ALTERNATE"])
                try:
                    data = s.loads(token, max_age=max_age)
                except BadData:
                    raise InvalidToken(gettext("Invalid or expired token"))
            else:
                raise InvalidToken(gettext("Invalid or expired token"))
        except Exception as err:
            msg = gettext("Error parsing token: {0}").format(err)
            current_app.logger.error(msg)
            raise MalformedToken(msg)

        try:
            results = {cls.__tablename__: cls.get(data.get("id"))}
        except (NoResultFound, MultipleResultsFound):
            msg = gettext("Error looking up id {id} in {tablename}").format(
                id=data.get("id"), tablename=cls.__tablename__
            )
            current_app.logger.warning(msg)
            raise MalformedToken(
                gettext("Invalid {tablename} id in token").format(
                    tablename=cls.__tablename__
                )
            )
        except Exception as err:
            msg = gettext("Error extracting record from token: {0}").format(err)
            current_app.logger.error(msg)
            raise MalformedToken(msg)

        purpose = kwargs.pop("purpose", None)
        if purpose and not (data.get("purpose") and data.get("purpose") == purpose):
            msg = gettext("Invalid purpose: {purpose} != {required}").format(
                purpose=data.get("purpose"), required=purpose
            )
            current_app.logger.warning(msg)
            raise InvalidToken(gettext("Invalid token purpose"))

        try:
            results.update({key: data[key] for key in kwargs})
        except KeyError as err:
            msg = gettext("Invalid key: {key}").format(key=str(err))
            current_app.logger.warning(msg)
            raise InvalidToken(gettext("Invalid token contents"))

        return results
