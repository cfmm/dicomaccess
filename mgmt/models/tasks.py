from datetime import datetime, timezone
from . import db


class Status(db.Model):
    __tablename__ = "status"
    #: Primary key
    id = db.Column("id", db.Integer, primary_key=True)
    expiry = db.Column(
        "expiry",
        db.DateTime(),
        nullable=False,
        default=datetime.now(timezone.utc),
        server_default=db.func.now(),
    )
