from flask_cfmm.database import create_database, create_metadata

db = create_database(create_metadata(naming_convention=None, schema=None))

from . import dicom  # noqa: E402
from . import credentials  # noqa: E402

__all__ = (dicom, credentials)
