"""
Created on Jul 23, 2015

Copyright 2015, The University of Western Ontario

.. module:: models
    :platform: Unix, Mac, Windows
    :synopsis: Defines data model for flask application

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

import re
import socket
import typing
from functools import partial
from string import whitespace

from sqlalchemy.dialects import postgresql
from flask import current_app
import sqlalchemy.types as types
from flask_babel import gettext
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy_utils import StringEncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesGcmEngine

from . import db
from .credentials import User, Group, UserMembership, MembershipType
from ..arc_records import AccessControl, Device, ExportRule

# noinspection PyProtectedMember
from ..mrbs.models import mysql, _varchar_params

if typing.TYPE_CHECKING:
    from ..arc import ArcConfig


def get_study(principal, project):
    return "{0}^{1}".format(principal, project)


class Principal(db.Model):
    """
    Principal group of projects
    """

    def mgmt_record(self, server: "ArcConfig") -> AccessControl:
        return AccessControl(self, server)

    __tablename__ = "principal"
    priority = 5

    #: Primary key
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: Name
    name = db.Column(
        "name",
        db.String(16),
        index=True,
        unique=True,
        nullable=False,
    )
    #: One-to-One relationship with :class:`Group` that is allowed to
    #: create/modify :class:`Project` within Principal
    group = db.relationship(
        Group,
        back_populates="principal",
        single_parent=True,
        cascade="all, delete-orphan",
        passive_updates=True,
        order_by=Group.name,
    )
    #: Foreign key for group
    group_id = db.Column(
        db.Integer,
        db.ForeignKey("group.id", ondelete="RESTRICT", onupdate="CASCADE"),
        nullable=False,
        unique=True,
        index=True,
    )
    user_member = association_proxy("group", "user_member")
    group_member = association_proxy("group", "group_member")
    #: One-to-Many relationship with :class:`Project` that belong to Principal
    projects = db.relationship(
        "Project",
        back_populates="principal",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
        cascade_backrefs=False,
    )
    #: Many-to-one relationship with :class:`User` that owns Principal
    user = db.relationship(
        User,
        back_populates="principals",
        order_by=User.username,
    )
    #: Foreign key for user
    user_id = db.Column(
        db.Integer,
        db.ForeignKey("user.id", ondelete="RESTRICT", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )

    @property
    def active(self):
        return any(project.active for project in self.projects)

    def __str__(self):
        """
        String identifying principal

        :return: string
        :raises: None
        """
        return self.name

    # noinspection PyUnusedLocal
    @db.validates("name")
    def validate_principal(self, key, value):
        """
        Validate changes to Principal name

        Automatic creation of attribute and group.

        :param key: Column being validated
        :type key: string
        :param value: Value of key
        :type value: string
        :returns: string
        """
        invalid = '":^@*.?'
        if any(x in whitespace + invalid for x in value):
            raise ValueError(
                gettext("Name cannot contain any of '{invalid}' nor whitespace").format(
                    invalid=invalid
                )
            )

        if not value:
            raise ValueError(gettext("Name cannot be null"))

        # Update the project study variables because the principal has changed
        if value != self.name:
            for project in self.projects:
                project.study = get_study(value, project.name)

        # Make sure the group exists
        if not self.group:
            # noinspection PyArgumentList
            self.group = Group(name=value)
            db.session.add(self.group)
        else:
            self.group.name = value

        return value

    def dicom_properties(self):
        """
        LDAP access control rule

        :return: list
        """
        # Case-insensitive (?i) comparison to the principal name
        # The `^` or ` ` character is the separator between principal and project
        # Trailing whitespace in a StudyDescription gets stripped, so also match just the principal name
        return [f"StudyDescription=(?i)^\\Q{self.name}\\E([\\^ ].*$|$)"]

    @classmethod
    def accessible_to(cls, user, level: typing.Union[MembershipType, None] = None):
        """Return a query for all principals which the user is a member of

        :returns: SQLAlchemy query
        """
        matching = cls.query

        if user.is_authenticated:
            matching = matching.filter(
                Principal.group_id.in_(UserMembership.member_of(user, level))
            )
        else:
            matching = matching.limit(0)

        return matching


class no_autoflush:
    def __init__(self, session):
        self.session = session
        self.autoflush = session.autoflush

    def __enter__(self):
        self.session.autoflush = False

    # noinspection PyUnusedLocal
    def __exit__(self, datatype, value, traceback):
        self.session.autoflush = self.autoflush


def get_default_rate():
    variable = Variable.query.filter(Variable.name == "default_rate_id").first()
    return int(variable.value) if variable else 1


def get_default_operator():
    variable = Variable.query.filter(Variable.name == "default_operator_id").first()
    return int(variable.value) if variable else 1


class Project(db.Model):
    """
    Project definition for DICOM handling
    """

    def mgmt_record(self, server: "ArcConfig") -> AccessControl:
        return AccessControl(self, server)

    __tablename__ = "project"
    priority = 10

    #: Primary key id
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: Project name
    name = db.Column(
        "name",
        db.String(32),
        index=True,
        unique=False,
        nullable=False,
    )
    #: Project description
    description = db.deferred(
        db.Column(
            "description",
            db.String(4095),
            index=False,
            unique=False,
            nullable=True,
        )
    )

    #: Project description for study participants and other 'public-facing' use
    participant_description = db.deferred(
        db.Column(
            "participant_description",
            db.Text(),
            index=False,
            unique=False,
            nullable=True,
        )
    )

    #: Flag for active studies that can be booked
    active = db.Column(
        "active",
        db.Boolean,
        default=True,
        nullable=False,
    )

    #: Study name for MRBS display
    study = db.Column(
        "study",
        db.VARCHAR(82).with_variant(
            mysql.VARCHAR(length=82, **_varchar_params), "mysql"
        ),
        nullable=False,
        unique=True,
    )

    #: One-to-Many relationship with :class:`Destination` to which DICOM data
    #: will be forwarded
    destinations = db.relationship(
        "Destination",
        back_populates="project",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
    )
    #: One-to-Many relationship with :class:`Attribute` that define DICOM data
    #: belonging to project
    attributes = db.relationship(
        "Attribute",
        back_populates="project",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
    )
    #: One-to-Many relationship with :class:`Project` that belong to Principal
    principal = db.relationship(
        Principal,
        back_populates="projects",
        order_by=Principal.name,
        cascade_backrefs=False,
    )
    #: Foreign key for defining relationship with :class:`Principal`
    principal_id = db.Column(
        "principal_id",
        db.Integer,
        db.ForeignKey("principal.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    #: One-to-One relationship with :class:`Group` that is allowed to
    #: allowed access to project
    group = db.relationship(
        Group,
        back_populates="project",
        single_parent=True,
        cascade="all, delete-orphan",
        passive_updates=True,
        order_by=Group.name,
    )
    #: Foreign key for group
    group_id = db.Column(
        db.Integer,
        db.ForeignKey("group.id", ondelete="RESTRICT", onupdate="CASCADE"),
        nullable=False,
        unique=True,
        index=True,
    )
    user_member = association_proxy("group", "user_member")
    group_member = association_proxy("group", "group_member")

    #: name and principal_id must be unique
    __table_args__ = (db.UniqueConstraint("principal_id", "name"),)

    #: One-to-Many relationship with default :class:`Rate`
    default_rate = db.relationship(
        "Rate",
        back_populates="projects",
        order_by="Rate.name",
    )

    #: Foreign key for defining relationship with :class:`Rate`
    default_rate_id = db.Column(
        "default_rate_id",
        db.Integer,
        db.ForeignKey("rate.id"),
        nullable=False,
        default=get_default_rate,
        index=True,
    )

    #: One-to-Many relationship with :class:`User` which is the default operator
    default_operator = db.relationship(
        "Operator",
        back_populates="projects",
        order_by="Operator.code",
    )

    #: Foreign key for defining relationship with :class:`Operator`
    default_operator_id = db.Column(
        "default_operator_id",
        db.Integer,
        db.ForeignKey("operator.id"),
        nullable=False,
        default=get_default_operator,
        index=True,
    )

    #: Default billing speedcode
    speedcode = db.Column(
        "speedcode", db.String(length=64), unique=False, nullable=True
    )

    screening_forms = db.relationship(
        "ScreeningForm", back_populates="project", cascade_backrefs=False
    )

    def as_dict(self):
        return {
            "id": self.id,
            "study": self.study,
        }

    @db.validates("name", "principal")
    def validate_project(self, key, value):
        """
        Validates changes to principal and name.

        Automatically creates correct Attribute match and group.

        :param key: Column being validated
        :type key: string
        :param value: Value of key
        :type value: string
        :returns: string
        :raises: ValueError
        """
        if key == "name":
            invalid = '":^@*.?'
            if any(x in whitespace + invalid for x in value):
                raise ValueError(
                    gettext(
                        "Name cannot contain any of '{invalid}' nor whitespace"
                    ).format(invalid=invalid)
                )
            name = value
        else:
            name = self.name

        if key == "principal":
            principal = value
        else:
            principal = self.principal

        if name and principal:
            # Set the required study name
            with db.session.no_autoflush:
                self.study = get_study(principal.name, name)

            # Create the group for the project
            grp_name = "{0}-{1}".format(principal.name, name)
            if not self.group:
                # noinspection PyArgumentList
                self.group = Group(
                    name=grp_name,
                    description=gettext("Group for {study}").format(study=self.study),
                )
                db.session.add(self.group)
            else:
                self.group.name = grp_name

        return value

    # noinspection PyUnusedLocal
    @db.validates("study")
    def validate_study(self, key, value):
        # noinspection PyUnresolvedReferences
        existing_study = (
            Project.query.autoflush(False).filter(Project.study.ilike(value)).first()
        )
        if existing_study and existing_study is not self:
            raise ValueError(
                gettext(
                    "Invalid study name: '{name}' conflicts with already existing '{study}'"
                ).format(name=value, study=existing_study.study)
            )
        return value

    def __str__(self):
        """
        String identification of project

        :returns: string
        :raises: None
        """
        return self.study

    @staticmethod
    def lookup(name):
        """
        Returns the principal and project associated with a string representation

        :return: (Principal, Project) Tuple
        :raises: None
        """
        names = name.split(str="^", num=1)

        return (
            Principal.query.filter(name=names[0]).first(),
            Project.query.filter(name=names[1]).first(),
        )

    def dicom_properties(self):
        """
        LDAP access control rule

        :return: list
        """
        if self.attributes:
            format_args = {
                "study": "\\Q" + self.study.lower() + "\\E",
                "principal": "\\Q" + self.principal.name.lower() + "\\E",
                "project": "\\Q" + self.name.lower() + "\\E",
            }
            # noinspection PyTypeChecker
            rules = {x.ldap_rule(**format_args) for x in self.attributes}
        else:
            # Case-insensitive comparison to study name
            # or principal and project name with the separator character of `^` or ` `
            rules = {
                f"StudyDescription=(?i)^(\\Q{self.study}\\E|\\Q{self.principal.name}\\E[\\^ ]\\Q{self.name}\\E)$"
            }

        return list(rules)

    @classmethod
    def accessible_to(
        cls,
        user: User,
        level: typing.Union[MembershipType, None] = None,
        active: bool = True,
    ):
        """
        Return a query for all projects which the `user` is allowed to access.

        :param user: user which should have access to projects in query
        :param level: minimum membership level required
        :param active: only return active projects
        :returns: SQLAlchemy query
        """
        matching_projects = cls.query

        if user.is_authenticated:
            if active:
                matching_projects = matching_projects.filter(cls.active.is_(True))

            if not user.is_datacare:
                # Query for all groups which the user is a direct member of
                member_of = UserMembership.member_of(user, level)

                principal_alias = db.aliased(Principal)

                # Restrict projects to those which the user is a member at the requested level
                matching_projects = matching_projects.join(principal_alias).filter(
                    db.or_(
                        cls.group_id.in_(member_of),
                        principal_alias.group_id.in_(member_of),
                    )
                )
        else:
            # Unauthenticated users never have access to any projects
            matching_projects = matching_projects.filter(db.false())

        return matching_projects


class Tag(db.Model):
    """
    DICOM tag definition
    """

    __tablename__ = "tag"
    #: Primary key
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: Common name of tag
    name = db.Column(
        "name",
        db.String(128),
        index=False,
        unique=True,
        nullable=False,
    )
    #: DICOM Tag ID value
    tagid = db.Column(
        "tagid",
        db.String(8),
        index=False,
        unique=True,
        nullable=False,
    )
    #: One-to-Many relationship with :class:`Attribute` that use this tag
    attributes = db.relationship(
        "Attribute",
        back_populates="tag",
        cascade="save-update, merge",
        passive_updates=True,
    )

    def __str__(self):
        """
        String identification of DICOM tag

        :returns: string
        :raises: None
        """
        return "{0} - {1}".format(self.tagid, self.name)


class Attribute(db.Model):
    """
    Project attributes to match DICOM data to Principal or Project
    """

    __tablename__ = "attribute"
    #: Primary key
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: DICOM :class:`Tag` to match
    tag = db.relationship("Tag", order_by=Tag.tagid, back_populates="attributes")
    #: Foreign key for relationship with :class:`Tag`
    tag_id = db.Column(
        "tag_id",
        db.Integer,
        db.ForeignKey("tag.id", ondelete="RESTRICT", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    #: Value for DICOM :class:`Tag`
    value = db.Column(
        "value",
        db.String(256),
        index=False,
        unique=False,
        nullable=False,
    )
    project = db.relationship(
        "Project",
        back_populates="attributes",
        order_by=Project.name,
    )
    #: Foreign key for relationship with :class:`Project`
    project_id = db.Column(
        "project_id",
        db.Integer,
        db.ForeignKey("project.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    __table_args__ = (
        db.UniqueConstraint(
            "project_id",
            "tag_id",
        ),
    )

    def __str__(self):
        """
        String identification of attribute

        :returns: string
        :raises: None
        """
        return "{0} == '{1}'".format(
            self.tag.name,
            self.value,
        )

    def ldap_rule(self, **kwargs):
        """
        LDAP Access control rule

        Note that value can be a full Java Regex expression. This may not be compatible with an XSL test, so it is not
        advisable to use both XSL and LDAP together.

        Because XSL requires version 1.0 which does not support matches and regex, it is not possible to fully support
        both.

        :return: string
        """
        fmt = f"{self.tag.tagid}={self.value}"
        return fmt.format(**kwargs)


class Destination(db.Model):
    """
    DICOM Forward destination
    """

    def mgmt_record(self, server: "ArcConfig") -> ExportRule:
        record = ExportRule(self, server)
        record.valid = self.server.installed
        return record

    __tablename__ = "destination"
    #: Primary key id
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: Delay for forwarding
    delay = db.Column(
        "delay",
        db.String(32),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Priority for forwarding
    priority = db.Column(
        "priority",
        db.String(32),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Foreign key for relationship with :class:`Server`
    server_id = db.Column(
        "server_id",
        db.Integer,
        db.ForeignKey("server.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    #: Reference to receiving :class:`Server`
    server = db.relationship(
        "Server", order_by="Server.aet", back_populates="destinations"
    )
    project = db.relationship(
        "Project",
        back_populates="destinations",
        order_by="Project.name",
    )
    #: Foreign key for relationship with :class:`Project`
    project_id = db.Column(
        "project_id",
        db.Integer,
        db.ForeignKey("project.id", onupdate="CASCADE", ondelete="CASCADE"),
        index=True,
    )

    # noinspection PyUnusedLocal
    @db.validates("delay")
    def validate_delay(self, key, value):
        if value:
            m = re.match(
                r"^P(?:(\d+)Y)?(?:(\d+)M)?(?:(\d+)D)?T(?:(\d+)H)?(?:(\d+)M)?(?:(\d+(?:.\d+)?)S)?$",
                value,
            )
            if m is None:
                raise ValueError(
                    gettext("{value} is not ISO 8061 compliant").format(value=value)
                )
        return value

    def __str__(self):
        """
        String identification of destination

        :returns: string
        :raises: None
        """
        frmt = gettext("{aet} delayed '{delay}' with priority '{priority}'")
        return frmt.format(
            aet=self.server.aet, delay=self.delay, priority=self.priority
        )


def database_default(name):
    try:
        return current_app.config["DATABASE_DEFAULTS"][name]
    except (RuntimeError, KeyError):
        pass
    return None


class Server(db.Model):
    """
    DICOM server configuration
    """

    def mgmt_record(self, server: "ArcConfig") -> Device:
        return Device(self, server)

    __tablename__ = "server"
    #: Primary key id
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    #: Application entity title
    aet = db.Column(
        "aet",
        db.String(30),
        index=False,
        unique=True,
        nullable=False,
    )
    #: Host name of dicom server
    host = db.Column(
        "host",
        db.String(250),
        index=False,
        unique=False,
        nullable=False,
    )
    #: Port number on server
    port = db.Column(
        "port",
        db.Integer,
        default=11112,
        index=False,
        unique=False,
        nullable=False,
    )
    #: Description
    description = db.Column(
        "description",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Available cipher suites
    cipher = db.Column(
        "cipher",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Issuer of Patient ID
    pat_id_issuer = db.Column(
        "pat_id_issuer",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
        default=partial(database_default, "issuer"),
    )
    #: Issuer of Accession Number
    acc_no_issuer = db.Column(
        "acc_no_issuer",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
        default=partial(database_default, "issuer"),
    )
    #: Username for AET access
    user_id = db.Column(
        "user_id",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Password for AET access
    passwd = db.Column(
        "passwd",
        StringEncryptedType(
            types.Unicode, "encrypt@key.store", AesGcmEngine, "pkcs5", length=250
        ).with_variant(postgresql.VARCHAR(250), "postgresql"),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Station Name
    station_name = db.Column(
        "station_name",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
    )
    #: Institution
    institution = db.Column(
        "institution",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
        default=partial(database_default, "institution"),
    )
    #: Department
    department = db.Column(
        "department",
        db.String(250),
        index=False,
        unique=False,
        nullable=True,
        default=partial(database_default, "department"),
    )
    #: AE Title is installed flag
    installed = db.Column(
        "installed",
        db.Boolean,
        default=True,
        index=False,
        unique=False,
        nullable=False,
    )
    restricted = db.Column(
        "restricted",
        db.Boolean,
        default=False,
        nullable=False,
        server_default=db.false(),
    )

    # Servers belonging to a User are deleted when that User is deleted
    owner_id = db.Column(
        db.Integer,
        db.ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )

    user = db.relationship(
        User,
        back_populates="servers",
        order_by=User.username,
    )

    destinations = db.relationship(
        "Destination",
        back_populates="server",
        cascade="all, delete-orphan",
        passive_deletes=True,
        passive_updates=True,
        cascade_backrefs=False,
    )

    @property
    def exporter_id(self):
        return f"{self.aet}@{self.host}"

    def __repr__(self):
        """
        Text representation of the server

        :returns: string
        :raises: None
        """
        return "{0} <{1}:{2}>".format(self.aet, self.host, self.port)

    def __str__(self):
        """
        Simple string representation

        :returns: string
        :raises: None
        """
        return self.aet

    @db.validates("port", "host", "aet")
    def validate_server(self, key, value):
        """
        - Validate removal/modification of Server object
        - Server hostname must be resolvable by the DNS

        :param key: Column being validated
        :type key: string
        :param value: Value of key
        :type value: string
        :returns: string
        :raises: ValueError
        """
        if self.destinations and not (value == getattr(self, key)):
            raise ValueError(gettext("Cannot modify Server that is in use."))

        if key == "host":
            try:
                socket.gethostbyname(value)
            except socket.gaierror as e:
                if e.errno == socket.EAI_NONAME:
                    raise ValueError(gettext("Hostname cannot be resolved by the DNS."))
                else:
                    raise ValueError(
                        gettext(
                            "Unknown error occurred when attempting to validate the hostname."
                        )
                    )
        return value


class Operator(db.Model):
    __tablename__ = "operator"
    id = db.Column("id", db.Integer, primary_key=True, nullable=False)
    code = db.Column("code", db.String(3), nullable=False, unique=True)
    name = db.Column(
        "name",
        db.String(255),
        nullable=True,
    )
    active = db.Column("active", db.Boolean, nullable=False, default=True)
    # Operator belonging to a User is deleted when the User is deleted
    user_id = db.Column(
        "user_id",
        db.Integer,
        db.ForeignKey("user.id", ondelete="CASCADE", onupdate="CASCADE"),
        nullable=False,
        index=True,
    )
    user = db.relationship(
        User,
        back_populates="operators",
        order_by=User.username,
    )

    projects = db.relationship(Project, back_populates="default_operator")

    def __str__(self):
        return "{0}: {1}".format(self.code, self.name or self.user.username)


class Variable(db.Model):
    __tablename__ = "variable"
    id = db.Column("id", db.Integer, primary_key=True, nullable=False)
    name = db.Column("name", db.String(255), unique=True, nullable=False)
    value = db.Column("value", db.String(255), unique=False, nullable=False)
