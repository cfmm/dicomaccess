import typing as t
from flask import current_app
from werkzeug.local import LocalProxy

if t.TYPE_CHECKING:
    from .ldap import LDAP
    from .arc import ArcUpdate
    from flask_sqlalchemy import SQLAlchemy
    from flask_cfmm.datastore import CFMMUserDatastore


# noinspection PyTypeChecker
current_ldap: "LDAP" = LocalProxy(lambda: current_app.extensions["ldap"])

# noinspection PyTypeChecker
current_arc: "ArcUpdate" = LocalProxy(lambda: current_app.extensions["arc"])

# noinspection PyTypeChecker
current_db: "SQLAlchemy" = LocalProxy(lambda: current_app.extensions["sqlalchemy"])

# noinspection PyTypeChecker
current_datastore: "CFMMUserDatastore" = LocalProxy(
    lambda: current_app.extensions["security"].datastore
)
