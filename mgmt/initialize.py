from pathlib import Path
from flask import current_app, Blueprint

from flask_cfmm.security import create_user
from flask_cfmm.configure import config_value
from flask_cfmm.cli import log_change
from sqlalchemy import event
from sqlalchemy.orm import Session

from .models.credentials import Group, GroupMembership, UserMembership, User
from .models.dicom import Tag, Operator, Variable
from .mrbs.models import Rate
from .models import db
from .events import after_commit, after_transaction_end

bp = Blueprint("init", __name__)


def _create_default_tags():
    if Tag.query.count() > 0:
        return False

    filename = Path(current_app.root_path).joinpath("dicom").joinpath("dictionary.txt")

    changed = False
    if filename.is_file():
        with open(filename, "r") as myfile:
            for line in myfile:
                # Discard comments starting with #
                if line[0] == "#":
                    continue

                # Fields are tab delimited
                fields = [x.strip() for x in line.split("\t")]

                tag = Tag(
                    tagid=fields[0][1:5] + fields[0][6:10],
                    name=fields[2],
                )
                db.session.add(tag)
                changed = True
            db.session.commit()

    log_change(changed)


def _create_default_users():
    if (
        User.query.first() is not None
        or Group.query.first() is not None
        or Variable.query.first() is not None
    ):
        return

    try:
        # Disable synchronization events
        event.remove(Session, "after_commit", after_commit)
        event.remove(Session, "after_transaction_end", after_transaction_end)

        # Create the Admin group that contains administrators
        admin_group = current_app.config.get("CFMM_ADMIN_ROLE", "admin")
        admin = Group(name=admin_group)
        db.session.add(admin)
        db.session.commit()

        admin_user = create_user(username=current_app.config.get("admin_user", "admin"))
        membership = UserMembership(member_id=admin_user.id, group_id=admin.id)
        db.session.add(membership)
        db.session.commit()

        datacare_group = current_app.config.get("CFMM_DATACARE_ROLE", "datacare")
        web_group = config_value("access_role")

        # Create the Datacare group that is allowed to change data
        datacare = Group(name=datacare_group)
        db.session.add(datacare)
        db.session.commit()

        datacare_user = create_user(
            username=current_app.config.get("datacare_user", "datacare")
        )
        membership = UserMembership(member_id=datacare_user.id, group_id=datacare.id)
        db.session.add(membership)
        db.session.commit()

        if web_group:
            # Create the Web group that contains all users with web access
            web = Group(name=web_group)
            db.session.add(web)
            db.session.commit()

            # Make sure datacare group can access web
            membership = GroupMembership(member_id=datacare.id, group_id=web.id)
            db.session.add(membership)

            membership = GroupMembership(member_id=admin.id, group_id=web.id)
            db.session.add(membership)

            db.session.commit()

        # Set the variables that control defaults
        rate = Rate(name="Default", hourly=0, fixed=0)
        db.session.add(rate)
        operator = Operator(code="N/A", user=datacare_user)
        db.session.add(operator)

        # Commit so Rate and Operator are correctly set
        db.session.commit()

        variable = Variable(name="default_rate_id", value=rate.id)
        db.session.add(variable)

        variable = Variable(name="default_operator_id", value=operator.id)
        db.session.add(variable)

        db.session.commit()
        log_change(True)
    finally:
        # Re-enable synchronization events
        event.listen(Session, "after_commit", after_commit)
        event.listen(Session, "after_transaction_end", after_transaction_end)


@bp.cli.command("setup")
def setup():
    """
    Create default tags and users
    """
    _create_default_tags()
    _create_default_users()


@bp.cli.command("tags")
def create_default_tags():
    """
    Parse a matlab dictionary of DICOM Tags and insert into database
    """
    _create_default_tags()


@bp.cli.command("users")
def create_default_users():
    """
    Create default users
    """
    _create_default_users()
