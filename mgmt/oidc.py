from flask_cfmm.resource import (
    create_client as _create_client,
    create_or_update,
    get_keycloak,
    KeycloakUMA,
    KeycloakOpenIDConnection,
    KeycloakPostError,
    RegistrationTokenConfiguration,
)


def create_sync_client(app, token, client_id, changed=False):
    payload = {
        "clientId": client_id,
        "name": client_id,
        "surrogateAuthRequired": False,
        "enabled": True,
        "alwaysDisplayInConsole": False,
        "clientAuthenticatorType": "client-secret",
        "description": "Service account to allow ARC to access MGMT arc sync endpoint",
        "notBefore": 0,
        "bearerOnly": False,
        "consentRequired": False,
        "standardFlowEnabled": False,
        "implicitFlowEnabled": False,
        "directAccessGrantsEnabled": False,
        "serviceAccountsEnabled": True,
        "authorizationServicesEnabled": False,
        "publicClient": False,
        "frontchannelLogout": False,
        "protocol": "openid-connect",
        "authenticationFlowBindingOverrides": {},
        "fullScopeAllowed": False,
        "nodeReRegistrationTimeout": -1,
    }

    registration_token = RegistrationTokenConfiguration(app, "registration_token_sync")
    return create_or_update(app, token, payload, registration_token, changed)


def create_or_update_access(client, policy, resource_name):
    changed = False
    scope = policy["scopes"][0]
    resource_ids = client.resource_set_list_ids(
        name=resource_name, exact_name=True, scope=scope, maximum=1
    )
    if resource_ids:
        try:
            client.policy_resource_create(resource_id=resource_ids[0], payload=policy)
            changed = True
        except KeycloakPostError as err:
            if err.response_code == 409:
                existing_policy = client.policy_query(
                    resource=resource_name,
                    scope=scope,
                    name=policy["name"],
                    maximum=1,
                )[0]
                for key in ("name", "description", "scopes", "clients", "roles"):
                    if key not in policy and key not in existing_policy:
                        continue
                    if (
                        key not in policy
                        or key not in existing_policy
                        or policy[key] != existing_policy[key]
                    ):
                        client.policy_update(
                            policy_id=existing_policy["id"], payload=policy
                        )
                        changed = True
                        break
            else:
                raise

        return changed


def create_client(app, token, servers, redirect_urls, **kwargs):
    changed = _create_client(app, token, servers, redirect_urls, **kwargs)

    # MRBS and MGMT share the same client
    # Authorize the client to access project query REST API
    client = KeycloakUMA(KeycloakOpenIDConnection(**get_keycloak(app)))
    changed |= create_or_update_access(
        client,
        {
            "name": "MRBS-MGMT",
            "description": "Allow MRBS to access MGMT project REST API",
            "scopes": ["query"],
            "clients": [client.connection.client_id],
        },
        "project",
    )

    changed |= create_or_update_access(
        client,
        {
            "name": "Project query",
            "description": "Allow users to query MGMT project REST API",
            "scopes": ["query"],
            "roles": [app.config["CFMM_ACCESS_ROLE"]],
        },
        "project",
    )

    # ARC instance need to be able to trigger a sync
    # Create service account client and authorize endpoint access
    arc_client = "mgmt-sync"
    changed |= create_sync_client(app, token, arc_client, changed)
    changed |= create_or_update_access(
        client,
        {
            "name": "ARC-MGMT",
            "description": "Allow ARC to access MGMT arc REST API",
            "scopes": ["sync"],
            "clients": [arc_client],
        },
        "arc",
    )

    return changed
