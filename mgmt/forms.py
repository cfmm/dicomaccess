"""
Forms for login

Created on July 29, 2015

Copyright 2015, The University of Western Ontario

.. module:: forms
    :platform: Unix, Mac, Windows
    :synopsis: Forms for Login to a Flask application

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

from flask import request, url_for, redirect
from flask_babel import gettext
from flask_wtf import FlaskForm as BaseForm
from ldap3.core.exceptions import LDAPException
from six.moves.urllib.parse import urlparse, urljoin
from wtforms.fields import StringField, HiddenField
from wtforms.validators import (
    DataRequired,
    Email,
    Length,
    ValidationError,
    Regexp,
)

from .models.credentials import User
from .proxy import current_ldap, current_app


class UsernameValidator(Regexp):
    """Ensure that no invalid characters are present in username when logging in or creating a new user"""

    def __init__(self, flags=0):
        super().__init__(
            regex="^[a-zA-Z0-9_.-]+$",
            message=gettext(
                "Username must only consist of alphanumeric characters and _.-"
            ),
            flags=flags,
        )

    def __call__(self, form, field, message=None):
        super().__call__(form, field, message)

        if not User.check_username(form.username.data):
            raise ValidationError(message or field.gettext("Username already exists"))


def is_safe_url(target):
    """
    Ensure redirect point to the same server.

    :param target: URL to check
    :type target: string
    :returns: string
    :raises: None
    """
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


def get_redirect_target():
    """
    Find a valid redirect target in a list of hints

    :returns: string, None
    :raises: None
    """
    for target in request.args.get("next"), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target


class RedirectForm(BaseForm, object):
    """
    Calls to handle forms that redirect
    """

    #: Field to hold the redirection information
    next = HiddenField()

    def __init__(self, *args, **kwargs):
        """
        Store a redirection url
        """
        super(RedirectForm, self).__init__(*args, **kwargs)
        if not self.next.data:
            self.next.data = get_redirect_target() or ""

    def redirect(self, endpoint="index", **values):
        """
        Get the redirection target
        """
        if is_safe_url(self.next.data):
            return redirect(self.next.data)
        target = get_redirect_target()
        return redirect(target or url_for(endpoint, **values))


class EmailDomain:
    """
    Class to test email domains
    """

    def __init__(self, required=None, banned=None, message=None):
        self.required = ["@" + x.lower() for x in required or []]
        self.banned = ["@" + x.lower() for x in banned or []]
        if message is None:
            message = gettext("Invalid email domain")
            if banned:
                message += gettext(" (domain cannot be {list})").format(
                    list=", ".join(banned)
                )
        self.message = message

    # noinspection PyUnusedLocal
    def __call__(self, form, field):
        value = field.data.lower()
        for domain in self.required:
            if not value.endswith(domain):
                raise ValidationError(self.message)
        for domain in self.banned:
            if value.endswith(domain):
                raise ValidationError(self.message)


class LDAPEmail:
    """
    Class to test whether email already exists in LDAP backend
    """

    # noinspection PyUnusedLocal
    def __call__(self, form, field):
        addr = field.data.lower()
        try:
            res = current_ldap.user_exists(dict(mail=addr))
        except LDAPException:
            raise ValidationError(
                "LDAP Error: Cannot confirm address '{}'".format(addr)
            )
        if res:
            raise ValidationError("Email address already exists in LDAP")


class NewUserForm(RedirectForm):
    firstName = StringField(
        gettext("First Name"),
        description=gettext("External user's first name"),
        validators=[],
    )

    lastName = StringField(
        gettext("Last Name"),
        description=gettext(
            "External user's last name or organization's descriptive name"
        ),
        validators=[
            DataRequired(),
        ],
    )

    email = StringField(
        gettext("Email"),
        description=gettext("External user's email address"),
        validators=[
            DataRequired(),
            Email(),
            EmailDomain(banned=current_app.config["INTERNAL_DOMAINS"]),
            LDAPEmail(),
        ],
    )

    username = StringField(
        gettext("Username (user ID)"),
        description=gettext(
            "Can contain letters, numbers, period '.', underscore '_' or dash '-'."
        ),
        validators=[
            DataRequired(),
            UsernameValidator(),
            Length(8, 20, gettext("Invalid length (8-20 characters)")),
        ],
    )
