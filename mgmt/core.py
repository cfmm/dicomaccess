"""
Created on Jul 22, 2015

Copyright 2015, The University of Western Ontario

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

import typing as t
import os
import pickle
import base64

from flask import Flask, request, url_for
from flask_cfmm import CFMMFlask
from flask_cfmm.configure import load_configuration
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_mail import Mail
from flask_celeryext import FlaskCeleryExt
from flask_babel import gettext

from .documentation import register
from .models import db
from .tasks import start_expiry
from .initialize import bp as initialize
from .oidc import create_client

cfmm = CFMMFlask()


class StartupFlask(Flask):
    def run(self, *args, **kwargs):
        if not self.testing:
            try:
                start_expiry(self)
            except Exception as err:
                self.logger.critical(f"Cannot start expiry task queue. f'{err}'")
        super().run(*args, **kwargs)


def create_app(
    config: str = None, mapping: t.Optional[t.Mapping[str, t.Any]] = None
) -> Flask:
    app = StartupFlask(__name__)

    load_configuration(app, config)

    # overwrite with mappings
    app.config.from_mapping(mapping)

    # set defaults
    app.config.setdefault("WTF_CSRF_ENABLED", True)

    app.config.setdefault("TITLE")
    app.config.setdefault("LDAPQUERY", dict())
    app.config.setdefault("DCM4CHEE5", list())
    for dcm4chee_config in app.config["DCM4CHEE5"]:
        dcm4chee_config.setdefault("verify", True)
        dcm4chee_config.setdefault("url", "https://localhost:8443/dcm4chee-arc")

    assert "USERMGMT" in app.config, "Missing USERMGMT configuration"
    usermgmt_config = app.config["USERMGMT"]
    assert "prefix" in usermgmt_config, "prefix required in USERMGMT configuration"
    assert "dicom_ou" in usermgmt_config, "dicom_ou required in USERMGMT configuration"
    assert "group_ou" in usermgmt_config, "group_ou required in USERMGMT configuration"
    assert (
        "user_prefix" in usermgmt_config
    ), "user_prefix required in USERMGMT configuration"
    assert usermgmt_config["user_prefix"] != "", "user_prefix cannot be empty string"
    usermgmt_config.setdefault("suffix", "")
    usermgmt_config.setdefault("offset", 0)

    app.config.setdefault(
        "INDEX_URL",
        next((x["url"] + "/ui2" for x in app.config["DCM4CHEE5"] if "url" in x), None),
    )

    app.config.setdefault("CFMM_DATACARE_ROLE", "datacare")

    app.config.setdefault("SCREENING", dict())
    app.config["SCREENING"].setdefault("contact_email", None)
    app.config["SCREENING"].setdefault("email_from", None)
    app.config["SCREENING"].setdefault("token_expiry", 30 * 24 * 60 * 60)
    app.config["SCREENING"].setdefault("datacare_emails", [])
    app.config["SCREENING"].setdefault("coordinator_emails", [])

    app.config.setdefault("INTERNAL_DOMAINS", [])
    app.config.setdefault("SESSION_TIMEOUT", 5)

    app.config.setdefault(
        "CELERY_BROKER_URL",
        os.environ.get("CELERY_BROKER_URL", "redis://localhost:6379/0"),
    )
    app.config.setdefault(
        "CELERY_RESULT_BACKEND",
        os.environ.get("CELERY_RESULT_BACKEND", "redis://localhost:6379/0"),
    )
    app.config.setdefault("CELERY_TASK_ALWAYS_EAGER", True)
    app.config.setdefault("CELERY_TASK_EAGER_PROPAGATES", True)
    app.config["CELERY_SEND_SENT_EVENT"] = True

    app.config.setdefault("SITE_NAME", app.config["TITLE"])

    app.config.setdefault(
        "RATELIMIT_STORAGE_URI",
        os.environ.get("RATELIMIT_STORAGE_URI", "memory://"),
    )
    app.config.setdefault(
        "RATELIMIT_DEFAULT", "80000/day;500/hour;120/minute;20/second"
    )
    app.config.setdefault("RATELIMIT_STRATEGY", "fixed-window")
    app.config.setdefault("RATELIMIT_STORAGE_OPTIONS", {"socket_connect_timeout": 30})

    # initialize configuration variables from config file in flask_cfmm
    from .models.credentials import User

    # noinspection PyUnresolvedReferences
    from .models.tasks import Status  # noqa: F401 needed for database models

    cfmm.client_factory = create_client
    cfmm.init_app(app, db, user_model=User)

    # user to call methods without return value
    app.jinja_env.add_extension("jinja2.ext.do")

    limiter = Limiter(key_func=get_remote_address)
    limiter.init_app(app)

    from .ldap import LDAP

    ldapquery = LDAP()
    app.extensions["ldap"] = ldapquery
    ldapquery.init_app(app)

    Mail(app=app)

    from .arc import ArcUpdate

    app.extensions["arc"] = ArcUpdate(app.config["DCM4CHEE5"])

    with app.app_context():
        from .admin import Admin as AdminCore
        from .admin_base import MgmtAdminIndexView
        from .screening.admin import ScreeningFormParticipantView
        from .models.screening import ScreeningForm

        with app.test_request_context():
            # noinspection PyUnresolvedReferences
            import mgmt.views

            # noinspection PyUnresolvedReferences
            import mgmt.events  # noqa: F401 needed for database models

    admin = AdminCore(
        app=app,
        name=app.config["TITLE"],
        index_view=MgmtAdminIndexView(
            name="",
            template="index.html",
            url="/",
        ),
        base_template="base.html",
    )

    admin.add_view(
        ScreeningFormParticipantView(
            ScreeningForm,
            db.session,
            endpoint="screening_participant",
            url="/screening/participant",
        )
    )

    from .api import bp, protector

    cfmm.protection.register(protector)

    app.extensions["csrf"].exempt(bp)
    app.register_blueprint(bp)

    register(app, admin, name="docs")

    default_render_template = app.extensions["security"].render_template

    def render_template_with_documentation(*args, **kwargs):
        kwargs["documentation"] = app.extensions["documentation"]
        return default_render_template(*args, **kwargs)

    # Ensure the login_user template can test for presence of documentation
    app.extensions["security"].render_template = render_template_with_documentation

    ext = FlaskCeleryExt()
    ext.init_app(app)

    @app.errorhandler(400)
    def handle_bad_request(exc):
        if request.method == "POST" and request.path.startswith(
            url_for("screening_participant.index_view")
        ):
            data = dict(form=request.form, url=request.url)
            app.logger.critical(
                gettext("Invalid participant screening form: {obj}").format(
                    obj=base64.b64encode(pickle.dumps(data))
                )
            )

        return exc

    app.register_blueprint(initialize)
    return app
