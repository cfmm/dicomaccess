$(document).on('adminFormReady', function () {
    // change date inputs' year select element to be 1900-current_year
    // Only select year
    $(':input[data-role], a[data-role]').each(function () {
        let $el = $(this);
        switch ($el.attr('data-role')) {
            case 'datepicker2':
                $el.datepicker({
                    format:  'yyyy',
                    minViewMode: 'years',
                    maxViewMode: 'years',
                    startView: 'years',
                    startDate: '1900',
                    endDate: moment().endOf('year').format('YYYY'),
                    });
                return true;
        }
    });

    $('.timezone-input').each(function () {
        $(this).val(moment().zone());
    });

    $('.timezone-guess').each(function () {
        $(this).val(moment.tz.guess());
    });
});
