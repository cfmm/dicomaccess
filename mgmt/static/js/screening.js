$(document).ready(function () {
    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function subjectSearch(element, attribute) {
        // Use text entered into `element` as an AJAX subject search query for `attribute`. Populate all subject fields
        // with selected result.
        if (element.length) {
            // noinspection JSUnusedGlobalSymbols
            element
                // don't navigate away from the field on tab when selecting an item
                .on("keydown", function (event) {
                    if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).autocomplete("instance").menu.active) {
                        event.preventDefault();
                    }
                })
                .autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: $subject_search_url,
                            dataType: "json",
                            data: {
                                [attribute]: request.term
                            },
                            success: function (data) {
                                response(data);
                            }
                        });
                    },
                    search: function () {
                        // custom minLength
                        let term = extractLast(this.value);
                        if (term.length < 2) {
                            return false;
                        }
                    },
                    focus: function () {
                        // prevent value inserted on focus
                        return false;
                    },
                    select: function (event, ui) {
                        // populate all subject fields
                        $("#subject_surname").val(ui.item.surname);
                        $("#subject_given_name").val(ui.item.given_name);
                        $("#subject_birthdate").val(ui.item.birthdate);
                        $("#subjectid").val(ui.item.subjectid);

                        return false;
                    }
                })
                .autocomplete("instance")._renderItem = function (ul, item) {
                // show results with some subject information
                if ("surname" in item){
                    return $("<li>")
                        .append("<div> <b>" + item.surname + "</b> "
                            + item.given_name + ", <i>" + item.birthdate + "</i>"
                            + (item.subjectid ? ", <b>" + item.subjectid + "</b>" : "") + "</div>")
                        .appendTo(ul);
                }
                else{
                    return ul;
                }
            };
        }
    }

    subjectSearch($(".subject_search_api"), "surname");
    subjectSearch($("#subjectid"), "subjectid");

    //control conditional visibility of elements

    // noinspection JSUnusedLocalSymbols
    $(".conditional-control ul li").each(function( index ) {
        $(this).change(function () {
            let input = $(this).find('input');
            const this_conditional = $(this).closest(".conditional");
            let show_value = "1";
            if (this_conditional.hasClass("hidden-on-yes"))
                show_value = "0";

            // only perform hiding/showing when the option is checked/selected
            if(input[0].checked) {
                if (input.val() === show_value) {
                    $(this).closest(".conditional").find(".conditional-target").show();
                    $(this).closest(".conditional").find(".conditional-target input").attr('required', '');
                } else {
                    $(this).closest(".conditional").find(".conditional-target").hide();
                    $(this).closest(".conditional").find(".conditional-target input").removeAttr('required');
                }
            }
        });
        // trigger the method to properly display existing records
        $(this).trigger('change');
    });

    // if nothing is selected, hide the conditional elements
    // noinspection JSUnusedLocalSymbols
    $(".conditional-control ul").each(function (index) {
        if ($(this).find('li input[checked]').length === 0) {
            $(this).closest(".conditional").find(".conditional-target").hide();
            $(this).closest(".conditional").find(".conditional-target input").removeAttr('required');
        }
    });


    function populate_select(select_element, option_list) {
        // Generates option tags for select_element to the values in key_list, with text values from value_dict
        select_element.empty();
        if (option_list) {
            option_list.forEach(function (element) {
                select_element.append($('<option>', {
                    value: element['id'],
                    text: element['value']
                }));
            });
        }
    }

    // On selection of principal, populate project list with projects that are associated with that principal
    let principal = $("#principal");
    principal.change(function () {
        let selected_principal = $(this).val();
        if (selected_principal !== '__None') {
            // disable the project element until it can be re-populated
            let s2id_project = $("#s2id_project");
            s2id_project.select2("enable", false);
            s2id_project.select2("val", "0");

            let s2id_entry = $("#s2id_entry");
            s2id_entry.select2("enable", false);
            s2id_entry.select2("val", "0");

            $.ajax({
                url: $project_search_url,
                dataType: "json",
                data: {
                    principal: selected_principal
                },
                success: function (data) {
                    let selector = $("#project");

                    // on success, populate the select element and enable it
                    populate_select(
                        selector,
                        data
                    );
                    $("#s2id_project").select2("enable", true);

                    // enabling the select2 element will select the first project on the list
                    // trigger a change event to make sure the participant_description is updated (see below)
                    selector.trigger("change");
                }
            });
        }
    });

    $("#project").change(function () {
        let selected_project = $(this).val();

        let s2id_entry = $("#s2id_entry");
        s2id_entry.select2("enable", false);
        s2id_entry.select2("val", "0");
        if (selected_project != null) {
            $.ajax({
                url: $entry_search_url,
                dataType: "json",
                data: {
                    project: selected_project,
                    timestamp: Date.now() / 1000.0,
                },
                success: function (data) {
                    let entry = $("#entry");
                    populate_select(
                        entry,
                        data
                    );
                    // ensure no entry is selected
                    entry.val([]);

                    $("#s2id_entry").select2("enable", true);
                }
            });

            // Populate participant description if that field is present
            if ($("#participant_description").length) {
                $.ajax({
                    url: $project_description_url,
                    dataType: "json",
                    data: {
                        id: selected_project
                    },
                    success: function (data) {
                        $("#participant_description").val(data.participant_description);
                    }
                });
            }
        }
    });

    $("#s2id_project").select2({
        tags: true
    });

    // Trigger a change event on the principal element to populate the project options
    // This is needed for displaying server-side messages on refresh when creating forms
    if ($('#project > option').length === 0)
        principal.trigger("change");

    // On input button click,
    // Change 'display:none' on the (hidden) select input to block
    // When display is none, there is no client-side input validation, so no error messages will appear and the field
    // won't be outlined in red.
    // see: https://stackoverflow.com/questions/34248898/how-to-validate-select-option-for-a-materialize-dropdown
    // (Doing this on document ready or in the css file breaks the select2 rendering)
    $("input.btn").on('click', function() {
        $("#project, #devices").css({display: "block", height: 0, padding: 0, width: 0, position: 'absolute'});
    });

});
