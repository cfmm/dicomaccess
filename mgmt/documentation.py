from pathlib import Path


def register(app, admin, name=None, path=None):
    docs = None

    name = name or "docs"
    config = app.config.get("DOCUMENTATION", dict())
    path = path or config.get("path", None)
    if path:
        doc_module = Path(path)
        import importlib.util
        import sys

        package_path = doc_module.joinpath("__init__.py")
        package_name = doc_module.name
        spec = importlib.util.spec_from_file_location(package_name, package_path)
        docs = importlib.util.module_from_spec(spec)
        sys.modules[package_name] = docs

        try:
            spec.loader.exec_module(docs)
        except FileNotFoundError:
            app.extensions["documentation"] = None
            return

        docs = docs.Docs(name=name)
        docs.init_app(app, admin, config=config)

    app.extensions["documentation"] = docs
