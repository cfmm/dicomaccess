import time

import requests
from flask import url_for, current_app
from flask_login import current_user
from flask_babel import gettext
from urllib.parse import urlparse, urlunparse


class KeycloakError(Exception):
    pass


def email(username):
    # Get admin token for keycloak
    # https://stackoverflow.com/questions/57393543/welcome-of-finish-registration-letter-in-keycloak

    client = current_app.extensions["security"].datastore.client

    # OIDC client not configured
    if not client:
        return

    # User must have role for creating users
    if not current_user.is_datacare:
        raise KeycloakError(gettext("Not authorized"))

    token = current_user.token.to_token()

    components = urlparse(client.load_server_metadata().get("issuer"))
    # noinspection PyProtectedMember
    components = components._replace(
        path=components.path.replace("/realms/", "/admin/realms/")
    )
    url = urlunparse(components)

    verify = False if current_app.debug else True

    # When user has just been created in LDAP, need to wait for Keycloak to be able to query for it
    # (Potentially from a different LDAP cluster member)
    # Wait for up to a minute
    for attempt_count in range(60):
        # Query for the user id
        resp = requests.get(
            url=f"{url}/users",
            params={
                "username": username,
                "exact": True,
            },
            verify=verify,
            headers={"Authorization": f"Bearer {token['access_token']}"},
        )

        if resp.status_code != 200:
            raise KeycloakError("Unable to query users")
        data = resp.json()
        if data:
            break
        time.sleep(1)
    else:
        raise KeycloakError("No user found")

    user_id = data[0]["id"]

    params = {
        "client_id": client.client_id,
        "lifespan": 60 * 60 * 24 * 2,  # two days
    }

    if current_app:
        params["redirect_uri"] = url_for("security.landing", _external=True)

    # Send keycloak verification email
    resp = requests.put(
        url=f"{url}/users/{user_id}/execute-actions-email",
        params=params,
        json=["UPDATE_PASSWORD", "VERIFY_EMAIL", "UPDATE_PROFILE"],
        verify=verify,
        headers={"Authorization": f"Bearer {token['access_token']}"},
    )

    if resp.status_code != 204:
        raise KeycloakError("Unable to send email")
