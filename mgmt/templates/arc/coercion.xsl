<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="xml"/>
  <xsl:template match="/NativeDicomModel">
    <NativeDicomModel>
      <xsl:variable name="performingPhysicianName" select="normalize-space(DicomAttribute[@tag='00081050']/PersonName[@number=1])"/>
      <xsl:if test="$performingPhysicianName = 'sphinx'">

        <xsl:apply-templates select="DicomAttribute"/>

        <!-- Patient Position -->
        <xsl:variable name="patientPosition" select="normalize-space(DicomAttribute[@tag='00185100']/Value)"/>
        <DicomAttribute tag="00185100" vr="CS">
          <Value number="1"><xsl:value-of select="$patientPosition"/>X</Value>
        </DicomAttribute>

        <!-- Delete the Performing Physician Name since it is a fake trigger that should be used once and only once -->
        <DicomAttribute tag="00081050" vr="PN">
        </DicomAttribute>
      </xsl:if>
    </NativeDicomModel>
  </xsl:template>

  <!-- 1 vectors -->
  <xsl:template match="DicomAttribute[@tag=('00200032', '300A012C', '00191012', '00191013', '00191014', '00191015')]">
    <!-- Image Position -->
    <!-- Isocenter Position -->
    <!-- TablePositionOrigin -->
    <!-- ImaAbsTablePosition -->
    <!-- ImaRelTablePosition -->
    <!-- SlicePosition_PCS -->
    <DicomAttribute tag="{@tag}" vr="{@vr}">
      <Value number="1"><xsl:value-of select="Value[@number=1]"/></Value>
      <Value number="2"><xsl:value-of select="-1 * Value[@number=3]"/></Value>
      <Value number="3"><xsl:value-of select="Value[@number=2]"/></Value>
    </DicomAttribute>
  </xsl:template>

  <!-- 2 vectors -->
  <xsl:template match="DicomAttribute[@tag='00200037']">
    <!-- Image Orientation -->
    <DicomAttribute tag="{@tag}" vr="{@vr}">
      <Value number="1"><xsl:value-of select="Value[@number=1]"/></Value>
      <Value number="2"><xsl:value-of select="-1 * Value[@number=3]"/></Value>
      <Value number="3"><xsl:value-of select="Value[@number=2]"/></Value>
      <Value number="4"><xsl:value-of select="Value[@number=4]"/></Value>
      <Value number="5"><xsl:value-of select="-1 * Value[@number=6]"/></Value>
      <Value number="6"><xsl:value-of select="Value[@number=5]"/></Value>
    </DicomAttribute>
  </xsl:template>

</xsl:stylesheet>
