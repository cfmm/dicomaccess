from . import create_app

app = create_app().extensions["flask-celeryext"].celery
