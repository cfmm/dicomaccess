from flask import redirect, url_for, request, current_app, flash
from flask_admin import AdminIndexView, expose
from flask_admin.menu import BaseMenu
from flask_babel import gettext
from flask_cfmm.model_views import AuthenticatedAccess, ModelView
from flask_login import current_user

from .proxy import current_ldap


class UserAccess(AuthenticatedAccess):
    def is_accessible(self):
        return super().is_accessible() and not current_user.is_datacare


class DatacareAccess(AuthenticatedAccess):
    def is_accessible(self):
        return (
            super().is_accessible()
            and current_user.is_datacare
            and not current_user.is_admin
        )


class UserDatacareAccess(AuthenticatedAccess):
    def is_accessible(self):
        return super().is_accessible() and not current_user.is_admin


class DatacareAdminAccess(AuthenticatedAccess):
    def is_accessible(self):
        return AuthenticatedAccess.is_accessible(self) and current_user.is_datacare


class AdminAccess(AuthenticatedAccess):
    """
    Class to restrict administrator access
    """

    def is_accessible(self):
        """
        Only authenticated admin users can access model view

        :return: boolean
        """
        return AuthenticatedAccess.is_accessible(self) and current_user.is_admin


class CoordinatorAccess(AuthenticatedAccess):
    def is_accessible(self):
        return (
            super(CoordinatorAccess, self).is_accessible()
            and not current_user.is_datacare
            and current_user.is_coordinator
        )

    def inaccessible_callback(self, name, **kwargs):
        # datacare users cannot access CoordinatorAccess endpoints,
        # but redirect them to the appropriate datacare endpoint
        if current_user.is_authenticated and current_user.is_datacare:
            # noinspection PyUnresolvedReferences
            return redirect(
                url_for("dc_" + self.endpoint + "." + name, **request.args, **kwargs)
            )
        return redirect(url_for("security.login", next=request.url))


class CoordinatorDatacareAccess(AuthenticatedAccess):
    def is_accessible(self):
        # datacare users are always coordinators
        return super(CoordinatorDatacareAccess, self).is_accessible() and (
            current_user.is_coordinator
        )


class MgmtAdminIndexView(AdminIndexView):
    @expose("/no_javascript")
    def no_javascript(self):
        flash("Javascript required")
        return redirect(url_for(".index"))

    @expose()
    @expose("/index")
    def index(self):
        """
        Create the default index page

        Redirect unauthenticated users to login
        """
        link_list = [
            {"url": current_app.config["INDEX_URL"], "text": "DICOM Data Browser"}
        ]

        if not current_user.is_authenticated:
            link_list.insert(
                0, {"url": url_for("security.login"), "text": "Manage Studies"}
            )

        for x in current_app.config.get("LINK_LIST", []):
            link_list.append(x)

        if current_user.is_authenticated:
            if current_app.extensions["documentation"]:
                link_list.append(
                    {"url": url_for("docs.page", page="index"), "text": gettext("Help")}
                )

        return self.render(
            self._template,
            title=current_app.config["TITLE"],
            readonly=current_ldap.config.get("readonly", False),
            user_creation=current_ldap.config.get("user_creation", True),
            link_list=link_list,
            documentation=current_app.extensions["documentation"],
        )

    def is_visible(self):
        return False


class MyModelView(ModelView):
    def _prettify_class_name(self, name):
        # noinspection PyProtectedMember
        return gettext(super(MyModelView, self)._prettify_class_name(name) + "s")

    def render(self, template, **kwargs):
        kwargs["documentation"] = current_app.extensions["documentation"]
        return super().render(template, **kwargs)

    def after_model_delete(self, model):
        return super().after_model_delete(model)

    def after_model_change(self, form, model, is_created):
        return super().after_model_change(form, model, is_created)


class ModelViewIndex(MyModelView):
    """
    Expose index of MyModelView
    """

    @expose("")
    def index(self):
        """
        Return index_view
        """
        return super(ModelViewIndex, self).index_view()


class DividerMenu(BaseMenu):
    """
    Bootstrap Menu divider item
    https://github.com/flask-admin/flask-admin/issues/1745#issuecomment-589110574
    """

    # noinspection PyUnusedLocal
    def __init__(
        self, name, class_name=None, icon_type=None, icon_value=None, target=None
    ):
        super(DividerMenu, self).__init__(
            name, "dropdown-divider", icon_type, icon_value, target
        )

    def get_url(self):
        pass

    # noinspection PyMethodMayBeStatic
    def is_visible(self):
        return current_user.is_authenticated and current_user.has_access

    # noinspection PyMethodMayBeStatic
    def is_accessible(self):
        return True


class DatacareAdminDividerMenu(DatacareAdminAccess, DividerMenu):
    pass


class OperationsDividerMenu(AuthenticatedAccess, DividerMenu):
    def is_accessible(self):
        return super().is_accessible() and current_user.is_datacare


class CoordinatorDividerMenu(CoordinatorAccess, DividerMenu):
    pass
