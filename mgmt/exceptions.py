class LdapDnError(ValueError):
    pass


class LdapNoMatchingEntry(ValueError):
    pass


class UserCreationError(Exception):
    pass


class EmailExists(UserCreationError):
    pass


class UsernameExists(UserCreationError):
    pass
