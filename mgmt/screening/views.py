import traceback
from datetime import datetime, timezone

from flask import redirect, url_for, flash, render_template, current_app, request
from flask_admin import expose, BaseView
from flask_babel import gettext
from flask_login import current_user

from .. import db
from ..admin_base import CoordinatorDatacareAccess
from ..emails import send_email
from ..models.screening import ScreeningForm, ScreeningState
from ..screening.admin import ScreeningFormView
from ..screening.forms import (
    CoordinatorParticipantSendForm,
    DatacareParticipantSendForm,
)


@current_app.route("/api/screening/entry/<int:entry_id>", methods=["GET"])
def linked_screening(entry_id):
    return redirect(
        url_for(
            "dc_screeningform.index_view",
            flt0_latest_equals=1,
            flt1_entry_id_equals=entry_id,
        )
    )


@current_app.route("/api/screening/form/<int:form_id>", methods=["GET"])
def screening_form(form_id):
    return redirect(url_for("dc_screeningform.edit_view", id=form_id))


class CoordinatorSendView(CoordinatorDatacareAccess, BaseView):
    @expose("/", methods=("GET", "POST"))
    def index_view(self):
        """Endpoint for creating forms and sending token links to participants"""
        if current_user.is_datacare:
            form = DatacareParticipantSendForm()
        else:
            form = CoordinatorParticipantSendForm()

        if form.validate_on_submit():
            # noinspection PyBroadException
            try:
                # Generate a new empty form in 'created' state
                screening = ScreeningForm(
                    create_user=current_user,
                    create_ip=request.remote_addr,
                    create_time=datetime.now(timezone.utc),
                    state=ScreeningState.created,
                    q_coordinator_comments=gettext("Sent to {email}.").format(
                        email=form.email.data
                    ),
                    version=ScreeningFormView.current_version(),
                )

                # assign the project from the form project
                with db.session.no_autoflush:
                    # noinspection PyProtectedMember
                    screening.project = form.project.data
                    if isinstance(form, DatacareParticipantSendForm):
                        screening.entry = form.entry.data

                # commit to generate an id
                db.session.add(screening)
                db.session.commit()
            except Exception:
                current_app.logger.error(
                    gettext(
                        "Could not create a blank screening form to send to a participant. Traceback: {traceback}"
                    ).format(traceback=traceback.format_exc())
                )
                flash(gettext("Could not create a blank screening form."), "error")
                return redirect(url_for(".index_view"))

            # noinspection PyBroadException
            try:
                # Generate a token
                token, expiry = screening.get_token(email=form.email.data)
                token_url = url_for(
                    "screening_participant.token", token=token, _external=True
                )
                expiry = expiry.strftime("%B %d %Y at %H:%M UTC")

                # Send email to coordinator and BCC the participant
                template_params = dict(
                    contact_email=current_app.config["SCREENING"]["email_from"],
                    expiry=expiry,
                    token_url=token_url,
                    coordinator=form.coordinator.data,
                    coordinator_phone=form.coordinator_phone.data,
                    project=screening.project,
                    study_details=form.study_details.data,
                    coordinator_email=current_user.email,
                    site_name=current_app.config["SITE_NAME"],
                )
                send_email(
                    gettext(
                        "{coordinator} is sending you an MRI Screening Form"
                    ).format(coordinator=form.coordinator.data),
                    current_app.config["SCREENING"]["email_from"],
                    [
                        form.email.data,
                    ],
                    render_template(
                        "email/participant_screening.txt", **template_params
                    ),
                    render_template(
                        "email/participant_screening.html", **template_params
                    ),
                    cc=[
                        current_user.email,
                    ],
                    send_async=False,
                    reply_to=current_user.email,
                )
            except Exception:
                # On failure, delete the new form
                db.session.delete(screening)
                db.session.commit()
                current_app.logger.error(
                    gettext(
                        "coordinator token create or email send failed. Traceback: {traceback}"
                    ).format(traceback=traceback.format_exc())
                )
                flash(
                    gettext("Could not send a screening form to participant."), "error"
                )
            else:
                flash(
                    gettext(
                        "Screening form creation successful. Link has been sent to participant's email address: {}. "
                        "You have been CCed on this email as well.".format(
                            form.email.data
                        )
                    )
                )
            return redirect(url_for(".index_view"))

        return self.render("screening/participant_send.html", form=form)
