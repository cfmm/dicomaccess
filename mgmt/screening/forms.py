from flask_admin.contrib.sqla.fields import QuerySelectField
from flask_babel import gettext
from flask_wtf import FlaskForm
from wtforms.fields import StringField, EmailField, TelField
from wtforms.validators import DataRequired, Email, InputRequired
from wtforms.widgets import TextArea

from .admin import (
    DynamicQuerySelectField,
    make_project_query,
    make_entry_query,
    make_principal_query,
)
from ..models.dicom import Project
from ..mrbs.models import Entry as MRBSEntry


class CoordinatorParticipantSendForm(FlaskForm):
    email = EmailField(
        gettext("E-mail:"),
        description=gettext("Participant's email address"),
        validators=[DataRequired(), Email()],
    )

    principal = QuerySelectField(
        gettext("Principal"),
        query_factory=make_principal_query,
        allow_blank=True,
    )

    project = DynamicQuerySelectField(
        label=gettext("Project"),
        model=Project,
        query_factory=make_project_query,
        validators=[InputRequired()],
    )

    participant_description = StringField(
        gettext("Description"),
        description=gettext(
            "Study description for participants. This can be edited on the project edit page."
        ),
        render_kw={"readonly": True},
        widget=TextArea(),
    )

    study_details = StringField(
        gettext("Additional study details:"),
        description=gettext(
            "Any other study details you want to share with the participant to "
            "help them understand why they are getting this email."
        ),
        widget=TextArea(),
    )

    coordinator = StringField(gettext("Coordinator name:"), validators=[DataRequired()])

    coordinator_phone = TelField(
        gettext("Coordinator phone:"),
        description=gettext(
            "Coordinator's phone number, format: '111-111-1111' or '111-111-1111 x 11111'"
        ),
        render_kw={
            "pattern": r"\(?[0-9]{3}\)?-[0-9]{3}-[0-9]{4}( (x|ext)(\.)? [0-9]{1,6})?"
        },
    )


class DatacareParticipantSendForm(CoordinatorParticipantSendForm):
    entry = DynamicQuerySelectField(
        label=gettext("Scheduled"),
        model=MRBSEntry,
        query_factory=make_entry_query,
        allow_blank=True,
    )
