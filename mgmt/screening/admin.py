import time
from collections import OrderedDict
from datetime import datetime, timezone
from urllib.parse import parse_qs, urlencode, urlparse
import sqlalchemy as sa

from flask import (
    redirect,
    url_for,
    request,
    flash,
    render_template,
    get_flashed_messages,
    session,
    current_app,
)
from flask_admin import expose
from flask_admin.actions import action as action_decorator
from flask_admin.contrib.sqla.fields import QuerySelectField
from flask_admin.contrib.sqla.form import AdminModelConverter
from flask_admin.form import DatePickerWidget, FormOpts
from flask_admin.helpers import is_form_submitted, get_redirect_target
from flask_admin.model import filters
from flask_admin.model.form import converts
from flask_admin.model.helpers import get_mdict_item_or_list
from flask_babel import gettext
from flask_login import current_user
from markupsafe import Markup
from wtforms import StringField, RadioField, fields, SelectMultipleField
from wtforms.validators import Length, DataRequired, InputRequired, StopValidation
from flask_cfmm.filters import CFMMFilterConverter

from .. import db
from ..admin_base import MyModelView, DatacareAdminAccess, CoordinatorAccess
from ..emails import send_email
from ..models.tokens import InvalidToken, MalformedToken
from ..models.dicom import Group, Project, Principal
from ..models.credentials import MembershipType
from ..models.screening import ScreeningForm, ScreeningState, Device, Subject
from ..mrbs.models import Entry as MRBSEntry
from ..user_reports import Mail


def make_principal_query():
    principal_ids = Project.accessible_to(
        current_user, level=MembershipType.coordinator
    ).with_entities(Project.principal_id)
    return Principal.query.filter(Principal.id.in_(principal_ids)).options(
        db.load_only(Principal.id, Principal.name)
    )


def make_project_query(obj, form):
    if obj is None:
        try:
            principal_id = form.principal.data.id
        except (TypeError, AttributeError):
            return Project.query.limit(0)
    else:
        principal_id = obj.principal_id

    return (
        Project.accessible_to(current_user, level=MembershipType.coordinator)
        .filter(Project.principal_id == principal_id)
        .options(db.load_only(Project.id, Project.study))
    )


def make_entry_query(obj, form):
    if obj is None:
        try:
            project_id = form.project.data.id
            entry_id = None
        except (TypeError, AttributeError):
            return MRBSEntry.query.filter(db.false())
    else:
        project_id = obj.project_id
        entry_id = obj.id

    return make_mrbs_query(project_id, entry_id=entry_id)


def make_mrbs_query(project_id, timestamp=None, entry_id=None):
    if project_id is None:
        return MRBSEntry.query.filter(db.false())

    if not timestamp:
        timestamp = int(time.time())

    if entry_id:
        sql_filter = db.or_(
            MRBSEntry.end_time > timestamp, MRBSEntry.id == int(entry_id)
        )
    else:
        sql_filter = MRBSEntry.end_time > timestamp

    sql_filter = db.and_(sql_filter, MRBSEntry.project_id == int(project_id))

    return MRBSEntry.query.filter(sql_filter).order_by(MRBSEntry.start_time)


class RequiredRadioField(RadioField):
    """Adds the HTML5 required attribute to radio field to enable client/browser side input validation."""

    # noinspection PyProtectedMember
    class _Option(RadioField._Option):
        def __init__(self, *args, **kwargs):
            kwargs["render_kw"] = {"required": True}
            super(RequiredRadioField._Option, self).__init__(*args, **kwargs)


class RequiredRadioFieldNoPrefill(RequiredRadioField):
    """Radio field that's required, but not pre-filled for coodinators/datacare"""

    pass


class RequiredRadioFieldNoValidate(RequiredRadioFieldNoPrefill):
    """Radio field that's required, not pre-filled for coodinators/datacare,
    and possibly set to the default not-selected value on submit (i.e. it's conditionally required).
    """

    pass


# noinspection PyAbstractClass
class ScreeningFormModelConverter(AdminModelConverter):
    """
    Custom column type mapper
    """

    # noinspection PyUnusedLocal
    @staticmethod
    def boolean_converter(field_args):
        """Map QBoolean columns to a radio field with Yes/No answers"""
        field_args["render_kw"] = {"class": "screening-qboolean"}
        return dict(choices=[(1, "Yes"), (0, "No")], coerce=int, **field_args)

    # noinspection PyUnusedLocal
    @converts("QBoolean")
    def convert_qboolean(self, field_args, **extra):
        return RequiredRadioField(
            **ScreeningFormModelConverter.boolean_converter(field_args)
        )

    # noinspection PyUnusedLocal
    @converts("QBooleanNoPrefill")
    def convert_qboolean_no_prefill(self, field_args, **extra):
        return RequiredRadioFieldNoPrefill(
            **ScreeningFormModelConverter.boolean_converter(field_args)
        )

    # noinspection PyUnusedLocal
    @converts("QBooleanNoValidate")
    def convert_qboolean_no_validate(self, field_args, **extra):
        return RequiredRadioFieldNoValidate(
            **ScreeningFormModelConverter.boolean_converter(field_args),
            validate_choice=False,
        )

    # noinspection PyUnusedLocal
    @converts("Date")
    def convert_date(self, field_args, **extra):
        return DateField(**field_args)

    @converts("QString")
    def convert_qstring(self, column, field_args, **extra):
        """Add a class identifier to allow for custom style rendering"""
        field_args["render_kw"] = {"class": "screening-qstring"}
        return self.conv_String(column, field_args, **extra)

    @converts("QText")
    def convert_qtext(self, field_args, **extra):
        """Add a class identifier to allow for custom style rendering"""
        field_args["render_kw"] = {"class": "screening-qtext"}
        return self.conv_Text(field_args, **extra)


class ScreeningFormFilterConverter(CFMMFilterConverter):
    @filters.convert("enum")
    def conv_enum(self, column, name, options=None, **kwargs):
        """
        Overload enum converter. Specify enum class to enable proper composition of SQLAlchemy queries
        """
        flts = super().conv_enum(column, name, options, **kwargs)
        for flt in flts:
            flt.enum_class = column.type.enum_class
        return flts


class DateField(fields.DateField):
    """
    Allows modifying the datetime.date format of a DateField using form_args.
    """

    widget = DatePickerWidget()

    def __init__(self, label=None, validators=None, format_=None, **kwargs):
        """
        Constructor

        :param label:
            Label
        :param validators:
            Field validators
        :param format:
            Format for text to date conversion. Defaults to '%Y-%m-%d'
        :param kwargs:
            Any additional parameters
        """
        super(DateField, self).__init__(label, validators, format_, **kwargs)

        self.format = format_ or "%Y-%m-%d"

        if not isinstance(self.format, list):
            self.format = [self.format]


class ScreeningFormAction:
    """
    Screening form action object

    Used for describing a form submit button associated with an action, as well as notifications

    :param new_state:
        The ScreeningFormState of the ScreeningForm after this action
    :param submit_style:
        Boostrap CSS class to apply to the submit button.
        For more details, see https://getbootstrap.com/docs/3.4/components/#btn-dropdowns
    :param submit_label:
        Button label
    :param action:
        Optionally specify the action name. Set to value of new_state by default.
    :param notify:
        Notify user type. Valid values are 'coordinator' and 'datacare', or a list with both values.
        No notification email sent if set to None.
    :param notify_message:
        Optionally specify a notification message to include in an email.
    :param confirm_message:
        Optionally specify a confirm message that's displayed to the user in a dialog box when the button is clicked
        By default, generic message is used, identifying the state and notify user type.

    """

    def __init__(
        self,
        new_state,
        submit_style,
        submit_label,
        action=None,
        notify=None,
        notify_message=None,
        confirm_message=None,
    ):
        if not action:
            action = str(new_state)
        self.action = action
        self.new_state = new_state
        self.submit_label = submit_label
        self.submit_style = submit_style

        if notify:
            if type(notify) is str:
                notify = [
                    notify,
                ]
            for notify_type in notify:
                if notify_type not in ("coordinator", "datacare"):
                    raise RuntimeError(
                        gettext("Invalid notify user type {notify_type}").format(
                            notify_type=notify_type
                        )
                    )
        self.notify = notify

        self.notify_message = notify_message
        self.confirm_message = confirm_message

    def render_submit(self):
        attrs = {
            "type": "submit",
            "class": "btn btn-" + self.submit_style,
            "name": self.action,
            "value": self.submit_label,
        }
        if self.notify or self.confirm_message:
            confirm_message = (
                self.confirm_message
                or "This action will mark the form as \\'{new_state}\\' and send an email notification "
                "to {notify}. Are you sure?".format(
                    new_state=self.new_state, notify=" and ".join(self.notify)
                )
            )
            attrs["onclick"] = "return confirm('{}')".format(confirm_message)
        return Markup(
            "<input "
            + " ".join('{}="{}"'.format(key, attrs[key]) for key in attrs)
            + " />"
        )

    def send_notification(self, model):
        if self.notify and current_app.config["SCREENING"]["email_from"]:
            # Send email
            template_params = dict(
                subject=model.subject,
                study_name=model.project.study,
                modify_user=model.create_user.username,
                modify_user_email=model.create_user.email,
                screening_form_url=url_for(
                    "screeningform.edit_view", id=model.id, _external=True
                ),
                notify_description=self.notify_message,
                history=model.history,
            )
            notification_addresses = ScreeningFormView.notification_addresses(
                self.notify, model
            )
            if None in notification_addresses:
                flash(
                    gettext(
                        "One or more coordinator users does not have a valid email address. "
                        "They will not get a notification."
                    ),
                    "error",
                )
                notification_addresses = [e for e in notification_addresses if e]
            if notification_addresses:
                send_email(
                    gettext("{name} - changes made to an MRI Screening Form").format(
                        name=current_app.config["SITE_NAME"]
                    ),
                    current_app.config["SCREENING"]["email_from"],
                    notification_addresses,
                    render_template("email/screening_notify.txt", **template_params),
                    render_template("email/screening_notify.html", **template_params),
                    send_async=False,
                )

    def set_new_state(self, model):
        model.state = self.new_state


# noinspection PyAbstractClass
class DynamicQuerySelectField(QuerySelectField):
    """Disable pre_validate on the Query Select field.

    When creating a new form, the field will be empty on page load and will only be populated once the user
    selects other fields.
    """

    def __init__(self, model=None, _form=None, **kwargs):
        super(DynamicQuerySelectField, self).__init__(_form=_form, **kwargs)
        self.model = model
        self._form = _form

    def _get_data(self):
        return self._data

    def _set_data(self, data):
        self._data = data
        self._formdata = None
        # Based on the data given determine the query to get acceptable values
        self.query = (
            self.query_factory(data, self._form) if self.query_factory else None
        )

    def process_formdata(self, valuelist):
        super(DynamicQuerySelectField, self).process_formdata(valuelist)
        if self._formdata is not None:
            # noinspection PyArgumentList
            self._set_data(db.session.get(self.model, self._formdata))

    # Overloading _get_data and _set_data, does not overload property the Parent class still calls its own
    # _get_data and _set_data methods. Actually it is worse in that sometimes it calls the parent methods and
    # sometimes the child methods. Basically it is broken in Python 3.9, so just override the property entirely.
    data = property(_get_data, _set_data)


class ScannerField(SelectMultipleField):
    """Disable pre_validate on the Select2 field.

    Field can be not-populated when filled by coordinator or participant
    """

    # noinspection PyUnusedLocal, PyMethodMayBeStatic
    def pre_validate(self, form):
        """Prevent 'not a valid choice' error"""
        return True


class ScreeningFormView(MyModelView):
    # allows displaying of many-to-many relations in details view
    column_display_all_relations = True

    # Displays navigation in ScreeningForm view. hidden in participant view
    screening_hide_navigation = False

    # Custom column type mapper
    model_form_converter = ScreeningFormModelConverter

    # Custom filter converter
    filter_converter = ScreeningFormFilterConverter()

    can_view_details = True

    action_disallowed_list = ("delete",)

    # fields needed for 'inline' subject creation
    _subject_fields = OrderedDict(
        (
            (
                "subject_surname",
                StringField(
                    gettext("Surname"), validators=[DataRequired(), Length(1, 64)]
                ),
            ),
            (
                "subject_given_name",
                StringField(
                    gettext("Given name"), validators=[DataRequired(), Length(1, 64)]
                ),
            ),
            (
                "subject_birthdate",
                DateField(
                    gettext("Year of birth"), validators=[DataRequired()], format_="%Y"
                ),
            ),
            ("subjectid", StringField(gettext("Subject ID"))),
        )
    )

    form_widget_args = {
        "subject_birthdate": {
            "data-role": "datepicker2",
        }
    }

    # extra fields for logged in users include project fields and scanner field
    form_extra_fields = OrderedDict(
        (
            (
                "principal",
                QuerySelectField(
                    gettext("Principal"),
                    query_factory=make_principal_query,
                    allow_blank=True,
                ),
            ),
        ),
        **_subject_fields,
    )

    form_overrides = dict(
        project=DynamicQuerySelectField, entry=DynamicQuerySelectField
    )

    column_list = (
        "state",
        "subject",
        "project",
        "create_time",
        "devices",
        "latest",
        "create_user",
        "entry",
    )

    column_details_list = (
        "subject",
        "state",
        "version",
        "project",
        "create_time",
        "create_user",
        "original_create_time",
        "entry",
    )

    # second element in tuple sets sorting order to descending (last modified entry first)
    column_default_sort = ("create_time", True)

    column_searchable_list = (
        Subject.given_name,
        Subject.surname,
        Subject.subjectid,
        Project.study,
        "q_coordinator_comments",
        "q_mr_tech_comments",
        "q_participant_comments",
    )

    column_sortable_list = (
        ("subject", "subject.surname"),
        ("project", "project.study"),
        ("create_user", "create_user.username"),
        "create_time",
        "state",
        "latest",
    )

    column_filters = (
        "create_time",
        "create_user.username",
        Project.study,
        "state",
        "devices.name",
        "latest",
        "entry_id",
        Subject.given_name,
        Subject.surname,
        Subject.subjectid,
    )

    # Numbered and cryptic otherwise
    named_filter_urls = True

    column_labels = {
        "create_user": gettext("Last edited by"),
        "create_time": gettext("Last modified"),
        "original_create_time": gettext("Created"),
        "project": gettext("Study"),
        "q_coordinator_comments": "Coordinator Comments",
        "q_mr_tech_comments": "MR Tech Comments",
        "q_participant_comments": "Comments",
        "entry": gettext("Scheduled"),
    }

    # Use state label
    column_formatters = {
        "state": lambda v, c, m, p: m.state.str(),
        "entry": lambda v, c, m, n: m.entry.link if m.entry else "",
    }

    # These columns are not user-defined
    form_excluded_columns = ("create_time", "create_user", "subject", "state")

    # controls which sections of the screening form are visible
    # show all fields, only datacare fields editable
    show_fields = "datacare"

    create_template = "screening/create.html"
    edit_template = "screening/edit.html"
    details_template = "screening/details.html"
    list_template = "screening/list.html"

    @property
    def extra_css(self):
        return [
            url_for("static", filename="css/screening.css"),
            "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css",
        ]

    @property
    def extra_js(self):
        return [
            url_for("static", filename="js/screening.js"),
            "https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js",
        ]

    @expose("/edit/", methods=("GET", "POST"))
    def edit_view(self):
        # look up the model corresponding to the ID
        return_url = get_redirect_target() or self.get_url(".index_view")

        if not self.can_edit:
            return redirect(return_url)

        id_ = get_mdict_item_or_list(request.args, "id")
        if id_ is None:
            return redirect(return_url)

        model = self.get_one(id_)

        # cannot edit a non-latest row, view its details instead
        if model and not model.latest:
            return redirect(url_for(".details_view", id=id_))

        return super(ScreeningFormView, self).edit_view()

    @expose("/delete/", methods=("POST",))
    def delete_view(self):
        # Cannot delete screening forms. Can only delete the archived state version, thus unarchiving the form
        form = self.delete_form()
        user_id = form.id.data
        model = self.get_one(user_id)
        if model.state != ScreeningState.archived:
            flash(gettext("Cannot delete screening forms."), "error")
            return redirect(get_redirect_target() or self.get_url(".index_view"))

        delete_rv = super(ScreeningFormView, self).delete_view()

        # Replace default record deletion message with wording about un-archiving
        flashes = list()
        for category, message in get_flashed_messages(with_categories=True):
            if category == "success" and message == gettext(
                "Record was successfully deleted."
            ):
                message = "Screening form successfully un-archived."
            flashes.append((category, message))
        session["_flashes"] = flashes

        return delete_rv

    def edit_form(self, obj=None):
        form = super(ScreeningFormView, self).edit_form(obj)

        # Populate the 'inline' subject and project fields when the form is being created for display
        if not is_form_submitted():
            if obj.subject:
                form.subject_surname.data = obj.subject.surname
                form.subject_given_name.data = obj.subject.given_name
                form.subject_birthdate.data = obj.subject.birthdate
                form.subjectid.data = obj.subject.subjectid

            if current_user.is_authenticated:
                if "principal" in form:
                    form.principal.data = obj.project.principal

        return form

    def on_model_change(self, form, model, is_created=False):
        # assign a subject
        # query Subject table for all info entered into subject fields
        # only query active subjects
        if form.subjectid.data is not None and len(form.subjectid.data) == 0:
            form.subjectid.data = None

        # event flushes on SQLAlchemy operations
        with db.session.no_autoflush:
            # noinspection PyUnresolvedReferences
            selected_subject = Subject.query.filter(
                db.and_(
                    Subject.surname.ilike("%s" % form.subject_surname.data),
                    Subject.given_name.ilike("%s" % form.subject_given_name.data),
                    db.extract("year", Subject.birthdate)
                    == sa.cast(
                        datetime.strftime(form.subject_birthdate.data, "%Y"), sa.Integer
                    ),
                    (
                        Subject.subjectid.ilike("%s" % form.subjectid.data)
                        if form.subjectid.data
                        else Subject.subjectid.is_(None)
                    ),
                )
            ).one_or_none()

            if selected_subject:
                selected_subject.active = True
                db.session.add(selected_subject)
                model.subject = selected_subject
            else:
                new_subject = Subject(
                    given_name=form.subject_given_name.data,
                    surname=form.subject_surname.data,
                    birthdate=form.subject_birthdate.data,
                    subjectid=form.subjectid.data,
                    active=True,
                )
                db.session.add(new_subject)

                old_subject = model.subject

                # If old subject only had this screening form associated with it
                if (
                    old_subject
                    and len(old_subject.screening_forms) == 1
                    and old_subject.screening_forms[0] == model
                ):
                    # mark the subject as inactive
                    old_subject.active = False
                    db.session.add(old_subject)

                model.subject = new_subject

            # assign values to auto-populated columns
            model.create_time = datetime.now(timezone.utc).replace(microsecond=0)
            model.create_ip = request.remote_addr
            model.version = form.version.data

            if form.subjectid.data == "":
                form.subjectid.data = None

            # ensure the scheduler entry is not associated with the screening form after the project changes
            if (
                form.entry
                and form.entry.data
                and form.project.data != form.entry.data.project
            ):
                model.entry = None

            # Assign a new state from request if it's on the list of valid state transitions
            for action in self.screening_form_actions:
                if action in request.form:
                    self.screening_form_actions[action].set_new_state(model)
                    return
            raise RuntimeError(
                gettext("Invalid screening form state for ") + __class__.__name__
            )

    # noinspection PyUnusedLocal
    def after_model_change(self, form, model, is_created):
        # Send notification
        for action in self.screening_form_actions:
            if action in request.form:
                notification = self.screening_form_actions[action].send_notification
                if notification:
                    # after_model_change is called after the model changes were committed to DB
                    # therefore, if a new version has been created, information used to send the notification should
                    #   be obtained from that new version.
                    new_version = ScreeningForm.query.filter(
                        ScreeningForm.parent_form_id == model.id
                    ).one_or_none()
                    notification(new_version or model)

    @staticmethod
    def current_version():
        # read the screening form version from the template versions directory
        return current_app.jinja_env.get_template(
            name="screening/versions/current.html"
        ).module.current_version

    # define filters to be appended to the index_view URL by default
    # (column, operation, value), column must be in column_filters, operation & value must be valid for column type
    default_filters = (
        # by default, only show latest revision of forms that are not in the archived state
        ("latest", "equals", 1),
        ("state", "not_in_list", "archived"),
    )

    @staticmethod
    def notification_addresses(notify_types, model):
        """Returns a list of recipients of action notification emails"""
        addresses = []
        for notify_type in notify_types:
            if notify_type == "coordinator":
                addresses += current_app.config["SCREENING"]["coordinator_emails"] or [
                    model.last_coordinator.email,
                ]
            elif notify_type == "datacare":
                if current_app.config["SCREENING"]["datacare_emails"]:
                    addresses += current_app.config["SCREENING"]["datacare_emails"]
                else:
                    group = Group.query.filter(
                        db.func.lower(Group.name)
                        == db.func.lower(current_app.config["CFMM_DATACARE_ROLE"])
                    ).one_or_none()
                    if group:
                        addresses += [user.email for user in group.users]
            else:
                raise RuntimeError(f"Unknown notification address type {notify_type}")
        return addresses

    def render(self, template, **kwargs):
        """
        Remove filter query params from clear_search_url
        These params are added to the default view URL in get_url below.
        They need to be skipped in clear_search_url because it's used as the "Reset" button URL.
        """
        if "clear_search_url" in kwargs:
            parsed_url = urlparse(kwargs["clear_search_url"])
            clear_search_url_params = parse_qs(parsed_url.query)
            for key in list(clear_search_url_params):
                # noinspection PyTypeChecker
                if key.startswith("flt0"):
                    del clear_search_url_params[key]

            # _replace is not private
            # " To prevent conflicts with field names, the method and attribute names start with an underscore."
            # https://docs.python.org/3.8/library/collections.html#namedtuple-factory-function-for-tuples-with-named-fields
            # noinspection PyProtectedMember
            parsed_url = parsed_url._replace(
                query=urlencode(clear_search_url_params, doseq=True)
            )
            kwargs["clear_search_url"] = parsed_url.geturl()

        return super(ScreeningFormView, self).render(template, **kwargs)

    def get_url(self, endpoint, **kwargs):
        """Use the default_filters variable to append arguments to the index_view URL"""
        if (
            endpoint.startswith(self.endpoint + ".") or endpoint.startswith(".")
        ) and endpoint.endswith("index_view"):
            count = 0
            for default_filter in self.default_filters:
                kwargs[
                    "flt{}_{}_{}".format(count, default_filter[0], default_filter[1])
                ] = str(default_filter[2])
                count += 1
        return super(ScreeningFormView, self).get_url(endpoint, **kwargs)

    def get_subject_fields(self, form):
        """Returns a list of subject fields"""
        return (
            [getattr(form, k) for k in self._subject_fields]
            if self._subject_fields
            else None
        )

    def _get_screening_actions(self, state):
        """Return a list of ScreeningFormAction objects defining the submit buttons to place on the screening form
        edit/create page

        """
        return []

    screening_form_actions = []

    def get_screening_form_actions(self, *actions):
        actions = [str(_action) for _action in actions]
        return [
            self.screening_form_actions[k]
            for k in self.screening_form_actions
            if self.screening_form_actions[k].action in actions
        ]

    # make the ScreeningState enum available inside Jinja template by making it a class variable
    _screening_state = ScreeningState


class ProjectValid:
    def __init__(self, message=None):
        self.message = message

    def __call__(self, form, field):
        if not (
            current_user.is_datacare
            or field.data
            in Project.accessible_to(
                current_user, level=MembershipType.coordinator
            ).all()
        ):
            if self.message is None:
                message = field.gettext("Project not authorized")
            else:
                message = self.message

            field.errors[:] = []
            raise StopValidation(message)


class ScreeningFormCoordinatorDatacareView(ScreeningFormView):
    # Assigning subject_search_api class to subject_surname field enables subject search API
    # It is not enabled in participant view
    form_widget_args = {
        "subject_surname": {"extra_classes": "subject_search_api"},
        "subject_birthdate": {
            "data-role": "datepicker2",
        },
    }

    form_args = dict(
        project=dict(
            label=gettext("Project"),
            model=Project,
            query_factory=make_project_query,
            allow_blank=True,
            validators=[DataRequired(), ProjectValid()],
        ),
        entry=dict(
            label=gettext("Scheduled"),
            model=MRBSEntry,
            query_factory=make_entry_query,
            allow_blank=True,
        ),
    )

    screening_form_actions = {
        "coordinator_review_save": ScreeningFormAction(
            new_state=ScreeningState.coordinator_review,
            submit_style="primary",
            submit_label=gettext("Save"),
            action="coordinator_review_save",
        ),
        "datacare_review_from_coordinator_review": ScreeningFormAction(
            new_state=ScreeningState.datacare_review,
            submit_style="success",
            submit_label=gettext("Submit for MR tech review"),
            action="datacare_review_from_coordinator_review",
            notify="datacare",
            notify_message=gettext(
                "The form has been submitted by a coordinator for MR technologist"
                " (datacare) approval."
            ),
            confirm_message=gettext(
                "This will send the form to an MR technologist for review. Are you sure?"
            ),
        ),
    }

    def validate_form(self, form):
        """
        Return False if the form is being cloned (POSTed to create_view) to allow editing
        """
        if "_clone_from_completed" in request.form:
            return False

        return super(ScreeningFormCoordinatorDatacareView, self).validate_form(form)

    def create_form(self, obj=None):
        form = super(ScreeningFormCoordinatorDatacareView, self).create_form(obj)

        # Pre-populate the boolean fields with "no" when creating a form as datacare
        if not is_form_submitted():
            for field in form:
                # RequiredRadioField is 1-to-1 mapped to QBoolean database column type
                if type(field) is RequiredRadioField:
                    field.data = 0

        # don't copy the parent form and scheduled entry references from the clone parent
        if "_clone_from_completed" in request.form:
            form.parent_form.data = None
            form.entry.data = None
        return form

    @expose("/edit/", methods=("GET", "POST"))
    def edit_view(self):
        """
        Redirect to a create view if the form is being cloned
        """

        if "_clone_from_completed" in request.form:
            flash(
                gettext(
                    "This is a cloned form. Please review answers with participant before submitting."
                ),
                "warning",
            )

            # Redirect to create_view with a temporary redirect code 307
            # This preserves the method (POST)
            return redirect(url_for(".create_view"), code=307)

        return super(ScreeningFormCoordinatorDatacareView, self).edit_view()

    def on_model_change(self, form, model, is_created=False):
        # no auto-flush to prevent flushes on SQLAlchemy operations
        with db.session.no_autoflush:
            model.create_user = current_user

        super(ScreeningFormCoordinatorDatacareView, self).on_model_change(
            form, model, is_created
        )

    @action_decorator(
        "archive",
        "Archive",
        "Are you sure you want to archive the selected form(s)?"
        " Archived forms are hidden from default view.",
    )
    def action_archive(self, ids):
        try:
            ids = [int(x) for x in ids]
            selected_forms = ScreeningForm.query.filter(ScreeningForm.id.in_(ids))

            count_archived = 0
            count_unchanged = 0
            for form in selected_forms.all():
                if form.state == ScreeningState.archived:
                    count_unchanged += 1
                else:
                    form.state = ScreeningState.archived
                    db.session.add(form)
                    count_archived += 1

            flash_message = list()
            if count_archived > 0:
                db.session.commit()
                flash_message.append(
                    gettext(
                        "Screening forms successfully archived: {count_archived}"
                    ).format(count_archived=count_archived)
                )
            if count_unchanged > 0:
                flash_message.append(
                    gettext(
                        "Screening forms unchanged (already archived): {count_unchanged}"
                    ).format(count_unchanged=count_unchanged)
                )
            if flash_message:
                flash("\n".join(flash_message))
        except Exception as ex:
            if not self.handle_view_exception(ex):
                raise

            flash(
                gettext("Failed to archive screening forms. %(error)s", error=str(ex)),
                "error",
            )


class ScreeningFormDatacareView(
    DatacareAdminAccess, ScreeningFormCoordinatorDatacareView
):
    # Add a details view link to subject
    column_formatters = dict(
        {
            "subject": lambda v, c, m, n: (
                Markup(
                    "<a href='{}'>{}</a>".format(
                        v.get_url("subject.details_view", id=m.subject.id), m.subject
                    )
                )
                if m.subject
                else None
            ),
        },
        **ScreeningFormView.column_formatters,
    )

    # Datacare users must define a device to which the Screening Form is applicable
    form_args = dict(
        devices=dict(
            query_factory=lambda: Device.query.filter(Device.clinical.is_(True)),
            validators=[DataRequired()],
        ),
        **ScreeningFormCoordinatorDatacareView.form_args,
    )

    screening_form_actions = dict(
        **ScreeningFormCoordinatorDatacareView.screening_form_actions,
        **dict(
            (action_obj.action, action_obj)
            for action_obj in [
                ScreeningFormAction(
                    new_state=ScreeningState.datacare_review,
                    submit_style="primary",
                    submit_label=gettext("Save"),
                    action="datacare_review_save",
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.approved,
                    submit_style="success",
                    submit_label=gettext("Approve"),
                    action="approved_from_datacare_review",
                    confirm_message=gettext(
                        "This will mark the form as Approved and notify the study"
                        " coordinator(s). Are you sure?"
                    ),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.coordinator_review,
                    submit_style="warning",
                    submit_label=gettext("Return to coordinator for review"),
                    action="return_to_coordinator_review",
                    notify="coordinator",
                    notify_message=gettext(
                        "An MR technologist has returned this form for coordinator review. "
                        "Use the link below to see MR technologist's comments."
                    ),
                    confirm_message=gettext(
                        "This will return the form to coordinator(s) for review. Any issues"
                        " should be identified in the MR Technologist Comments section to"
                        " help the coordinator(s) decide on the next step."
                        " Are you sure?"
                    ),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.approved,
                    submit_style="primary",
                    submit_label=gettext("Save"),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.completed,
                    submit_style="success",
                    submit_label=gettext("Confirm and Submit"),
                    confirm_message=gettext(
                        "This will mark the screening form as Completed. It should be done"
                        " just prior to the exam in case no issues are identified when"
                        " reviewing the form in person with the participant. The form will"
                        " then be archived with no further modifications possible."
                        " Are you sure?"
                    ),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.rejected,
                    submit_style="warning",
                    submit_label=gettext("Reject"),
                    confirm_message=gettext(
                        "This will mark the screening form as Rejected. "
                        "The form will then be archived with no further modifications"
                        " possible. Are you sure?"
                    ),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.approved,
                    submit_style="primary",
                    submit_label=gettext("Approve"),
                    action="approved_from_created_or_rejected",
                    confirm_message=gettext(
                        "This will mark the form as Approved by an MR technologist. Are you sure?"
                    ),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.approved,
                    submit_style="warning",
                    submit_label=gettext("Approve (MR tech)"),
                    action="approved_from_coordinator_review",
                    confirm_message=gettext(
                        "This will mark the form as Approved by an MR technologist. Are you sure?"
                    ),
                ),
                ScreeningFormAction(
                    new_state=ScreeningState.completed,
                    submit_style="success",
                    submit_label=gettext("Confirm and Submit"),
                    action="completed_from_created",
                    confirm_message=gettext(
                        "This will mark the screening form as Completed. It should be done"
                        " just prior to the exam in case no issues are identified when"
                        " reviewing the form in person with the participant. The form will"
                        " then be archived with no further modifications possible."
                        " Are you sure?"
                    ),
                ),
            ]
        ),
    )

    def _get_screening_actions(self, state):
        """
        Define submit options (state transitions) depending on the current screening form state
        See also superclass formatting documentation
        """
        if state == ScreeningState.coordinator_review:
            return self.get_screening_form_actions(
                "coordinator_review_save",
                "datacare_review_from_coordinator_review",
                "approved_from_coordinator_review",
            )
        elif state == ScreeningState.datacare_review:
            return self.get_screening_form_actions(
                "datacare_review_save",
                "approved_from_datacare_review",
                "return_to_coordinator_review",
            )
        elif state == ScreeningState.approved:
            return self.get_screening_form_actions(
                ScreeningState.approved,
                ScreeningState.completed,
                ScreeningState.rejected,
            )
        elif state == ScreeningState.created:
            return self.get_screening_form_actions(
                "approved_from_created_or_rejected", "completed_from_created"
            )
        elif state == ScreeningState.rejected:
            return self.get_screening_form_actions(
                "approved_from_created_or_rejected",
                "return_to_coordinator_review",
            )
        elif state in (ScreeningState.completed, ScreeningState.archived):
            return []
        else:
            raise RuntimeError(
                gettext("Invalid screening form state for datacare view")
            )

    def validate_form(self, form):
        """
        Return False if form is transitioning to `completed` state and both devices are being selected
        """
        if not super(ScreeningFormDatacareView, self).validate_form(form):
            return False

        if (
            type(form) not in (self._action_form_class, self._delete_form_class)
            and form.devices.data
            and len(form.devices.data) > 1
            and any(
                action_ in request.form
                for action_ in ("ScreeningState.completed", "completed_from_created")
            )
        ):
            err = gettext(
                "You must choose only one device when marking form as Completed."
            )
            form.devices.errors += (err,)
            flash(err, "error")
            return False

        return True


class ScreeningFormParticipantView(ScreeningFormView):
    """Public screening form view"""

    # do not include project fields
    form_extra_fields = ScreeningFormView._subject_fields

    # Participant sees no navigation links or coordinator/mr tech fields
    screening_hide_navigation = True
    show_fields = "participant"

    # In Participant view, the participant name (signature/initials) field is mandatory
    form_widget_args = {
        "q_participant_name": dict(required=""),
        "subject_birthdate": {
            "data-role": "datepicker2",
        },
    }

    # disable operations

    # can only edit via the token endpoint
    can_edit = False
    can_create = False
    can_view_details = False
    can_delete = False

    @expose("/")
    def index_view(self):
        """
        do not allow index (list) view
        """
        return redirect(url_for("admin.index", next=request.url))

    screening_form_actions = dict(
        (action_obj.action, action_obj)
        for action_obj in [
            ScreeningFormAction(
                new_state=ScreeningState.coordinator_review,
                submit_style="success",
                submit_label=gettext("Submit form"),
                notify=["coordinator", "datacare"],
                notify_message=gettext(
                    "Screening form has been filled out by the participant and submitted for"
                    " coordinator review."
                ),
                confirm_message=gettext(
                    "This will send your completed form to the research study coordinator."
                    " Are you sure?"
                ),
            )
        ]
    )

    def _get_screening_actions(self, state):
        return [self.screening_form_actions[k] for k in self.screening_form_actions]

    @expose("/<token>", methods=("GET", "POST"))
    def token(self, token):
        """Token-based form editing"""

        # verify the token
        # noinspection PyBroadException
        try:
            data = ScreeningForm.verify_token(token)
        except (InvalidToken, MalformedToken):
            flash(gettext("Invalid screening form token provided"), "error")
            return redirect(url_for("admin.index"))

        model = data["screening_form"]

        if model.state != ScreeningState.created:
            # Participant can only edit the form while it's in the created state
            flash(
                gettext(
                    "This form has been submitted to the research study coordinator and"
                    " can no longer be edited by the participant."
                ),
                "warning",
            )
            return redirect(url_for("admin.index"))

        # stripped-down version of self.edit_view

        form = self.edit_form(obj=model)

        # remove form fields that are not modifiable by the participant
        for field in "project", "entry":
            delattr(form, field)

        if self.validate_form(form):
            if self.update_model(form, model):
                flash(gettext("Your form has been successfully saved."), "success")
                form_success = True
            else:
                flash(gettext("Could not update form"), "error")
                form_success = False

            return self.render(
                "screening/participant_landing.html",
                form_success=form_success,
                contact=Mail(model.last_coordinator),
                contact_url=current_app.config.get("CONTACT_URL", None),
                link=next(iter(current_app.config.get("LINK_LIST", [])), None),
            )

        form_opts = FormOpts(
            widget_args=self.form_widget_args, form_rules=self._form_edit_rules
        )

        return self.render(
            self.edit_template,
            model=model,
            form=form,
            form_opts=form_opts,
            return_url=get_redirect_target() or self.get_url(".index_view"),
        )

    # noinspection PyMethodMayBeStatic
    def is_accessible(self):
        # World accessible
        return True

    def is_visible(self):
        return False

    def get_subject_fields(self, form):
        # Don't display subject ID field
        return [
            f
            for f in super(ScreeningFormParticipantView, self).get_subject_fields(form)
            if f.name != "subjectid"
        ]

    def edit_form(self, obj=None):
        form = super(ScreeningFormParticipantView, self).edit_form(obj)

        # remove InputRequired validator from project field
        # this is added automatically by Flask-Admin but interferes with validaton of the form on participant edit
        for i in range(len(form.project.validators)):
            if type(form.project.validators[i]) is InputRequired:
                del form.project.validators[i]
        return form

    def render(self, template, **kwargs):
        """Don't display access control menu"""
        kwargs["show_access_control"] = False
        return super().render(template, **kwargs)


class ScreeningFormCoordinatorView(
    CoordinatorAccess, ScreeningFormCoordinatorDatacareView
):
    # show all fields, only coordinator fields editable
    show_fields = "coordinator"

    # noinspection PyMethodMayBeStatic
    def get_query(self):
        """
        Restrict to :class:`ScreeningForm` belonging to coordinator :class:`User`
        """
        return ScreeningForm.query.filter(
            ScreeningForm.project_id.in_(
                Project.accessible_to(
                    current_user, level=MembershipType.coordinator
                ).with_entities(Project.id)
            )
        )

    def get_count_query(self):
        """
        Count the items in the query
        """
        return self.get_query().with_entities(db.func.count(ScreeningForm.id))

    def _get_screening_actions(self, state):
        """
        Define submit options (state transitions) depending on the current screening form state
        See also superclass formatting documentation
        """
        if (
            state == ScreeningState.coordinator_review
            or state == ScreeningState.created
        ):
            return self.get_screening_form_actions(
                "coordinator_review_save", "datacare_review_from_coordinator_review"
            )
        elif state in [
            ScreeningState.datacare_review,
            ScreeningState.approved,
            ScreeningState.completed,
            ScreeningState.rejected,
            ScreeningState.archived,
        ]:
            return []
        else:
            raise RuntimeError(
                gettext("Invalid screening form state for coordinator view")
            )
