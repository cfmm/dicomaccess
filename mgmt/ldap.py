"""
Created on Jul 28, 2015

Copyright 2015, The University of Western Ontario

.. module:: ldapquery
    :platform: Unix, Mac, Windows
    :synopsis: Handles LDAP queries

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

import re
import ssl
import typing

from flask import flash, has_request_context, current_app
from ldap3 import (
    Server,
    Connection,
    Tls,
    SAFE_RESTARTABLE,
    BASE,
    SUBTREE,
    MODIFY_REPLACE,
    MODIFY_ADD,
    MODIFY_DELETE,
    RANDOM,
    AUTO_BIND_NONE,
    ServerPool,
)
from ldap3.core.exceptions import (
    LDAPException,
    LDAPEntryAlreadyExistsResult,
    LDAPNoSuchObjectResult,
    LDAPUnwillingToPerformResult,
    LDAPMaximumRetriesError,
    LDAPInvalidDnError,
    LDAPInvalidValueError,
)

if has_request_context():
    from flask_babel import gettext
else:

    def gettext(x):
        return x


from .models.credentials import User, Group
from .exceptions import LdapDnError, EmailExists, UsernameExists, LdapNoMatchingEntry


class LDAP:
    """
    LDAP object to handle queries and authentication
    """

    conn = None

    def __init__(self, app=None, config=None):
        """
        Handle initialization with flask app

        :param app: Flask application for initialization
        :type app: Flask application
        :returns: None
        :raises: None
        """
        self.logger = None
        self.config = config or dict()
        # noinspection PyTypeChecker
        self.pool = ServerPool(None, pool_strategy=RANDOM, active=True, exhaust=10)

        if app is not None:
            self.init_app(app)

    def init_app(self, app, config=None):
        """
        Set default configuration values

        :param app: Flask application
        :param config: Flask application configuration
        :type config: Dictionary
        :returns: None
        :raises: None
        """
        self.config = config or app.config["LDAPQUERY"]

        self.config.setdefault(
            "servers",
            [
                "ldap://localhost:389",
            ],
        )
        assert "base" in self.config, "base required in LDAPQUERY"
        assert "user_base" in self.config, "user_base required in LDAPQUERY"
        assert "local_base" in self.config, "local_base required in LDAPQUERY"
        assert "group_base" in self.config, "group_base required in LDAPQUERY"
        assert "bind_dn" in self.config, "bind_dn required in LDAPQUERY"
        assert "bind_auth" in self.config, "bind_auth required in LDAPQUERY"

        self.config.setdefault("timeout", 0)
        self.config.setdefault("start_tls", False)
        self.config.setdefault("read_only", False)
        self.config.setdefault("group_id", "gidNumber")
        self.config.setdefault("user_attribute", "cn")
        self.config.setdefault("group_attribute", "sAMAccountName")
        self.config.setdefault("identifier", "sAMAccountName")
        self.config.setdefault("group_object_class", "group")
        # Active Directory requires special recursive queries
        # "(member:1.2.840.113556.1.4.1941:={0}))".format(dn)),
        # OpenLDAP does not support recursive groups and therefore does not work correctly
        self.config.setdefault("group_search_attr", "member:1.2.840.113556.1.4.1941:")
        self.config.setdefault("user_object_class", "user")
        self.config.setdefault("size_limit", 100)
        self.config.setdefault("time_limit", 2)

        tls = Tls(
            validate=(
                ssl.CERT_REQUIRED
                if self.config.get("validate", True)
                else ssl.CERT_NONE
            )
        )

        if type(self.servers) is str:
            self.config["servers"] = [
                self.servers,
            ]
        self.config["user_base"] = self.user_base.lower()
        self.config["user_bases"] = [self.user_base]
        if self.local_base:
            self.config["local_base"] = self.local_base.lower()
            self.config["user_bases"] += [
                self.local_base,
            ]

        # noinspection PyTypeChecker
        self.pool = ServerPool(None, pool_strategy=RANDOM, active=True, exhaust=10)
        for host in self.servers:
            self.pool.add(Server(host=host, tls=tls))

        # noinspection PyTypeChecker
        self.conn = Connection(
            self.pool,
            auto_referrals=False,
            client_strategy=SAFE_RESTARTABLE,
            user=self.bind_dn,
            password=self.bind_auth,
            auto_bind=AUTO_BIND_NONE,
            # Required for LDAP library to raise LDAPEntryAlreadyExistsResult
            raise_exceptions=True,
            read_only=self.config["read_only"],
        )

        self.logger = app.logger

    def __getattr__(self, item):
        try:
            return self.config[item]
        except KeyError:
            pass

        raise AttributeError

    @staticmethod
    def extract_cn(dn):
        result = None
        if dn:
            # Convert a byte array to string
            try:
                dn = str(dn, "utf-8")
            except TypeError:
                pass
            try:
                reg = re.search(".+?=(.+?),", dn)
                if reg:
                    result = reg.group(1)
            except TypeError:
                pass
        return result

    def is_group(self, dn):
        return (
            len(self.search(dn, BASE, f"(objectClass={self.group_object_class})")) > 0
        )

    def is_user(self, dn):
        return len(self.search(dn, BASE, f"(objectClass={self.user_object_class})")) > 0

    def connect(self):
        """
        Establish an LDAP connection with server

        Connection does not handle referrals and uses start_tls.

        :returns: LDAP connection
        :raises: None
        """
        if self.start_tls and not self.conn.tls_started:
            self.conn.start_tls()

        if not self.conn.bound:
            try:
                self.conn.bind()
            except LDAPMaximumRetriesError as err:
                if self.logger:
                    self.logger.error(str(err))
                raise LDAPException(gettext("Unable to contact authentication server"))
            except (LDAPException, Exception) as err:
                if self.logger:
                    self.logger.error(str(err))
                raise

    def _add_ou(self, ou):
        ou = str(ou)
        result = ou.split(",", 1)
        name = result[0].split("=", 1)[1]

        attr = {
            "ou": [name.encode("utf-8")],
        }

        self.connect()

        while True:
            try:
                self.conn.add(
                    ou, object_class=["organizationalUnit", "top"], attributes=attr
                )
            except LDAPEntryAlreadyExistsResult:
                pass
            except LDAPNoSuchObjectResult:
                self._add_ou(result[1])
            break

    def group_membership(self, user: User):
        """
        Returns all groups of which a user is a member.

        :param user: User for which to find membership
        :type user: User
        :returns: list
        :raises: None
        """
        return self.search(
            self.group_base,
            SUBTREE,
            f"(&(objectClass={self.group_object_class})({self.group_search_attr}={user.dn}))",
            ["gidNumber"],
        )

    def get_user(self, user: User, fields: typing.Iterable[str] = None):
        """
        Get users LDAP information

        :param user: User to lookup
        :type user: string
        :param fields: Fields to lookup
        :type fields: list
        :returns: Dictionary of LDAP properties
        :raises: None
        """
        for user_base in self.user_bases:
            results = self.search(
                user_base,
                SUBTREE,
                f"(&(objectClass={self.user_object_class})({self.user_attribute}={user.username}))",
                fields or [],
            )
            if results:
                return results[0]

    def display_entry(self, result_entry, users=True, groups=True):
        object_classes = [
            oc.lower() for oc in result_entry["attributes"].get("objectClass", list())
        ]

        if users and groups:
            group_prefix = gettext("Group: ")
            user_prefix = gettext("User: ")
        else:
            user_prefix = ""
            group_prefix = ""

        display_name = result_entry["attributes"].get("displayName", None)
        if self.group_object_class.lower() in object_classes:
            common_name = result_entry["attributes"][self.group_attribute]
            if isinstance(common_name, list):
                common_name = common_name[0]
            display = (
                f"{group_prefix}{display_name or common_name} ({result_entry['dn']})"
            )
        elif self.user_object_class.lower() in object_classes:
            common_name = result_entry["attributes"][self.user_attribute]
            if isinstance(common_name, list):
                common_name = common_name[0]
            display = f"{user_prefix}{common_name} ({display_name or result_entry['attributes']['sAMAccountName']})"
        else:
            display = f"Entry: {result_entry['dn']}"

        return display

    def make_group(self, group):
        """
        Create group if it does not exist
        """
        if not group:
            raise ValueError(f"Invalid group: {group}")

        attr = {
            "groupType": [b"-2147483644"],
            self.group_attribute: [str(group.identifier).encode("utf-8")],
            "gidNumber": [str(group.id).encode("utf-8")],
            "displayName": [str(group.name).encode("utf-8")],
            "cn": [str(group.name).encode("utf-8")],
        }

        if group.description:
            attr["description"] = [str(group.description).encode("utf-8")]

        dn = f"{group.path},{self.group_base}"

        update = False
        while True:
            try:
                result = self.conn.add(
                    dn, object_class=[self.group_object_class, "top"], attributes=attr
                )
                if not result[0]:
                    if result[1]["result"] == 32:
                        raise LDAPNoSuchObjectResult

                update = not result[0]
            except LDAPNoSuchObjectResult:
                self._add_ou(dn.split(",", 1)[1])
                continue
            except LDAPEntryAlreadyExistsResult:
                update = True
            break

        if update:
            # Remove attributes that cannot be changed
            attr.pop("groupType", None)
            keys = [x for x in attr]

            try:
                entry = self.get_group_entry(group, keys)
            except LdapNoMatchingEntry:
                raise ValueError(gettext("Invalid group: {dn}").format(dn=dn))

            self._update_dn(entry, attr, dn)

    def _update_dn(self, entry, attr, new_dn=None):
        # Get the DN based on the existing entry
        dn = entry["dn"]

        changes = self._changes(attr, entry["raw_attributes"])

        if changes:
            current_app.logger.debug(f"Applying changes to {dn}: {changes} ")
            self.connect()

            # Changes to CN cannot be done with modify
            data = changes.pop("cn", None)
            if changes:
                self.conn.modify(dn, changes=changes)

            # Use modify_dn to change the CN
            if data is not None:
                try:
                    # cn should be a list of bytearrays, of which only the first is used
                    self.conn.modify_dn(dn, f"cn={data[1][0].decode('utf-8')}")
                except LDAPEntryAlreadyExistsResult:
                    if new_dn:
                        self.conn.delete(new_dn)
                        self.conn.modify_dn(dn, f"cn={data[1][0].decode('utf-8')}")
                    else:
                        raise

    def get_dn(self, item: typing.Union[str, Group, User]):
        if isinstance(item, Group):
            return self.get_group_entry(item)["dn"]
        elif isinstance(item, User):
            return self.get_user_entry(item)["dn"]
        else:
            return item

    def writeable(self, dn):
        return any(
            dn.lower().endswith(x)
            for x in (self.local_base.lower(), self.group_base.lower())
        )

    def remove(self, item: typing.Union[str, Group, User]):
        """
        Remove a DN
        """
        try:
            dn = self.get_dn(item)
        except LdapNoMatchingEntry:
            return

        if dn and self.writeable(dn):
            self.connect()
            try:
                self.conn.delete(dn)
            except LDAPNoSuchObjectResult:
                # Deleting a group that does not exist is not an error
                if self.logger:
                    self.logger.error(
                        gettext("LDAP failed to delete {dn}").format(dn=dn)
                    )
            except Exception as err:
                current_app.logger.error(
                    gettext("Failed to delete LDAP {dn}: {error}").format(
                        error=str(err), dn=dn
                    )
                )
                raise

    @property
    def find_attributes(self):
        return list(
            {
                "displayName",
                "objectClass",
                "cn",
                "sAMAccountName",
                self.user_attribute,
                self.group_attribute,
            }
        )

    def find_member(
        self,
        criteria: str,
        not_memberof: str = None,
        pattern: str = None,
        restricted_users: set = None,
        offset: int = 0,
        limit: int = None,
        users: bool = True,
        groups: bool = True,
    ):
        dn_criteria = ""
        group_limits = list()
        if pattern:
            group_limits.append(f"({self.group_attribute}={pattern})")
        if not_memberof:
            result = self.search(
                self.base,
                SUBTREE,
                f"(&(objectClass={self.group_object_class})({self.group_attribute}={not_memberof}))",
            )
            if result:
                dn_criteria = f"(!(memberOf={result[0]['dn']}))"
                group_limits.append(f"({self.group_attribute}={not_memberof})")

        if len(group_limits) > 1:
            group_limit = f"(!(|{''.join(group_limits)}))"
        elif len(group_limits) > 0:
            group_limit = f"(!{''.join(group_limits)})"
        else:
            group_limit = ""

        if restricted_users:
            user_limit = (
                "(!(|"
                + "".join(f"({self.user_attribute}={r})" for r in restricted_users)
                + "))"
            )
        else:
            user_limit = ""

        term = f"{criteria.strip('*')}*"

        # Search for users that match the criteria
        match = f"(|({self.user_attribute}={term})(sn={term})(givenName={term})(sAMAccountName={term}))"
        user_query = (
            f"(&(objectClass={self.user_object_class})"
            f"(!(objectClass=computer)){match}{dn_criteria}{user_limit})"
        )

        # Query for groups that match the criteria
        match = f"(|({self.group_attribute}=*{term})(displayName=*{term})(sAMAccountName=*{term}))"
        group_query = f"(&(objectClass={self.group_object_class}){match}{dn_criteria}{group_limit})"

        if users and groups:
            query = f"(|{group_query}{user_query})"
        elif users:
            query = user_query
        elif groups:
            query = group_query
        else:
            raise ValueError(gettext("Must select at least one of users and groups"))

        self.connect()
        entry_generator = self.conn.extend.standard.paged_search(
            search_base=self.base,
            search_filter=query,
            attributes=self.find_attributes,
            search_scope=SUBTREE,
            paged_size=limit,
            time_limit=self.time_limit,
            generator=True,
        )

        limit = limit or self.size_limit
        count = 0

        for entry in entry_generator:
            if entry["type"] == "searchResEntry":
                count += 1
                # Skip the first offset results
                if count > offset:
                    yield entry["dn"], self.display_entry(entry, users, groups)
                    # Stop once the required limit has been found
                    if count >= offset + limit:
                        break

    def groups(self, name, attributes=None):
        """
        Get a list of all group attributes
        """
        match = f"({self.group_attribute}={name})"

        results = self.search(
            self.group_base,
            SUBTREE,
            f"(&(objectClass={self.group_object_class}){match})",
            attributes or [self.group_attribute],
        )
        if not results:
            return []

        try:
            return [x["attributes"] for x in results]
        except TypeError as exc:
            if self.logger:
                self.logger.error("Groups failed with {0}".format(results))
            raise exc

    def find_groups(self, name):
        """
        Search for groups
        """
        if not name:
            raise ValueError("Group name must be set.")

        results = self.search(
            self.base,
            SUBTREE,
            (
                "(&(objectClass={2})({0}=*{1}*))".format(
                    self.group_attribute, name.rstrip("*"), self.group_object_class
                )
            ),
            [self.group_attribute],
        )

        return [
            (x["dn"], x["attributes"][self.group_attribute][0])
            for x in results
            if (x and x["attributes"][self.group_attribute])
        ]

    def members(self, group, base=None):
        """
        Get the member fields in a group
        """
        if not group:
            return []

        results = self.search(
            base or self.group_base,
            SUBTREE,
            "(&(objectClass={2})({0}={1}))".format(
                self.group_attribute, group.identifier, self.group_object_class
            ),
            ["member"],
        )

        if results:
            try:
                return results[0]["attributes"]["member"]
            except KeyError:
                return []
        return []

    def lookup_by_dn(self, base, attributes=None, object_class=None):
        return self.search(
            base,
            BASE,
            "(&(objectClass={0}))".format(object_class or self.user_object_class),
            attributes or [self.user_attribute],
        )

    def user_exists(self, attributes: typing.Union[str, dict]):
        # String is username
        if isinstance(attributes, str):
            attributes = {self.user_attribute: attributes}

        data = [f"{key}={value}" for key, value in attributes.items()]
        query = f"(&(objectClass={self.user_object_class})({')('.join(data)}))"

        for base in self.user_bases:
            result = self.search(base, SUBTREE, query, [self.user_attribute])
            if result:
                return True
        return False

    def _user_attributes(self, user: User):
        dn = f"{self.user_attribute}={user.username},{self.local_base}"

        if user.first_name is not None:
            display_name = f"{user.first_name} {user.last_name}"
        else:
            display_name = user.last_name or user.username

        identifier = user.identifier
        display_name = display_name.encode("utf-8")
        attr = {
            self.user_attribute: [user.username.encode("utf-8")],
            self.identifier: [identifier.encode("utf-8")],
            "displayName": [display_name],
            "sn": [display_name],
        }
        if user.email:
            attr["mail"] = [user.email.encode("utf-8")]
        if user.last_name:
            attr["sn"] = [user.last_name.encode("utf-8")]
        if user.first_name:
            attr["givenName"] = [user.first_name.encode("utf-8")]

        return dn, attr

    @staticmethod
    def _changes(attributes, current):
        def compare(x, y):
            # Match all items in list
            if isinstance(x, list) and isinstance(y, list):
                return all(compare(i, j) for i, j in zip(x, y))
            # Decode byte strings if necessary
            return (x.decode() if type(x) is bytes else x) != (
                y.decode() if type(y) is bytes else y
            )

        # Create list of required changes
        changes = dict()
        for key, value in attributes.items():
            if key in current:
                if compare(value, current[key]):
                    changes[key] = (MODIFY_REPLACE, value)
            else:
                changes[key] = (MODIFY_ADD, value)

        return changes

    def get_user_entry(self, user: User, attributes=None):
        results = list()

        # Initial search by identifier in local_base
        results += self.search(
            self.local_base,
            SUBTREE,
            f"(&(objectClass={self.user_object_class})({self.identifier}={user.identifier}))",
            attributes,
        )

        # Do not stop at the first found, because a check is needed to identify potential duplicates

        # Search by user_attribute
        query = f"(&(objectClass={self.user_object_class})({self.user_attribute}={user.username}))"
        for base in self.user_bases:
            results += self.search(base, SUBTREE, query, attributes)

        if not results:
            raise LdapNoMatchingEntry(gettext("User does not exist in LDAP"))

        result = results[0]
        if len(results) > 1:
            dn = {x["dn"].lower() for x in results}
            if len(dn) > 1:
                current_app.logger.critical(
                    gettext("Overlapping users:\n{data}").format(data="\n".join(dn))
                )

                # Prefer the local user entry
                for x in results:
                    if x["dn"].lower().endswith(self.local_base):
                        result = x
                        result["local"] = True
                        return result

        result["local"] = result["dn"].lower().endswith(self.local_base)
        return result

    def get_group_attributes(self, dn: str, attr: list):
        result = self.search(
            dn,
            BASE,
            f"(objectClass={self.group_object_class})",
            attr,
        )

        if not result:
            return dict()

        return result[0]["attributes"]

    def get_group_entry(self, group: Group, attr=None):
        result = self.search(
            self.group_base,
            SUBTREE,
            f"(&(objectClass={self.group_object_class})({self.identifier}={group.identifier}))",
            attr,
        )

        # Fallback to lookup via DN
        if not result:
            result = self.search(
                f"{group.path},{self.group_base}",
                BASE,
                f"(objectClass={self.group_object_class})",
                attr,
            )

        if not result:
            raise LdapNoMatchingEntry(
                gettext("Group {name} does not exist in LDAP").format(
                    name=f"{self.identifier}={group.identifier}"
                )
            )

        return result[0]

    def make_user(self, user: User):
        dn, attr = self._user_attributes(user)

        # validate the email and username are not already being used
        query = f"({self.user_attribute}={user.username})"
        if user.email:
            query = f"(|{query}(mail={user.email}))"

        query = f"(&(objectClass={self.user_object_class}){query})"
        query_attributes = list({self.user_attribute, "mail"})
        for user_base in self.user_bases:
            for result in self.search(user_base, SUBTREE, query, query_attributes):
                # If there is an entry in the search result whose dn does not match the user, this is an error
                if result["dn"].lower() != dn.lower():
                    username_result = result["attributes"][self.user_attribute]
                    # Determine the username of found entry. In some schema (e.g. AD server), the attribute is
                    # single-valued. In others, it's multi-valued (see https://ldapwiki.com/wiki/Cn)
                    if type(username_result) is list:
                        if len(username_result) != 1:
                            # The user_attribute should never return multiple values
                            raise LDAPInvalidValueError(
                                gettext(
                                    "Need exactly 1 value for attribute {attr} when querying user {username}."
                                ).format(
                                    attr=self.user_attribute, username=user.username
                                )
                            )
                        username_result = username_result[0]
                    if user.username.lower() == username_result.lower():
                        raise UsernameExists
                    else:
                        raise EmailExists

        object_classes = [self.user_object_class]

        self.connect()
        update = False
        try:
            add_result = self.conn.add(dn, object_class=object_classes, attributes=attr)
            if not add_result[0]:
                raise LDAPException(
                    f"Making user {dn} failed due to {add_result[1]['description']}: {add_result[1]['message']}"
                )
        except LDAPEntryAlreadyExistsResult:
            update = True
        except Exception as err:
            current_app.logger.error(
                gettext("Failed to create LDAP user {username}: {error}").format(
                    error=str(err), username=user.username
                )
            )
            raise

        if update:
            keys = [x for x in attr]
            try:
                entry = self.get_user_entry(user, keys)
            except LdapNoMatchingEntry:
                return
            self._update_dn(entry, attr)

    def set_members(
        self,
        group: Group,
        members: typing.Iterable,
    ):
        """
        Assign group members

        :param group: Group or distinguished name of group
        :param members: set of member distinguished names
        :return:
        """
        if not group:
            return

        result = self.search(
            self.group_base,
            SUBTREE,
            f"(&(objectClass={self.group_object_class})({self.group_attribute}={group.identifier}))",
            ["member"],
        )

        if not result:
            return

        dn = result[0]["dn"].lower()

        new_members = set()
        for member in members:
            if member is None:
                continue
            if isinstance(member, User):
                value = self.get_user_entry(member)["dn"]
            elif isinstance(member, Group):
                value = self.get_group_entry(member)["dn"]
            else:
                value = member

            # Only add members not equal to the group DN
            if value.lower() != dn:
                new_members.add(value)

        existing_members = result[0]["attributes"]["member"]

        changes = None
        if new_members:
            if existing_members:
                if set(existing_members).symmetric_difference(new_members):
                    changes = {"member": (MODIFY_REPLACE, new_members)}
            else:
                changes = {"member": (MODIFY_ADD, new_members)}
        elif existing_members:
            changes = {"member": (MODIFY_DELETE, existing_members)}

        if changes:
            try:
                current_app.logger.debug(f"Applying changes to {dn}: {changes}")
                self.conn.modify(dn, changes)
            except (LDAPEntryAlreadyExistsResult,):
                pass
            except LDAPNoSuchObjectResult as err:
                # member or group is not a valid object
                # noinspection PyTypeChecker
                if err.args[0].get("info", "").startswith("0000208D"):
                    raise LdapDnError(gettext("Invalid group: {dn}").format(dn=dn))
                elif err.args[0].get("info", "").startswith("00000525"):
                    raise LdapDnError(
                        gettext("Invalid members: {members}").format(members=members)
                    )

                raise ValueError(gettext("Invalid object"))

    def remove_members(self, dn: str, members: set):
        """
        Remove a member from a group
        """
        if not members:
            return None

        changes = {"member": (MODIFY_DELETE, [str(item) for item in members])}

        self.connect()

        try:
            self.conn.modify(dn, changes)
        except LDAPUnwillingToPerformResult:
            # member does not belong to group
            pass
        except LDAPNoSuchObjectResult:
            # Group does not exist
            raise ValueError(gettext("Invalid group: {dn}").format(dn=dn))

    def search(self, base, scope, query, attr=None, **kwargs):
        """
        Perform an LDAP query

        :param base: Base form which to perform query
        :type base: string
        :param scope: Scope of query
        :type scope: Literal
        :param query: Query string
        :type query: string
        :param attr: List of attributes to return
        :type attr: list
        :returns: list
        :raises: None
        """
        self.connect()
        # LDAP3 library does not accept 'dn' as a valid search attribute. 'dn' always returned in search results.
        if attr:
            attr = [a for a in attr if a != "dn"]

        # Set a default time limit of 10 seconds on the search
        kwargs.setdefault("time_limit", 10)

        try:
            result = self.conn.search(
                base, query, search_scope=scope, attributes=attr, **kwargs
            )
            if result[0]:
                return result[2] if result[2] else list()
        except (LDAPInvalidDnError, LDAPNoSuchObjectResult):
            if self.logger:
                self.logger.debug(
                    gettext(
                        'LDAP search failed for {query} in {base} using "{servers}"'
                    ).format(query=query, base=base, servers=self.servers)
                )
        except LDAPException as err:
            msg = gettext(
                'LDAP search failed for {query} in {base} using "{servers}: {error}"'
            ).format(query=query, base=base, servers=self.servers, error=str(err))
            if self.logger:
                self.logger.error(msg)
            self.flash_error(msg)
        except Exception as err:
            if self.logger:
                self.logger.error(str(err))
            self.flash_error(str(err))

        return list()

    @staticmethod
    def flash_error(msg):
        """
        Display general errors

        :param msg: Error message
        :raises: None
        """
        if has_request_context():
            flash(message=msg, category="error")
