"""
Created on Apr 26, 2016

Copyright 2016, The University of Western Ontario

.. codeauthor:: mklassen
"""

from flask import render_template, current_app
from flask_login import current_user
from sqlalchemy.orm import aliased
from .models import Entry
from ..models.dicom import Project, Principal
import datetime
import pytz
import tzlocal


@current_app.template_filter("epoch_format")
def epoch_format(value, fmt="%Y/%m/%d %H:%M%z", tz=pytz.utc):
    return datetime.datetime.fromtimestamp(value, tz=tz).strftime(fmt)


@current_app.template_filter("epoch_format_local")
def epoch_format_local(
    value, fmt="%Y-%m-%dT%H:%M:00.000", tz=pytz.utc, timezone=tzlocal.get_localzone()
):
    return (
        datetime.datetime.fromtimestamp(value, tz=tz).astimezone(timezone).strftime(fmt)
    )


class Report:
    @staticmethod
    def generate_query(
        principals=None,
        projects=None,
        rooms=None,
        rates=None,
        start=None,
        end=None,
        description=None,
        user=None,
    ):
        user = user or current_user
        query = Entry.query
        if not user.is_authenticated:
            return query.limit(0)

        # Get subquery on only accessible projects
        project_sq = aliased(
            Project,
            Project.accessible_to(user, active=False).subquery(),
        )

        # Inner left join on accessible projects only
        query = query.join(Entry.project.of_type(project_sq)).join(Project.principal)
        if projects:
            query = query.filter(
                Entry.project_id.in_(project.id for project in projects)
            )
        elif principals:
            query = query.filter(
                Principal.id.in_(principal.id for principal in principals)
            )

        if rooms:
            query = query.filter(Entry.room_id.in_(room.id for room in rooms))

        if rates:
            query = query.filter(Entry.rate_id.in_(rate.id for rate in rates))

        if start:
            query = query.filter(Entry.end_time >= start)
        if end:
            query = query.filter(Entry.start_time <= end)

        if description:
            query = query.filter(Entry.description.contains(description))

        query = query.order_by(Principal.name, project_sq.name, Entry.start_time)
        return query

    """
    Generate reports for MRBS booked studies
    """

    @staticmethod
    def principal(
        principals=None,
        projects=None,
        rooms=None,
        rates=None,
        start=None,
        end=None,
        description=None,
        output_type="html",
        interval=None,
        offset=0,
        timezone=None,
    ):
        query = Report.generate_query(
            principals=principals,
            projects=projects,
            rooms=rooms,
            rates=rates,
            start=start,
            end=end,
            description=description,
        )

        entries = query.all()
        body = ""

        summary = list()

        # Entries are sorted by principal name first
        previous_principal = None
        for principal in [entry.project.principal for entry in entries]:
            if principal == previous_principal:
                continue
            previous_principal = principal

            # Get entries that belong to principal
            my_entries = [
                entry for entry in entries if entry.project.principal == principal
            ]

            total = 0
            total_duration = 0
            project_body = ""
            projects = 0
            rows = 1

            # Entries are sorted by project name second
            previous_project = None
            for project in [entry.project for entry in my_entries]:
                if project == previous_project:
                    continue
                previous_project = project

                subtotal = 0
                subtotal_duration = 0
                count = 0
                entry_body = ""

                # Entries are sorted by start time third
                for entry in [x for x in my_entries if x.project == project]:
                    entry_body += render_template(
                        "mrbs/entry." + output_type,
                        description=entry.description,
                        operator=entry.operator.code,
                        start_time=entry.start_time,
                        duration=entry.duration,
                        hourly=entry.rate.hourly,
                        fixed=entry.rate.fixed,
                        cost=entry.cost,
                        rate_name=entry.rate.name,
                        project=project.name,
                        offset=60 * offset,
                        timezone=timezone,
                    )
                    subtotal += entry.cost
                    subtotal_duration += entry.duration
                    count += 1

                project_body += render_template(
                    "mrbs/project." + output_type,
                    name=project.name,
                    total=subtotal,
                    total_duration=subtotal_duration,
                    speedcode=project.speedcode or "N/A",
                    entries=entry_body,
                    number=count,
                )
                rows += count + 1
                projects += 1
                total += subtotal
                total_duration += subtotal_duration

            summary.append(
                {
                    "name": principal.name,
                    "duration": total_duration,
                    "cost": total,
                    "projects": projects,
                }
            )

            body += render_template(
                "mrbs/principal." + output_type,
                name=principal.name,
                total=total,
                total_duration=total_duration,
                projects=project_body,
                rows=rows,
                interval=interval,
            )

        body += render_template("mrbs/summary." + output_type, summary=summary)

        return render_template("mrbs/report." + output_type, body=body)
