"""
Created on Apr 26, 2016

Copyright 2016, The University of Western Ontario

.. codeauthor:: mklassen
"""

from datetime import datetime

import pytz
from flask import current_app
from markupsafe import Markup
from sqlalchemy.ext.hybrid import hybrid_property
from flask_cfmm.models import ModifiedDefault
from sqlalchemy.dialects import mysql

from ..models import db

_varchar_params = dict(charset="utf8mb4", collation="utf8mb4_unicode_ci")


class Area(db.Model):
    __tablename__ = "mrbs_area"
    id = db.Column(
        db.Integer,
        primary_key=True,
        nullable=False,
        autoincrement=True,
    )
    disabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    area_name = db.Column(
        db.VARCHAR(30).with_variant(
            mysql.VARCHAR(length=30, **_varchar_params), "mysql"
        ),
        unique=True,
    )
    sort_key = db.Column(
        db.VARCHAR(30).with_variant(
            mysql.VARCHAR(length=30, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    timezone = db.Column(
        db.VARCHAR(50).with_variant(
            mysql.VARCHAR(length=50, **_varchar_params), "mysql"
        )
    )
    area_admin_email = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    resolution = db.Column(db.Integer)
    default_duration = db.Column(db.Integer)
    default_duration_all_day = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    morningstarts = db.Column(db.Integer)
    morningstarts_minutes = db.Column(db.Integer)
    eveningends = db.Column(db.Integer)
    eveningends_minutes = db.Column(db.Integer)
    private_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    private_default = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    private_mandatory = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    private_override = db.Column(
        db.VARCHAR(32).with_variant(
            mysql.VARCHAR(length=32, **_varchar_params), "mysql"
        )
    )
    min_create_ahead_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    min_create_ahead_secs = db.Column(db.Integer)
    max_create_ahead_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    max_create_ahead_secs = db.Column(db.Integer)
    min_delete_ahead_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    min_delete_ahead_secs = db.Column(db.Integer)
    max_delete_ahead_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    max_delete_ahead_secs = db.Column(db.Integer)
    max_per_day_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_per_day = db.Column(db.Integer, server_default=db.text("0"), nullable=False)
    max_per_week_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_per_week = db.Column(db.Integer, server_default=db.text("0"), nullable=False)
    max_per_month_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_per_month = db.Column(db.Integer, server_default=db.text("0"), nullable=False)
    max_per_year_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_per_year = db.Column(db.Integer, server_default=db.text("0"), nullable=False)
    max_per_future_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_per_future = db.Column(db.Integer, server_default=db.text("0"), nullable=False)
    max_secs_per_day_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_secs_per_day = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    max_secs_per_week_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_secs_per_week = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    max_secs_per_month_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_secs_per_month = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    max_secs_per_year_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_secs_per_year = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    max_secs_per_future_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_secs_per_future = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    max_duration_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    max_duration_secs = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    max_duration_periods = db.Column(
        db.Integer, server_default=db.text("0"), nullable=False
    )
    custom_html = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    approval_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    reminders_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    enable_periods = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    periods = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql"),
        server_default=None,
    )
    confirmation_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    confirmed_default = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql")
    )
    times_along_top = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        nullable=False,
        server_default=db.text("0"),
    )
    default_type = db.Column(db.CHAR, server_default="E", nullable=False)

    rooms = db.relationship("Room", back_populates="area")

    __table_args__ = (
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Room(db.Model):
    __tablename__ = "mrbs_room"
    id = db.Column(
        db.Integer,
        nullable=False,
        autoincrement=True,
        primary_key=True,
    )
    disabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    area_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "mrbs_area.id",
            onupdate="CASCADE",
            ondelete="RESTRICT",
        ),
        server_default=db.text("0"),
        nullable=False,
        index=True,
    )
    room_name = db.Column(
        db.VARCHAR(60).with_variant(
            mysql.VARCHAR(length=60, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    sort_key = db.Column(
        db.VARCHAR(60).with_variant(
            mysql.VARCHAR(length=60, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    description = db.Column(
        db.VARCHAR(60).with_variant(
            mysql.VARCHAR(length=60, **_varchar_params), "mysql"
        )
    )
    capacity = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
    )
    room_admin_email = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    invalid_types = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        server_default=None,
        comment="JSON encoded",
    )
    custom_html = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )

    area = db.relationship("Area", back_populates="rooms")

    __table_args__ = (
        db.UniqueConstraint("area_id", "room_name"),
        db.Index("ix_mrbs_room_sort_key", "sort_key"),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )

    def __str__(self):
        return self.area.area_name + ": " + self.room_name


class Repeat(db.Model):
    __tablename__ = "mrbs_repeat"
    id = db.Column(
        db.Integer,
        nullable=False,
        autoincrement=True,
        primary_key=True,
    )
    start_time = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
        comment="Unix timestamp",
    )
    end_time = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
        comment="Unix timestamp",
    )
    rep_type = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
    )
    end_date = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
        comment="Unix timestamp",
    )
    rep_opt = db.Column(
        db.VARCHAR(32).with_variant(
            mysql.VARCHAR(length=32, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    room_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "mrbs_room.id",
            ondelete="RESTRICT",
            onupdate="CASCADE",
        ),
        server_default=db.text("1"),
        nullable=False,
        index=True,
    )
    timestamp = db.Column(
        db.TIMESTAMP().with_variant(db.TIMESTAMP(timezone=True), "postgresql"),
        server_default=ModifiedDefault(),
        server_onupdate=db.FetchedValue(),
    )
    create_by = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    modified_by = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    name = db.Column(
        db.VARCHAR(82).with_variant(
            mysql.VARCHAR(length=82, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
        index=True,
    )
    project = db.relationship("Project")
    project_id = db.Column(
        db.Integer, db.ForeignKey("project.id"), nullable=False, index=True
    )
    operator = db.relationship("Operator")
    operator_id = db.Column(
        db.Integer,
        db.ForeignKey("operator.id"),
        nullable=False,
        index=True,
    )
    rate = db.relationship("Rate")
    rate_id = db.Column(
        db.Integer, db.ForeignKey("rate.id"), nullable=False, index=True
    )
    type = db.Column(
        db.CHAR,
        server_default="E",
        nullable=False,
    )
    description = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    rep_interval = db.Column(
        db.SmallInteger, server_default=db.text("1"), nullable=False
    )
    month_absolute = db.Column(db.SmallInteger)
    month_relative = db.Column(
        db.VARCHAR(4).with_variant(mysql.VARCHAR(length=4, **_varchar_params), "mysql"),
        server_default=None,
    )
    status = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(unsigned=True), "mysql"),
        nullable=False,
        server_default=db.text("0"),
    )
    reminded = db.Column(db.Integer)
    info_time = db.Column(db.Integer)
    info_user = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        )
    )
    info_text = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    ical_uid = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    ical_sequence = db.Column(
        db.SmallInteger,
        server_default=db.text("0"),
        nullable=False,
    )
    __table_args__ = (
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Entry(db.Model):
    __tablename__ = "mrbs_entry"
    id = db.Column(
        db.Integer,
        nullable=False,
        autoincrement=True,
        primary_key=True,
    )
    start_time = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
        comment="Unix timestamp",
    )
    end_time = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
        comment="Unix timestamp",
    )
    entry_type = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
    )
    repeat_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "mrbs_repeat.id",
            onupdate="CASCADE",
            ondelete="CASCADE",
        ),
        server_default=None,
        index=True,
    )

    room = db.relationship("Room")
    room_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "mrbs_room.id",
            ondelete="RESTRICT",
            onupdate="CASCADE",
        ),
        server_default=db.text("1"),
        nullable=False,
        index=True,
    )
    timestamp = db.Column(
        db.TIMESTAMP().with_variant(db.TIMESTAMP(timezone=True), "postgresql"),
        server_default=ModifiedDefault(),
        server_onupdate=db.FetchedValue(),
    )
    create_by = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    modified_by = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    name = db.Column(
        db.VARCHAR(82).with_variant(
            mysql.VARCHAR(length=82, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    project = db.relationship("Project")
    project_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "project.id",
        ),
        nullable=False,
        index=True,
    )
    operator = db.relationship("Operator")
    operator_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "operator.id",
        ),
        nullable=False,
        index=True,
    )
    rate = db.relationship("Rate")
    rate_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "rate.id",
        ),
        nullable=False,
        index=True,
    )
    type = db.Column(
        db.CHAR,
        server_default="E",
        nullable=False,
    )
    description = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    status = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(unsigned=True), "mysql"),
        nullable=False,
        server_default=db.text("0"),
    )
    reminded = db.Column(db.Integer)
    info_time = db.Column(db.Integer)
    info_user = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        )
    )
    info_text = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    ical_uid = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        server_default="",
        nullable=False,
    )
    ical_sequence = db.Column(
        db.SmallInteger,
        server_default=db.text("0"),
        nullable=False,
    )
    ical_recur_id = db.Column(
        db.VARCHAR(16).with_variant(
            mysql.VARCHAR(length=16, **_varchar_params), "mysql"
        )
    )
    allow_registration = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        nullable=False,
        server_default=db.text("0"),
    )
    registrant_limit_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        nullable=False,
        server_default=db.text("1"),
    )
    registrant_limit = db.Column(
        db.Integer, nullable=False, server_default=db.text("0")
    )
    registration_opens = db.Column(
        db.Integer,
        nullable=False,
        server_default="1209600",
        comment="Seconds before the start time",
    )
    registration_opens_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )
    registration_closes = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
        comment="Seconds before the start time",
    )
    registration_closes_enabled = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(1), "mysql"),
        server_default=db.text("0"),
        nullable=False,
    )

    __table_args__ = (
        db.Index("ix_mrbs_entry_name", "name"),
        db.Index("ix_mrbs_entry_start_time", "start_time"),
        db.Index("ix_mrbs_entry_end_time", "end_time"),
        db.Index(
            "ix_mrbs_entry_room_id_start_time_end_time",
            "room_id",
            "start_time",
            "end_time",
            unique=False,
        ),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )

    @hybrid_property
    def duration(self):
        return (self.end_time - self.start_time) / 3600.0

    @hybrid_property
    def cost(self):
        return self.duration * self.rate.hourly + self.rate.fixed

    @property
    def link(self):
        server = current_app.config.get("MRBS_URL", None)
        if server:
            return Markup(
                f'<a href="{server}/view_entry.php?id={self.id}">{self.entry_name}</a>'
            )
        return Markup(self.entry_name)

    @property
    def entry_name(self):
        timestamp = datetime.fromtimestamp(
            self.start_time, pytz.timezone(current_app.config.get("TIMEZONE", "UTC"))
        ).strftime("%A %Y-%m-%d %H:%M %Z")
        return f"{timestamp} ({self.room.room_name})"

    def __str__(self):
        return self.entry_name


class Participant(db.Model):
    __tablename__ = "mrbs_participants"
    id = db.Column(
        db.Integer,
        primary_key=True,
        nullable=False,
        autoincrement=True,
    )
    entry_id = db.Column(
        db.Integer,
        db.ForeignKey(
            "mrbs_entry.id",
            ondelete="CASCADE",
            onupdate="CASCADE",
        ),
        nullable=False,
        index=True,
    )
    username = db.Column(
        db.VARCHAR(191).with_variant(
            mysql.VARCHAR(length=191, **_varchar_params), "mysql"
        ),
        server_default=None,
    )
    create_by = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        server_default=None,
    )
    registered = db.Column(
        db.Integer,
        server_default=None,
    )

    __table_args__ = (
        db.UniqueConstraint("entry_id", "username"),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Variables(db.Model):
    __tablename__ = "mrbs_variables"
    id = db.Column(
        db.Integer,
        primary_key=True,
        nullable=False,
        autoincrement=True,
    )
    variable_name = db.Column(
        db.VARCHAR(80).with_variant(
            mysql.VARCHAR(length=80, **_varchar_params), "mysql"
        ),
    )
    variable_content = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    __table_args__ = (
        db.UniqueConstraint("variable_name"),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Zoneinfo(db.Model):
    __tablename__ = "mrbs_zoneinfo"
    id = db.Column(
        db.Integer,
        primary_key=True,
        nullable=False,
        autoincrement=True,
    )
    timezone = db.Column(
        db.VARCHAR(127).with_variant(
            mysql.VARCHAR(length=127, **_varchar_params), "mysql"
        ),
        nullable=False,
        server_default="",
    )
    outlook_compatible = db.Column(
        db.SmallInteger().with_variant(mysql.TINYINT(unsigned=True), "mysql"),
        nullable=False,
        server_default=db.text("0"),
    )
    vtimezone = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql")
    )
    last_updated = db.Column(
        db.Integer,
        nullable=False,
        server_default=db.text("0"),
    )
    __table_args__ = (
        db.UniqueConstraint("timezone", "outlook_compatible"),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Sessions(db.Model):
    __tablename__ = "mrbs_sessions"
    id = db.Column(
        db.VARCHAR(191).with_variant(
            mysql.VARCHAR(length=191, **_varchar_params), "mysql"
        ),
        nullable=False,
        primary_key=True,
    )
    access = db.Column(
        db.Integer().with_variant(mysql.INTEGER(unsigned=True), "mysql"),
        server_default=None,
        nullable=True,
        comment="Unix timestamp",
    )
    data = db.Column(
        db.Text().with_variant(mysql.TEXT(**_varchar_params), "mysql"),
        server_default=None,
        nullable=True,
    )
    __table_args__ = (
        db.Index("ix_mrbs_sessions_access", "access"),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Users(db.Model):
    __tablename__ = "mrbs_users"
    id = db.Column(
        db.Integer,
        primary_key=True,
        nullable=False,
        autoincrement=True,
    )
    level = db.Column(
        db.SmallInteger,
        server_default=db.text("0"),
        nullable=False,
    )
    name = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        nullable=False,
    )
    unique_id = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        unique=True,
        nullable=False,
    )
    display_name = db.Column(
        db.VARCHAR(191).with_variant(
            mysql.VARCHAR(length=191, **_varchar_params), "mysql"
        ),
        server_default=None,
    )
    password_hash = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        )
    )
    email = db.Column(
        db.VARCHAR(75).with_variant(
            mysql.VARCHAR(length=75, **_varchar_params), "mysql"
        )
    )
    timestamp = db.Column(
        db.TIMESTAMP().with_variant(db.TIMESTAMP(timezone=True), "postgresql"),
        server_default=ModifiedDefault(),
        server_onupdate=db.FetchedValue(),
    )
    last_login = db.Column(
        db.Integer,
        server_default=db.text("0"),
        nullable=False,
    )
    reset_key_hash = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        server_default=None,
    )
    reset_key_expiry = db.Column(
        db.Integer, nullable=False, server_default=db.text("0")
    )
    __table_args__ = (
        db.UniqueConstraint("name"),
        {
            "mysql_engine": "InnoDB",
            "mysql_charset": "utf8mb4",
            "mysql_collate": "utf8mb4_unicode_ci",
        },
    )


class Rate(db.Model):
    """
    Rate code
    """

    __tablename__ = "rate"
    id = db.Column(
        "id",
        db.Integer,
        primary_key=True,
    )
    name = db.Column(
        "name",
        db.VARCHAR(30).with_variant(
            mysql.VARCHAR(length=30, **_varchar_params), "mysql"
        ),
        unique=True,
        nullable=False,
    )
    hourly = db.Column(
        "hourly",
        db.Float,
        unique=False,
        nullable=False,
        server_default=db.text("0"),
        default=0,
    )
    fixed = db.Column(
        "fixed",
        db.Float,
        unique=False,
        nullable=False,
        server_default=db.text("0"),
        default=0,
    )
    active = db.Column(
        "active",
        db.Boolean,
        default=True,
        nullable=False,
    )

    projects = db.relationship("Project", back_populates="default_rate")

    def __str__(self):
        """
        String identification of rate

        :returns: string
        :raises: None
        """
        return self.name


class OAuth2Token(db.Model):
    __tablename__ = "mrbs_oauth2token"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(
        db.VARCHAR(255).with_variant(
            mysql.VARCHAR(length=255, **_varchar_params), "mysql"
        ),
        nullable=False,
        index=True,
    )
    access_token = db.Column(db.String(length=4096))
    refresh_token = db.Column(db.String(length=4096))
    id_token = db.Column(db.String(length=4096))
    expires_at = db.Column(db.Integer())
    sid = db.Column(db.String(length=256), index=True)
    session = db.Column(db.String(length=256), index=True)

    __table_args__ = (db.UniqueConstraint("name", "session"),)
