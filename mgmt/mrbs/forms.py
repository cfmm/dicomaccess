import datetime

import pytz
import tzlocal
from flask import Response
from flask_cfmm.fields import DateTimeTimezoneField
from flask_babel import gettext
from flask_login import current_user
from wtforms import ValidationError
from wtforms.fields import HiddenField, StringField
from wtforms.widgets import HiddenInput, TextInput
from wtforms_sqlalchemy.fields import QuerySelectMultipleField

from ..forms import RedirectForm
from ..models.dicom import Project, Principal
from .models import Rate, Room, Area
from .reports import Report


def first_day_of_last_quarter():
    ldlm = last_day_of_last_month()
    last_quarter = datetime.datetime(
        ldlm.year, ldlm.month, 1, 0, 0, 0, 0
    ) - datetime.timedelta(days=32)
    return datetime.datetime(last_quarter.year, last_quarter.month, 1, 0, 0, 0, 0)


def last_day_of_last_month():
    date = datetime.date.today()
    return datetime.datetime(date.year, date.month, 1, 0, 0, 0, 0) - datetime.timedelta(
        microseconds=1
    )


class TimezoneInput(HiddenInput):
    def __call__(self, field, **kwargs):
        if "class" in kwargs:
            kwargs["class"] += " "
        kwargs["class"] = kwargs.get("class", "") + "timezone-input"
        return super(TimezoneInput, self).__call__(field, **kwargs)


class TimezoneGuess(TextInput):
    def __call__(self, field, **kwargs):
        if "class" in kwargs:
            kwargs["class"] += " "
        kwargs["class"] = kwargs.get("class", "") + "timezone-guess"
        return super(TimezoneGuess, self).__call__(field, **kwargs)


class TimezoneValid:
    def __init__(self, message=None):
        self.message = message or gettext("Timezone is not valid")

    def __call__(self, form, field):
        try:
            pytz.timezone(field.data)
        except pytz.exceptions.UnknownTimeZoneError:
            raise ValidationError(self.message)


class ReportForm(RedirectForm):
    """
    Form for report generation
    """

    #: Report
    principal = QuerySelectMultipleField(
        label=gettext("Principal"),
        query_factory=lambda: Principal.accessible_to(current_user),
        render_kw={"size": 5, "style": "width: 100%"},
    )

    #: project field
    project = QuerySelectMultipleField(
        label=gettext("Project"),
        query_factory=lambda: Project.accessible_to(
            current_user, active=False
        ).order_by(Project.study),
        render_kw={"size": 5, "style": "width: 100%"},
    )

    #: Room
    room = QuerySelectMultipleField(
        label=gettext("Room"),
        query_factory=lambda: Room.query.filter(Room.disabled == 0)
        .join(Area, Room.area)
        .order_by(Area.area_name, Room.room_name),
        render_kw={"size": 5, "style": "width: 100%"},
    )

    #: Rate
    rate = QuerySelectMultipleField(
        label=gettext("Rate"),
        query_factory=lambda: Rate.query.order_by(Rate.name),
        render_kw={"size": 5, "style": "width: 100%"},
    )

    #: Entry description
    description = StringField(
        label=gettext("Entry Description"),
    )

    #: Start time
    start_time = DateTimeTimezoneField(
        label=gettext("Start"), default=first_day_of_last_quarter()
    )

    #: End time
    end_time = DateTimeTimezoneField(
        label=gettext("End"), default=last_day_of_last_month()
    )

    #: User entered timezone
    timezone = StringField(widget=TimezoneGuess(), validators=[TimezoneValid()])

    #: Hidden input that returns the timezone offset
    offset = HiddenField(widget=TimezoneInput())

    def generate_report(self, output_type="html"):
        start_time = self.start_time.data
        end_time = self.end_time.data

        interval = gettext("From {start} To {stop}").format(
            start=start_time.strftime("%B %d, %Y"), stop=end_time.strftime("%B %d, %Y")
        )

        # Convert times from local to UTC
        try:
            timezone = pytz.timezone(self.timezone.data)
            start_time = start_time.replace(tzinfo=timezone)
            end_time = end_time.replace(tzinfo=timezone)
            offset = 0
        except pytz.exceptions.UnknownTimeZoneError:
            timezone = tzlocal.get_localzone()
            offset = int(self.offset.data)
            start_time += datetime.timedelta(minutes=offset)
            end_time += datetime.timedelta(minutes=offset)

            # Set the timezone data
            start_time = start_time.replace(tzinfo=pytz.utc)
            end_time = end_time.replace(tzinfo=pytz.utc)

        epoch = datetime.datetime(1970, 1, 1, tzinfo=pytz.utc)

        data = Report.principal(
            principals=self.principal.data,
            projects=self.project.data,
            rooms=self.room.data,
            rates=self.rate.data,
            start=(start_time - epoch).total_seconds(),
            end=(end_time - epoch).total_seconds(),
            description=self.description.data,
            output_type=output_type,
            interval=interval,
            offset=offset,
            timezone=timezone,
        )

        if output_type in ["xml", "excel"]:
            data = Response(
                data,
                mimetype="application/xml",
                headers=[("Content-disposition", "attachment")],
            )

        return data
