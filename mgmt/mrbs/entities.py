from alembic_utils.pg_function import PGFunction
from alembic_utils.pg_trigger import PGTrigger
from alembic_utils.pg_view import PGView

from sqlalchemy.schema import DropTable
from sqlalchemy.ext.compiler import compiles
from sqlalchemy import case, not_
from sqlalchemy.orm import aliased

from sqlalchemy import select
from ..models.screening import ScreeningForm
from ..models.dicom import User, Operator, Principal, Project
from .models import Rate


@compiles(DropTable, "postgresql")
def _compile_drop_table(element, compiler, **kwargs):
    return f"{compiler.visit_drop_table(element)} CASCADE"


def get_entities(schema):
    if schema is None:
        schema = "public"

    prefix = f"{schema}."

    entities = list()
    entities.append(
        PGFunction(
            schema=schema,
            signature="get_entry_name(project_id int, operator_id int, alternate VARCHAR(82))",
            definition="RETURNS VARCHAR(88) AS $$ "
            "DECLARE "
            "operator_code VARCHAR(3); "
            "project_name VARCHAR(82); "
            "BEGIN "
            f"SELECT t1.code INTO operator_code FROM {prefix}operator AS t1 WHERE t1.id = operator_id; "
            f"SELECT t1.study INTO project_name FROM {prefix}project AS t1 WHERE t1.id = project_id; "
            "IF COALESCE(alternate, '') = '' OR alternate = '.' THEN "
            "RETURN project_name || ' (' || operator_code || ')';"
            "END IF; "
            "RETURN alternate; "
            "END; "
            "$$ language 'plpgsql';",
        )
    )

    entities.append(
        PGView(
            schema=schema,
            signature="mrbs_all_screening",
            definition=str(
                select(ScreeningForm.id, ScreeningForm.entry_id).select_from(
                    ScreeningForm
                )
            ),
        )
    )

    entities.append(
        PGView(
            schema=schema,
            signature="mrbs_operator",
            definition=str(
                select(
                    Operator.id.label("id"),
                    case(
                        (Operator.name.is_not(None), Operator.name), else_=User.username
                    ).label("name"),
                    Operator.code.label("code"),
                    Operator.active.label("active"),
                ).select_from(
                    Operator.__table__.outerjoin(User, Operator.user_id == User.id)
                )
            ),
        )
    )

    entities.append(
        PGView(
            schema=schema,
            signature="mrbs_principal",
            definition=str(
                select(
                    Principal.id.label("id"),
                    Principal.name.label("name"),
                    select(1)
                    .select_from(Project)
                    .filter(Project.principal_id == Principal.id)
                    .filter(Project.active)
                    .exists()
                    .label("active"),
                ).select_from(Principal)
            ),
        )
    )

    entities.append(
        PGView(
            schema=schema,
            signature="mrbs_rate",
            definition=str(
                select(
                    Rate.id.label("id"),
                    Rate.name.label("name"),
                    Rate.active.label("active"),
                ).select_from(Rate)
            ),
        )
    )

    screening_form_alias = aliased(ScreeningForm, name="screening_form_alias")
    entities.append(
        PGView(
            schema=schema,
            signature="mrbs_screening",
            definition=str(
                select(
                    ScreeningForm.id.label("id"),
                    ScreeningForm.entry_id.label("entry_id"),
                )
                .select_from(ScreeningForm)
                .filter(
                    not_(
                        select(1)
                        .select_from(screening_form_alias)
                        .filter(screening_form_alias.parent_form_id == ScreeningForm.id)
                        .exists()
                    )
                )
            ),
        )
    )

    entities.append(
        PGView(
            schema=schema,
            signature="mrbs_study",
            definition=str(
                select(
                    Project.id.label("id"),
                    Project.study.label("name"),
                    Project.principal_id.label("principal_id"),
                    Project.default_rate_id.label("rate_id"),
                    Project.default_operator_id.label("operator_id"),
                    Project.active.label("active"),
                ).select_from(Project)
            ),
        )
    )

    entities.append(
        PGFunction(
            schema=schema,
            signature="update_timestamp_column()",
            definition="""
        RETURNS TRIGGER AS $$
            BEGIN
                NEW.timestamp = NOW();
                RETURN NEW;
            END;
            $$ language 'plpgsql';
        """,
        )
    )

    entities.append(
        PGTrigger(
            schema=schema,
            signature="update_mrbs_entry_timestamp",
            on_entity=f"{prefix}mrbs_entry",
            definition=f"BEFORE UPDATE ON {prefix}mrbs_entry FOR EACH ROW "
            f"EXECUTE PROCEDURE {prefix}update_timestamp_column();",
        )
    )

    entities.append(
        PGTrigger(
            schema=schema,
            signature="update_mrbs_repeat_timestamp",
            on_entity=f"{prefix}mrbs_repeat",
            definition=f"BEFORE UPDATE ON {prefix}mrbs_repeat FOR EACH ROW "
            f"EXECUTE PROCEDURE {prefix}update_timestamp_column();",
        )
    )

    entities.append(
        PGTrigger(
            schema=schema,
            signature="update_mrbs_users_timestamp",
            on_entity=f"{prefix}mrbs_users",
            definition=f"BEFORE UPDATE ON {prefix}mrbs_users FOR EACH ROW "
            f"EXECUTE PROCEDURE {prefix}update_timestamp_column();",
        )
    )

    return entities
