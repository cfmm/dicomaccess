from urllib.parse import urlparse

from flask import flash, url_for, request, current_app
from flask_admin import BaseView, expose
from flask_babel import gettext
from flask_cfmm.security import create_user
from ldap3.core.exceptions import LDAPException

from .admin_base import DatacareAdminAccess
from .forms import (
    NewUserForm,
)
from .proxy import current_ldap, current_db
from .usermgmt import email as keycloak_email, KeycloakError
from .exceptions import UserCreationError


class NewUserView(DatacareAdminAccess, BaseView):
    @staticmethod
    def create_user(username, email, last_name, first_name):
        # Create
        user = create_user(
            username,
            email=email,
            last_name=last_name,
            first_name=first_name,
            active=False,
        )

        try:
            # Create the user in LDAP. It is required immediately and cannot wait on synchronization
            user.update()

            # Attempt to send the user email
            keycloak_email(user.username)
            flash(
                gettext(
                    "User {username} created. Activation email sent to {email}"
                ).format(username=user.username, email=email)
            )
        except (Exception, UserCreationError, LDAPException, KeycloakError) as err:
            flash(
                gettext("Unable to create {username}: {error}").format(
                    username=user.username, error=str(err)
                )
            )

            # Remove the user from LDAP
            current_ldap.remove(user)

            try:
                # Remove the user from the database
                current_db.session.delete(user)
                current_db.session.commit()
            except Exception as err_:
                current_app.logger.critical(
                    gettext("Failed to rollback DB user {username}: {error}").format(
                        username=user.username, error=str(err_)
                    )
                )
            raise UserCreationError

        # Mark user as active, so they can be assigned as owner of Principal
        user.active = True
        current_db.session.commit()

    @expose("/", methods=["GET", "POST"])
    def index(self):
        """
        View for creating a new user
        """
        form = NewUserForm(request.form)
        if form.validate_on_submit():
            try:
                self.create_user(
                    form.username.data,
                    form.email.data,
                    form.lastName.data,
                    form.firstName.data,
                )
            except UserCreationError:
                return self.render("newuser.html", form=form)

            # Prevent recursive redirect
            ref_loc = urlparse(form.next.data)
            if ref_loc[2] == url_for(self.endpoint + ".index"):
                form.next.data = url_for(".index")

            return form.redirect(url_for("admin.index"))

        return self.render("newuser.html", form=form)
