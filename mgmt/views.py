"""
Created on Jul 22, 2015

Copyright 2015, The University of Western Ontario

.. codeauthor:: mklassen
"""

import traceback
from contextlib import contextmanager
from datetime import timedelta
from functools import wraps

from flask import (
    request,
    redirect,
    url_for,
    flash,
    current_app,
    make_response,
    session,
    jsonify,
    abort,
)
from flask_babel import gettext
from flask_login import current_user, login_required
from ldap3.core.exceptions import LDAPConnectionIsReadOnlyError
from sqlalchemy import and_

from .models.dicom import Group, Project
from .models.screening import Subject
from .models.credentials import User, MembershipType
from .proxy import current_ldap
from .screening.admin import make_mrbs_query
from .user_reports import UserReport

current_app.permanent_session_lifetime = timedelta(
    minutes=current_app.config["SESSION_TIMEOUT"]
)


def admin_required(func):
    @wraps(func)
    @login_required
    def decorated_view(*args, **kwargs):
        if not current_user.is_admin:
            abort(403)
        return func(*args, **kwargs)

    return decorated_view


def datacare_required(func):
    @wraps(func)
    @login_required
    def decorated_view(*args, **kwargs):
        if not current_user.is_datacare:
            abort(403)
        return func(*args, **kwargs)

    return decorated_view


@current_app.before_request
def session_update():
    # Logged in users are assigned sessions that persist until expiry defined in config
    # Otherwise, the session is deleted when the browser is closed
    session.permanent = current_user is not None and current_user.is_authenticated

    session.modified = True


@current_app.route("/update_ldap", methods=["GET"])
@admin_required
def update_ldap_view():
    try:

        @contextmanager
        def handle_read_only():
            """Ignore read only connection error if LDAP is configured to be read-only."""
            try:
                yield
            except LDAPConnectionIsReadOnlyError:
                if current_ldap.config["read_only"]:
                    pass
                else:
                    raise

        current_app.logger.debug("Updating users...")
        for user in User.query.all():
            with handle_read_only():
                user.update()
        current_app.logger.debug("Updating groups...")
        for group in Group.query.all():
            with handle_read_only():
                current_ldap.make_group(group)
        current_app.logger.debug("Updating group memberships...")
        for group in Group.query.all():
            with handle_read_only():
                group.update(make_group=False)
        flash(gettext("LDAP server updated"))
    except Exception as err:
        flash(
            gettext("Unable to update LDAP server: {error}").format(error=str(err)),
            "error",
        )

    return redirect(url_for("admin.index"))


@current_app.route("/maintainers.xml", methods=["GET"])
@datacare_required
def maintainers():
    response = make_response(UserReport().maintainers())
    response.headers["Content-Type"] = "application/xml"
    return response


@current_app.route("/all_users.xml", methods=["GET"])
@datacare_required
def all_users():
    response = make_response(UserReport().all_users())
    response.headers["Content-Type"] = "application/xml"
    return response


@current_app.route("/principals.xml", methods=["GET"])
@datacare_required
def principals():
    response = make_response(UserReport().principals())
    response.headers["Content-Type"] = "application/xml"
    return response


@current_app.route("/update", methods=["GET"])
@current_app.route("/arc/sync", methods=["GET"])
@datacare_required
def sync_arc():
    try:
        if current_app.extensions["arc"].sync():
            flash(gettext("Successfully synchronized ARC settings"))
        else:
            flash(gettext("ARC settings already up to date"))
    except Exception as err:
        current_app.logger.error(
            gettext("{name}: Unknown error. Traceback: {traceback}").format(
                name="sync_arc", traceback=traceback.format_exc()
            )
        )
        flash(
            gettext("Encountered errors when synchronizing ARC settings: {err}").format(
                err=err
            ),
            "warning",
        )

    return redirect(url_for("admin.index"))


@current_app.route("/subject_search", methods=["GET"])
@datacare_required
def subject_search():
    """
    Subject search API

    Search active subjects for partial matches on surname and/or subject_id field
    """
    surname = request.args.get("surname")
    subjectid = request.args.get("subjectid")
    if not surname and not subjectid:
        return make_response("Missing query terms", 400)

    match_subquery = [Subject.active.is_(True)]
    if surname:
        match_subquery.append(Subject.surname.ilike("%%%s%%" % surname))
    if subjectid:
        match_subquery.append(Subject.subjectid.ilike("%%%s%%" % subjectid))

    matching_subjects = Subject.query.filter(and_(*match_subquery)).all()
    return jsonify([subj.as_dict() for subj in matching_subjects])


@current_app.route("/project_search", methods=["GET"])
def project_search():
    """
    Project search API

    Search active projects for which current user is allowed to send screening forms
    """
    projects = []
    if current_user.is_authenticated:
        principal = request.args.get("principal")
        if principal:
            projects = [
                {"id": p.id, "value": p.study}
                for p in Project.accessible_to(
                    current_user,
                    level=MembershipType.coordinator,
                )
                .filter(Project.principal_id == int(principal))
                .with_entities(Project.id, Project.study)
                .all()
            ]
    return jsonify(projects)


@current_app.route("/entry_search", methods=["GET"])
def entry_search():
    """
    Schedule entry search API

    :return:
    """
    matching_entries = []
    if current_user.is_authenticated:
        project = request.args.get("project")
        timestamp = request.args.get("timestamp")
        if project:
            query = make_mrbs_query(
                project, int(float(timestamp)) if timestamp else None
            )

            matching_entries = [{"id": p.id, "value": str(p)} for p in query.all()]

    return jsonify(matching_entries)


@current_app.route("/project_description", methods=["GET"])
@login_required
def project_description():
    """
    Project description API

    Given a project ID, return the description given
    """
    project_id = request.args.get("id")
    if not project_id:
        return make_response("Invalid project id", 400)
    description = (
        Project.accessible_to(
            current_user,
            level=MembershipType.coordinator,
        )
        .filter(Project.id == int(project_id))
        .with_entities(Project.participant_description)
        .scalar()
    )
    return jsonify({"participant_description": description or ""})
