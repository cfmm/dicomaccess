"""
Created on Jul 22, 2015

Copyright 2015, The University of Western Ontario

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

from .core import create_app
from .models import db

__all__ = (create_app, db)
