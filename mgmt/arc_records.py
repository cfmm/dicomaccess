import typing

import warnings

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    import ldap3

import ldap3.core.exceptions
from flask import current_app
from flask_babel import gettext

if typing.TYPE_CHECKING:
    from .arc import ArcConfig


class Record:
    object_class = "mgmtRecord"

    def __init__(self, source, conf: "ArcConfig"):
        self.source = source
        self.conf = conf
        self.valid = True

    @staticmethod
    def attributes(example: dict):
        return tuple(key for key in example if key != "dn")

    @property
    def dn(self):
        name = None
        for k, v in self.identifier.items():
            # This accommodates LDAP entries with DN that use multiple valued attributes
            if isinstance(v, list):
                name = f"{k}={f'+{k}='.join(v)}"
            else:
                name = f"{k}={v}"
            break
        if name is None:
            raise ValueError(
                gettext("Invalid mgmtRecord identifier: {item}").format(
                    item=self.identifier
                )
            )
        return f"{name},{self.base}"

    def lookup(self, attributes=None):
        # Lookup the entries via mgmt attributes
        entries = self.conf.search(
            search_base=self.search_base,
            search_filter=f"(&(objectClass={Record.object_class})(objectClass={self.object_class})"
            f"(mgmtId={self.source.id})(mgmtModel={self.source.__tablename__}))",
            attributes=attributes if attributes is not None else [],
            search_scope=self.search_scope,
        )

        return entries

    def existing_entries(self):
        entry = self.entry()
        entries = self.lookup(self.attributes(entry))
        return entries, entry

    def remove(self):
        modified = False
        for record in self.records:
            modified |= record._remove()
        return modified

    def _remove(self):
        modified = False
        entries = list()
        for entry in self.lookup(("mgmtId",)):
            if len(entry["attributes"]["mgmtId"]) > 1:
                # If the entry has more than one mgmtId associated with it then remove the mgmtId from the existing
                # record
                self.conf.conn.modify(
                    entry["dn"],
                    changes={"mgmtId": (ldap3.MODIFY_DELETE, [self.source.id])},
                )
                modified = True
            else:
                entries.append(entry)

        return self.conf.remove_entries(entries) or modified

    def pop_entry(self, entries: list) -> typing.Tuple[typing.Union[dict, None], dict]:
        """
        Find entry matching record using mgmt* LDAP attributes

        :param entries: list of ldap search responses
        :return: Tuple with ((Matched LDAP entry or None), entry attributes of this object)
        """
        attributes = self.entry()
        try:
            for entry in entries:
                if (
                    all(
                        entry["attributes"][key] == attributes[key]
                        for key in ("mgmtId", "mgmtModel")
                    )
                    and self.object_class in entry["attributes"]["objectClass"]
                ):
                    entries.remove(entry)
                    return entry, attributes
        except (KeyError, TypeError):
            pass
        return None, attributes

    def _modify(self, entries: list) -> bool:
        modified = False
        entry, attributes = self.pop_entry(entries)
        dn = self.dn

        # Type conversion to ensure comparison works correctly, e.g. list and set should be equal with the same
        # contents
        def changed(key) -> bool:
            try:
                a = entry["attributes"][key]
                b = attributes[key]
                if b is None:
                    return len(a) != 0
                assert type(a) is type(b), "Type mismatch when syncing ldap attributes"
                return a != b
            except KeyError:
                return False

        modifiable_keys = tuple(
            key for key in attributes if key not in ("objectClass",)
        )
        modify = entry is not None and any(changed(key) for key in modifiable_keys)
        if not entry:
            try:
                # Add entry with attributes that are not None
                self.conf.conn.add(
                    dn,
                    attributes={k: v for k, v in attributes.items() if v is not None},
                )
                current_app.logger.debug(f"Added {dn} on {self.conf.conn.server.host}")
                return True
            except ldap3.core.exceptions.LDAPEntryAlreadyExistsResult:
                current_app.logger.debug(
                    f"Found existing entry {dn} which was not created by this app. Will not sync."
                )
                return False
        elif entry["dn"].lower() != dn.lower():
            relative_dn = dn.split(",", 1)[0]
            try:
                self.conf.conn.modify_dn(entry["dn"], relative_dn, True)
            except ldap3.core.exceptions.LDAPNoSuchObjectResult:
                # Source was already moved by the parent
                pass
            except ldap3.core.exceptions.LDAPEntryAlreadyExistsResult:
                current_app.logger.debug(
                    f"Existing entry {dn} already exists. Will not sync."
                )
                return False
            modified = True

        if modify:
            changes = {
                key: (
                    ldap3.MODIFY_REPLACE,
                    attributes[key] if attributes[key] is not None else [],
                )
                for key in modifiable_keys
            }

            # Need to merge any existing mgmtId to allow for multiple values
            if entry:
                mgmt_id = set(entry["attributes"]["mgmtId"])
                mgmt_id.update(attributes["mgmtId"])
                changes["mgmtId"] = (ldap3.MODIFY_REPLACE, list(mgmt_id))

            self.conf.conn.modify(dn, changes=changes)
            current_app.logger.debug(f"Modified {dn} on {self.conf.conn.server.host}")
            modified = True

        return modified

    def update(self, entries):
        remove = False
        if entries is None:
            entries = list()
            for record in self.records:
                record_entries, _ = record.existing_entries()
                entries.extend(record_entries)
            remove = True

        modified = False
        for record in self.records:
            modified |= record._modify(entries)

        if remove:
            modified |= self.conf.remove_entries(entries)

        return modified

    @property
    def records(self):
        # noinspection PyRedundantParentheses
        return (self,)

    @property
    def identifier(self):
        return {"cn": [f"mgmt{self.source.__tablename__.capitalize()}{self.source.id}"]}

    @property
    def base(self):
        return self.conf.device_base

    @property
    def search_base(self):
        return self.base

    @property
    def search_scope(self):
        return ldap3.LEVEL

    def _entry(self) -> dict:
        return dict()

    def entry(self) -> dict:
        values = self.identifier
        values["mgmtId"] = [self.source.id]
        values["mgmtModel"] = self.source.__tablename__

        object_classes = [self.object_class]
        for base in self.__class__.__bases__:
            if hasattr(base, "object_class"):
                object_classes.append(base.object_class)

        values["objectClass"] = object_classes

        values.update(self._entry())
        return values


class RecordDirectDescendant(Record):
    @property
    def search_base(self):
        return self.base.split(",", 1)[1]

    @property
    def search_scope(self):
        return ldap3.SUBTREE

    def existing_entries(self):
        entries, entry = super().existing_entries()

        # Existing entries will be moved when the parent is moved
        for value in entries:
            dn = f"{value['dn'].split(',', 1)[0]},{self.base}"
            value["dn"] = dn
            value["raw_dn"] = dn.encode()

        return entries, entry


class Exporter(Record):
    object_class = "dcmExporter"

    def __init__(self, connection):
        super().__init__(connection.source, connection.conf)
        self.connection = connection

    @property
    def identifier(self):
        return {"dcmExporterID": [self.source.exporter_id]}

    def _entry(self) -> dict:
        attributes = {
            "dcmURI": f"{self.connection.name}:{self.source.aet}",
            "dicomAETitle": self.conf.aetitle,
            "dcmQueueName": self.conf.export_queue,
            "dcmExportAsSourceAE": True,
            "dicomDescription": self.source.description,
        }
        return attributes


class AETitle(Record):
    object_class = "dicomUniqueAETitle"

    @property
    def identifier(self):
        return {"dicomAETitle": self.source.aet}

    @property
    def base(self):
        return self.conf.aets_registry_dn


class Connection(RecordDirectDescendant):
    object_class = "dicomNetworkConnection"

    def __init__(self, device):
        super().__init__(device.source, device.conf)
        self.device = device

    @property
    def name(self):
        return "dicom-tls" if self.source.cipher else "dicom"

    @property
    def identifier(self):
        return {"cn": [self.name]}

    @property
    def base(self):
        return self.device.dn

    def _entry(self) -> dict:
        entry = {
            "dicomHostname": self.source.host,
            "dicomPort": self.source.port,
            "dicomTLSCipherSuite": (
                self.source.cipher.split(",") if self.source.cipher else None
            ),
        }
        return entry


class Network(RecordDirectDescendant):
    object_class = "dicomNetworkAE"

    def __init__(self, connection):
        super().__init__(connection.source, connection.conf)
        self.connection = connection

    @property
    def identifier(self):
        return {"dicomAETitle": self.source.aet}

    @property
    def base(self):
        return self.connection.device.dn

    def _entry(self) -> dict:
        return {
            "dicomNetworkConnectionReference": [
                self.connection.dn,
            ],
            "dicomAssociationInitiator": False,
            "dicomAssociationAcceptor": True,
        }


class Device(Record):
    object_class = "dicomDevice"

    @property
    def base(self):
        return self.conf.devices_base

    @property
    def identifier(self):
        return {"dicomDeviceName": self.source.exporter_id}

    def _entry(self) -> dict:
        return {
            "dicomInstalled": self.source.installed,
            "dicomInstitutionName": (
                [self.source.institution] if self.source.institution else None
            ),
            "dicomStationName": self.source.station_name,
            "dicomIssuerOfPatientID": self.source.pat_id_issuer,
            "dicomIssuerOfAccessionNumber": self.source.acc_no_issuer,
        }

    @property
    def records(self):
        # Devices records need additional records
        connection = Connection(self)
        return (
            self,
            Exporter(connection),
            AETitle(self.source, self.conf),
            connection,
            Network(connection),
        )


class ExportRule(Record):
    object_class = "dcmExportRule"

    def _entry(self) -> dict:
        if not self.source.server:
            raise ValueError(gettext("ExportRule source missing server"))

        return {
            "dcmEntity": "Series",
            "dcmExporterID": [self.source.server.exporter_id],
            "dcmDuration": self.source.delay or "PT1M",
            "dcmProperty": (
                self.source.project.dicom_properties() if self.source.project else None
            ),
            "dcmExportReoccurredInstances": "REPLACE",
            "dcmExportPreviousEntity": False,
        }


class AccessControl(Record):
    object_class = "dcmStoreAccessControlIDRule"

    def _entry(self) -> dict:
        return {
            "dcmStoreAccessControlID": (
                str(self.source.group.id) if self.source.group else "*"
            ),
            "dcmRulePriority": self.source.priority,
            "dcmProperty": self.source.dicom_properties(),
        }
