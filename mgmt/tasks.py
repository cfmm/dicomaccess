import typing as t
from datetime import datetime, timedelta, timezone
from itertools import chain

from celery import shared_task
from flask import current_app, jsonify, flash, url_for
from flask_babel import lazy_gettext, gettext

from .models.credentials import (
    db,
    User,
    Group,
    UserMembership,
    GroupMembership,
    MembershipType,
)
from .models.tasks import Status

if t.TYPE_CHECKING:
    from flask import Flask

from sqlalchemy.sql import literal_column


def run_task(method):
    task = method.apply_async()
    response = jsonify({})
    if task.state == "SUCCESS":
        flash(gettext("Task {name} completed").format(name=method.name))
    else:
        response.status_code = 202
    response.headers["Location"] = url_for(".status", task_id=task.id)
    return response


def update_ldap(task, klass) -> dict:
    total = klass.query.count()
    count = 1
    for obj in klass.query.order_by(klass.task_name).all():
        if task.request.id:
            task.update_state(
                state="PROGRESS",
                meta={
                    "current": count,
                    "total": total,
                    "name": klass.__name__,
                    "job": obj.task_name,
                },
            )
        count += 1
        obj.update(make_group=True)

    return {
        "current": total,
        "total": total,
        "name": klass.__name__,
        "result": True,
    }


@shared_task(bind=True)
def update_ldap_users(self):
    # noinspection PyTypeChecker
    return update_ldap(self, User)


@shared_task(bind=True)
def update_ldap_groups(self):
    # noinspection PyTypeChecker
    return update_ldap(self, Group)


def task_status(task_id):
    task = current_app.extensions["flask-celeryext"].celery.AsyncResult(task_id)
    try:
        if task.state == "PENDING":
            # job did not start yet
            response = {
                "state": task.state,
                "current": 0,
                "total": 1,
                "status": lazy_gettext("Pending..."),
            }
        elif task.state != "FAILURE":
            response = {
                "state": task.state,
                "current": task.info.get("current", 0),
                "total": task.info.get("total", 1),
                "status": gettext("Updating {name} {job}").format(
                    name=task.info.get("name", ""), job=task.info.get("job", "")
                ),
            }
            if "result" in task.info:
                message = gettext("Updating {name} completed").format(
                    name=task.info.get("name", "")
                )
                flash(message)
                response["result"] = message
        else:
            flash(gettext("Error in background task {0}".format(task.info)))
            current_app.logger.error(
                "Celery Task error: {0}\n{1}".format(
                    task.info, getattr(task, "backtrace", None)
                )
            )
            # something went wrong in the background job
            response = {
                "state": task.state,
                "current": 1,
                "total": 1,
                "status": str(task.info),
            }
    except AttributeError:
        current_app.logger.error(
            "Celery Task error: {0}\n{1}".format(
                task.info, getattr(task, "backtrace", None)
            )
        )
        response = {
            "state": "UNKNOWN",
            "current": 0,
            "total": 1,
            "status": gettext("Unknown state"),
        }

    return jsonify(response)


@shared_task(bind=True, ignore_result=True)
def update_expired(self):
    status = Status.query.first()

    last_run = status.expiry if status else datetime.fromtimestamp(0)
    this_run = datetime.now(timezone.utc)

    current_app.logger.debug(
        f"[{self.name}] last run: {last_run}. this run: {this_run}"
    )

    for membership in chain(
        UserMembership.query.filter(
            UserMembership.expires < this_run, UserMembership.expires >= last_run
        ).all(),
        GroupMembership.query.filter(
            GroupMembership.expires < this_run, GroupMembership.expires >= last_run
        ).all(),
    ):
        current_app.logger.debug(
            f"[{self.name}] updating expired membership of {str(membership.member)} in group {membership.group.name}."
        )
        membership.group.update()

    if not status:
        status = Status()
    status.expiry = this_run
    db.session.add(status)
    db.session.commit()

    # Revoke all other instances of the task
    scheduled_tasks = self.app.control.inspect().scheduled()
    tasks_to_revoke = [
        task["request"]["id"]
        for worker in scheduled_tasks
        for task in scheduled_tasks[worker]
        if task["request"]["name"] == self.name
    ]
    if tasks_to_revoke:
        current_app.logger.debug(
            f"[{self.name}]: Revoking {len(tasks_to_revoke)} tasks: {tasks_to_revoke}"
        )
        self.app.control.revoke(tasks_to_revoke)

    # Schedule the next expiry update
    schedule_next_expiry()


def schedule_next_expiry():
    status = Status.query.first()

    last_run = status.expiry if status else datetime.fromtimestamp(0)

    # Get the time of the next membership that will expire
    next_run = db.session.query(
        db.select(db.func.min(literal_column("expires_union")))
        .select_from(
            db.select(UserMembership.expires.label("expires_union"))
            .where(
                UserMembership.expires >= last_run,
                UserMembership.level == MembershipType.full,
            )
            .union(
                db.select(GroupMembership.expires.label("expires_union")).where(
                    GroupMembership.expires >= last_run
                )
            )
            .subquery()
        )
        .subquery()
    ).first()[0]

    # If there is an expiry in the future, schedule an update at least 1 second in the future
    # and 1 second after the next expiry
    if next_run:
        if isinstance(next_run, datetime):
            next_run = next_run.astimezone(timezone.utc)

        try:
            next_run = datetime.strptime(next_run, "%Y-%m-%d %H:%M:%S.%f").astimezone(
                timezone.utc
            )
        except (TypeError, ValueError):
            pass

        interval_datetime = next_run - datetime.now(timezone.utc)
        # next_run could be before now
        interval = max(0.0, interval_datetime.total_seconds()) + 1
        current_app.logger.debug(
            f"[update_expired] Found membership expiry on {next_run}. "
            f"Scheduling next run for {datetime.now(timezone.utc) + timedelta(seconds=interval)}."
        )
        update_expired.apply_async(countdown=interval)


def start_expiry(app: "Flask"):
    # Make sure that there is not another instance of the task already running
    celery = app.extensions["flask-celeryext"].celery
    data = celery.control.inspect()
    for worker, tasks in chain(data.active().items(), data.scheduled().items()):
        task_names = [t_["request"]["name"] for t_ in tasks]
        if "mgmt.tasks.update_expired" in task_names:
            return

    with app.app_context():
        schedule_next_expiry()
