"""
Created on Jul 22, 2015

Copyright 2015, The University of Western Ontario

.. module:: decorators
    :platform: Unix, Mac, Windows
    :synopsis: Utility functions and decorators

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

from flask import current_app
from flask_babel import gettext


def guard():
    if current_app.config["TESTING"]:
        raise RuntimeError(gettext("Unexpected testing error"))
