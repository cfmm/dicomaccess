import traceback

from flask import (
    Blueprint,
    request,
    flash,
    current_app,
    abort,
    make_response,
    jsonify,
)
from flask_babel import gettext
from flask_cfmm.exceptions import RequestNotFound, BadRequest
from flask_cfmm.resource import ResourceProtector
from flask_cfmm.keycloak import UMAPermission
from flask_login import current_user

from . import db
from .exceptions import UsernameExists, EmailExists, UserCreationError
from .models.credentials import User, MembershipType
from .models.dicom import Project
from .tasks import (
    update_ldap_users,
    update_ldap_groups,
    task_status,
    run_task as run_celery_task,
)
from .user import NewUserView
from .arc_schema import make_ldif
from .proxy import current_db
from .arc import ArcUpdate

bp = Blueprint("api", __name__, url_prefix="/api")

protector = ResourceProtector(name=__name__)


@bp.route("/ldap_users", methods=["POST"])
@protector(criteria=dict(permissions=[UMAPermission(resource="ldap", scope="update")]))
def ldap_users():
    response = run_celery_task(update_ldap_users)

    if response.status_code not in (200, 202):
        flash(
            gettext("Failed to start update AD user task: {error}").format(
                error=str(response.data)
            )
        )
    return response


@bp.route("/ldap_groups", methods=["POST"])
@protector(criteria=dict(permissions=[UMAPermission(resource="ldap", scope="update")]))
def ldap_groups():
    response = run_celery_task(update_ldap_groups)

    if response.status_code not in (200, 202):
        flash(
            gettext("Failed to start update AD group task: {error}").format(
                error=str(response.data)
            )
        )
    return response


@bp.route("/status/<task_id>", methods=["GET"])
def status(task_id):
    return task_status(task_id)


@bp.route("/user/remove", methods=["PUT", "POST"])
@protector(criteria=dict(permissions=[UMAPermission(resource="user", scope="remove")]))
def api_remove():
    """
    Handle local user removal
    """
    data = request.get_json()
    # ensure user is local and existing
    # noinspection PyUnresolvedReferences
    user = User.query.filter(User.username == data["username"]).first()
    if not user:
        raise RequestNotFound("User not found")
    if not user.is_local:
        raise BadRequest("Cannot delete non-local users")

    current_db.session.delete(user)
    current_db.session.commit()

    return make_response("Success", 200)


@bp.route("/user/create", methods=["PUT", "POST"])
@protector(criteria=dict(permissions=[UMAPermission(resource="user", scope="create")]))
def api_create():
    """
    Handle local user creation
    """
    current_db.session.commit()

    data = request.get_json()
    try:
        NewUserView.create_user(
            data["username"],
            data["mail"],
            data["lastName"],
            data.get("firstName", None),
        )
    except UsernameExists:
        raise BadRequest(gettext("Username exists"))
    except EmailExists:
        raise BadRequest(gettext("Email exists"))
    except UserCreationError:
        raise BadRequest(gettext("Will not perform"))
    except KeyError as err:
        raise BadRequest(gettext("Missing {key}").format(err))

    return make_response("Success", 200)


@bp.route("/arc/sync", methods=["POST"])
@protector(criteria=dict(permissions=[UMAPermission(resource="arc", scope="sync")]))
def sync_arc():
    updater = current_app.extensions["arc"]
    # noinspection PyBroadException
    try:
        config = request.get_json(silent=True)
        if config is not None:
            updater = ArcUpdate(config)
    except Exception:
        pass

    try:
        updater.sync()
    except Exception as err:
        current_app.logger.error(
            gettext("{name}: {error}\nTraceback: {traceback}").format(
                name="arc_sync", error=str(err), traceback=traceback.format_exc()
            )
        )
        abort(400)
    return make_response("Success", 200)


@bp.route("/schema", methods=["GET"])
def api_schema():
    return make_response(make_ldif(), 200)


@bp.route("/projects", methods=["GET"])
@protector(
    criteria=dict(permissions=[UMAPermission(resource="project", scope="query")])
)
def api_projects():
    with db.session.no_autoflush:
        if current_user is None or not current_user.is_authenticated:
            return make_response("No such user", 400)

        response = jsonify(get_projects(current_user))

        # response should not be cached
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        response.headers["Pragma"] = "no-cache"
        response.headers["Expires"] = "0"

        return response, 200


def get_projects(user):
    query = Project.accessible_to(
        user, level=MembershipType.coordinator, active=False
    ).order_by(Project.study)
    return project_list(query)


def project_list(query):
    data = {
        "projects": list(),
        "principals": dict(),
    }
    projects = data["projects"]
    principals = data["principals"]
    for record in query:
        project_id = f"0{record.id}"
        principal_id = f"0{record.principal_id}"
        projects.append(
            (
                project_id,
                {
                    "name": record.study,
                    "principal_id": principal_id,
                    "rate_id": f"0{record.default_rate_id}",
                    "operator_id": f"0{record.default_operator_id}",
                    "active": record.active,
                },
            )
        )
        if record.active:
            if principal_id not in principals:
                principals[principal_id] = [project_id]
            else:
                principals[principal_id].append(project_id)
    data["principals"] = list(principals.items())
    return data
