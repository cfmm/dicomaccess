"""
Send notification emails

Copyright 2015, The University of Western Ontario

.. module:: emails
    :platform: Unix, Mac, Windows
    :synopsis: Send asynchronous emails

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

from flask import current_app
from flask_mail import Message


def send_email(
    subject, sender, recipients, text_body, html_body, send_async=True, **message_params
):
    """
    Send an email

    :param subject: email subject
    :param sender: sender email address
    :param recipients: email addresses of recipients
    :param text_body: Text for email body
    :param html_body: HTML for email body
    :param send_async: Send asynchronously if True (default)
    :param message_params: any other valid parameters of `flask_email.Message`
    """

    if current_app.extensions["mail"].server != "":
        msg = Message(subject, sender=sender, recipients=recipients, **message_params)
        msg.body = text_body
        msg.html = html_body

        current_app.extensions["mail"].send(msg)
