import typing
from flask import request, flash

# noinspection PyProtectedMember
from flask_admin._compat import iteritems
from flask_admin.form import DateTimeField
from flask_admin.model.form import InlineFormAdmin
from flask_admin.model.ajax import AjaxModelLoader
from flask_admin.model.fields import AjaxSelectMultipleField, AjaxSelectField
from flask_admin.model.helpers import get_mdict_item_or_list
from flask_admin.contrib.sqla.form import InlineModelConverter, create_ajax_loader
from flask_admin.contrib.sqla.fields import QuerySelectField
from flask_babel import gettext
from flask_cfmm import ModelAdminView
from flask_cfmm.filters import FilterMember, FilterNotMember
from sqlalchemy import or_
from sqlalchemy.orm import load_only
from wtforms import ValidationError

from . import db
from .models.credentials import Group, User, UserMembership, GroupMembership
from .models.dicom import Project, Principal
from .proxy import current_ldap


class MemberAjaxModelLoader(AjaxModelLoader):
    def __init__(self, *args, users=True, groups=True, pattern=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.users = users
        self.groups = groups
        self.pattern = pattern
        self.model = None

    def format(self, model):
        """
        Return (id, name) tuple from the model.

        Member is not an actual model, but an LDAP entry, so the model is the DN and the name is the CN
        """
        if isinstance(model, tuple):
            return model[0], model[1]

        return model, current_ldap.extract_cn(model)

    def get_one(self, pk):
        """
        Find model by its primary key.

        :param pk:
            Primary key value
        """
        return pk

    def get_list(self, query, offset=0, limit=10):
        """
        Return models that match `query`.

        :param query:
            Query string
        :param offset:
            Offset
        :param limit:
            Limit
        """
        not_memberof, restricted_users = self.restrict()

        def filter_function(x):
            # filter results to exclude groups that are managed by the app
            # return only external groups
            return not Group.find_by_dn(x[0]) if self.groups else True

        yield from filter(
            filter_function,
            current_ldap.find_member(
                criteria=query,
                not_memberof=not_memberof,
                pattern=self.pattern,
                restricted_users=restricted_users,
                offset=offset,
                limit=limit,
                users=self.users,
                groups=self.groups,
            ),
        )

    def configure_url_options(self, options, model, model_id):
        self.model = model
        options["ajax_parent_id"] = model_id

    def restrict(self):
        group_id = request.args.get("ajax_parent_id")
        if group_id is None or self.model is None:
            return None, None

        group = db.session.get(self.model, group_id)
        if group is None:
            return None, None

        if not isinstance(group, Group):
            group = group.group

        if self.users:
            existing_users = set(group.users)
            if group.principal:
                existing_users.add(group.principal.user)
            elif group.project:
                existing_users.add(group.project.principal.user)
            restricted_users = set(user.username for user in existing_users)
        else:
            restricted_users = None

        return group.identifier, restricted_users


class UserAjaxModelLoader(MemberAjaxModelLoader):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.users = True
        self.groups = False

    def format(self, model):
        if not model:
            return

        if isinstance(model, tuple):
            return model[0], model[1]

        return model.dn, model.username

    def get_one(self, pk):
        return User.find_by_dn(pk, create=True)


class LdapAjaxGroupMemberLookupMixin:
    model: typing.Union[Group, Project, Principal]
    _form_ajax_refs: typing.Dict

    def get_url(self, endpoint, **kwargs):
        if endpoint == ".ajax_lookup":
            name = kwargs.get("name")
            if name is not None:
                loader = self._form_ajax_refs.get(name)
                if loader is not None:
                    model_id = get_mdict_item_or_list(request.args, "id")
                    loader.configure_url_options(kwargs, self.model, model_id)

        # noinspection PyUnresolvedReferences
        return super().get_url(endpoint, **kwargs)


# noinspection PyAbstractClass
class GroupAjaxSelectMultipleField(AjaxSelectMultipleField):
    separator = ";"


# noinspection PyAbstractClass
class MemberAjaxSelectField(AjaxSelectField):
    separator = ";"


# noinspection PyProtectedMember
class MemberInlineModelConverter(InlineModelConverter):
    # The loader must have name == new-name, but it is model based and therefore unknown to the form
    # So we allow a callable with the appropriate name
    def process_ajax_refs(self, info):
        refs = getattr(info, "form_ajax_refs", None)

        result = {}

        if refs:
            for name, opts in iteritems(refs):
                new_name = "%s-%s" % (info.model.__name__.lower(), name)

                if isinstance(opts, dict):
                    loader = create_ajax_loader(
                        info.model, self.session, new_name, name, opts
                    )
                elif callable(opts):
                    loader = opts(new_name)
                else:
                    loader = opts

                result[name] = loader
                self.view._form_ajax_refs[new_name] = loader

                # Also assign the loader through the form_args
                if name in info.form_args:
                    info.form_args[name]["loader"] = loader
                else:
                    info.form_args[name] = {"loader": loader}

        return result

    def _calculate_mapping_key_pair(self, model, info):
        # The key mapping cannot be automatically determined because GroupMembership is a many-
        # to-many relationship between groups and groups. Because the forward and reverse have
        # the same type you cannot identify the correct relationship by type. So the same class
        # can be used for UserMembership and GroupMembership, the relationship is not hardcoded.

        # Members always are for groups, but may be via association proxy (Project, Principal)
        reference = model
        if not issubclass(model, Group):
            model = Group

        mapper = model._sa_class_manager.mapper

        # Find property from target model to current model
        # Use the base mapper to support inheritance
        target_mapper = info.model._sa_class_manager.mapper.base_mapper

        for prop in target_mapper.iterate_properties:
            if hasattr(prop, "direction") and prop.direction.name in (
                "MANYTOONE",
                "MANYTOMANY",
            ):
                # The reverse property (Membership to Group) is never member
                if issubclass(model, prop.mapper.class_) and prop.key != "member":
                    reverse_prop = prop
                    break
        else:
            raise Exception("Cannot find reverse relation for model %s" % info.model)

        # Find forward property
        if prop.direction.name == "MANYTOONE":
            candidate = "ONETOMANY"
        else:
            candidate = "MANYTOMANY"

        for prop in mapper.iterate_properties:
            if hasattr(prop, "direction") and prop.direction.name == candidate:
                # The forward property (Group to Membership) is not member or membership property
                if (
                    prop.mapper.class_ == target_mapper.class_
                    and not prop.key.startswith("member")
                ):
                    forward_prop = prop
                    break
        else:
            raise Exception("Cannot find forward relation for model %s" % info.model)

        # Confirm that association proxy provides access to group
        if not hasattr(reference, forward_prop.key):
            raise Exception("Cannot find forward relation for model %s" % reference)

        return forward_prop.key, reverse_prop.key


class ExpiresField(DateTimeField):
    def process_formdata(self, valuelist):
        # Expires field is nullable
        try:
            super().process_formdata(valuelist)
        except ValueError:
            pass


class InlineUserMember(InlineFormAdmin):
    inline_converter = MemberInlineModelConverter
    form_columns = ("id", "member", "expires", "level")

    form_overrides = {"member": MemberAjaxSelectField, "expires": ExpiresField}

    form_ajax_refs = {
        "member": lambda name: UserAjaxModelLoader(
            name,
            {"minimum_input_length": 3},
            users=True,
            groups=False,
        )
    }

    form_args = {
        "member": {
            "label": gettext("User"),
        },
    }

    form_widget_args = {
        "member": {
            "data-delay": 1000,  # debounce the input to reduce queries while user is typing
            "data-placeholder": gettext("Click to find users"),
        }
    }


class UniqueMemberValidator:
    def __call__(self, form, field):
        members = [v["member"].id for v in field.data if v["member"] is not None]
        if len(members) != len(set(members)):
            message = f"Duplicate member in '{field.label.text}' field"

            # Validation error message does not show for inline field
            # Display as flash message in order to make it visible
            flash(f"Duplicate member in '{field.label.text}' field", "error")

            raise ValidationError(message)


# noinspection PyAbstractClass
class GroupMembershipQuerySelectField(QuerySelectField):
    def __init__(self, parent_model_class, **kwargs):
        super().__init__(**kwargs)
        # parent_model_class is needed to know what table the 'id' column in the requests is referring to
        obj_id = get_mdict_item_or_list(request.args, "id")

        obj = db.session.get(parent_model_class, obj_id) if obj_id is not None else None

        query = Group.query
        if obj is not None:
            if parent_model_class == Project or parent_model_class == Principal:
                obj = obj.group
            elif parent_model_class != Group:
                raise RuntimeError("Invalid object type for group member select field")

            query = query.filter(Group.id != obj.id)
            if obj.principal:
                # Do not allow adding project groups for principal's projects
                query = query.outerjoin(Project).filter(
                    or_(
                        Project.principal_id != obj.principal.id,
                        Project.principal_id.is_(None),
                    )
                )
            elif obj.project:
                # Do not allow adding principal group for project's principal
                query = query.filter(Group.id != obj.project.principal.group_id)

        self.query = query.order_by(Group.name).options(load_only(Group.id, Group.name))


class InlineGroupMember(InlineFormAdmin):
    def __init__(self, model, parent_model_class, **kwargs):
        self.form_args = {"member": {"parent_model_class": parent_model_class}}
        super().__init__(model, **kwargs)

    inline_converter = MemberInlineModelConverter
    column_labels = {"member": gettext("Group")}
    form_columns = ("id", "member", "expires")
    form_overrides = {
        "expires": ExpiresField,
        "member": GroupMembershipQuerySelectField,
    }


class InlineUserMembership(InlineFormAdmin):
    form_columns = ("id", "group", "expires", "level")
    form_args = {
        "group": {
            "query_factory": lambda: Group.query.order_by(Group.name).options(
                load_only(Group.id, Group.name)
            )
        },
    }
    form_overrides = {
        "expires": ExpiresField,
    }


class MembershipView(ModelAdminView):
    column_list = (
        "member",
        "group",
        "level",
        "expires",
        "warned",
    )
    form_columns = (
        "member",
        "group",
        "expires",
    )
    column_searchable_list = (
        "member.username",
        "group.name",
    )
    column_filters = (
        "expires",
        "warned",
        FilterMember(UserMembership.member),
        FilterNotMember(UserMembership.member),
        FilterMember(UserMembership.group),
        FilterNotMember(UserMembership.group),
    )
    column_default_sort = "expires"

    column_sortable_list = (
        ("member", "member.username"),
        ("group", "group.name"),
        "level",
        "expires",
        "warned",
    )

    form_args = {
        "group": {
            "query_factory": lambda: Group.query.order_by(Group.name).options(
                load_only(Group.id, Group.name)
            )
        },
        "member": {
            "query_factory": lambda: User.query.order_by(User.username).options(
                load_only(User.id, User.username)
            )
        },
    }
    form_extra_fields = {
        "expires": ExpiresField(),
    }


# noinspection PyUnresolvedReferences
class GroupMembershipView(MembershipView):
    column_list = (
        "member",
        "group",
        "expires",
    )
    column_searchable_list = (
        "member.name",
        "group.name",
    )
    column_filters = (
        "expires",
        FilterMember(GroupMembership.member),
        FilterNotMember(GroupMembership.member),
        FilterMember(GroupMembership.group),
        FilterNotMember(GroupMembership.group),
    )
    column_sortable_list = (
        ("member", "member.name"),
        ("group", "group.name"),
        "expires",
    )

    form_args = {
        "group": {
            "query_factory": lambda: Group.query.order_by(Group.name).options(
                load_only(Group.id, Group.name)
            )
        },
        "member": {
            "query_factory": lambda: Group.query.order_by(Group.name).options(
                load_only(Group.id, Group.name)
            )
        },
    }
