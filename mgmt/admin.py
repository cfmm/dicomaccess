"""
Administrative views for Server

Created on Nov 24, 2014

Copyright 2014, The University of Western Ontario

.. module:: admin
    :platform: Unix, Mac, Windows
    :synopsis: Model views for Flask application

.. moduleauthor:: Martyn Klassen <mklassen@robarts.ca>
.. codeauthor:: mklassen
"""

import socket
import uuid

from collections import OrderedDict
from flask import flash
from flask import redirect, url_for, request
from flask_admin import BaseView, expose, helpers
from flask_admin.form import BaseForm
from flask_admin.contrib.sqla.fields import QuerySelectField
from flask_admin.contrib.sqla.form import AdminModelConverter
from flask_admin.contrib.sqla.view import func
from flask_admin.helpers import get_redirect_target
from flask_admin.menu import MenuLink
from flask_admin.model.form import InlineFormAdmin, converts
from flask_admin.model.helpers import get_mdict_item_or_list
from flask_babel import gettext
from flask_cfmm import Admin as AdminBase
from flask_cfmm.filters import member_filters
from flask_cfmm.fields import disable_field
from flask_login import current_user
from markupsafe import Markup
from wtforms import PasswordField
from wtforms.utils import unset_value

from .admin_base import (
    AdminAccess,
    MyModelView,
    DatacareAccess,
    DatacareAdminAccess,
    UserAccess,
    DividerMenu,
    DatacareAdminDividerMenu,
    AuthenticatedAccess,
    OperationsDividerMenu,
    CoordinatorDividerMenu,
)
from .membership import (
    GroupMembershipView,
    MembershipView,
    InlineUserMembership,
    InlineUserMember,
    InlineGroupMember,
    LdapAjaxGroupMemberLookupMixin,
    UniqueMemberValidator,
)

from .models.credentials import db, User, UserMembership, GroupMembership
from .models.dicom import (
    Principal,
    Project,
    Destination,
    Server,
    Operator,
    Attribute,
    Tag,
    Group,
    Variable,
)
from .models.screening import ScreeningForm, Device, Subject
from .mrbs.forms import ReportForm
from .mrbs.models import Rate
from .screening.admin import (
    ScreeningFormDatacareView,
    ScreeningFormCoordinatorView,
    DateField,
)
from .screening.views import CoordinatorSendView
from .user import NewUserView


# noinspection PyUnusedLocal
def _group_formatter(self, context, model, name):
    if current_user.is_admin:
        view = "admin_group"
    elif current_user.is_datacare:
        view = "dc_group"
    else:
        view = "group"

    return Markup(
        "<a href='%s'>%s</a>"
        % (url_for(view + ".edit_view", id=model.group.id), model.group)
    )


class LoggedInMenuLink(MenuLink):
    def is_accessible(self):
        """
        Only authenticated users can see the link

        :return: boolean
        """
        return current_user.is_authenticated


class ReportView(AuthenticatedAccess, BaseView):
    @expose("/")
    def index(self):
        return redirect(url_for(".report"))

    @expose("/report", methods=["GET", "POST"])
    def report(self):
        form = ReportForm(request.form)
        if request.method == "POST" and form.validate():
            output_type = "html"
            if request.form.get("action") == "export":
                output_type = "excel"
            return form.generate_report(output_type=output_type)

        return self.render(
            "mrbs/form.html", form=form, admin_view=self, h=helpers, get_url=url_for
        )

    def is_accessible(self):
        return super().is_accessible() and current_user.has_projects


class InlineAttribute(InlineFormAdmin):
    """
    View definition for inline :class:`Attribute`
    """

    form_columns = (
        "id",
        "tag",
        "value",
    )


# noinspection PyAbstractClass
class ServerField(QuerySelectField):
    def process(self, formdata, data=unset_value, extra_filters=None):
        if data:
            sql_filter = db.or_(
                Server.owner_id == current_user.id,
                Server.id == getattr(data, "id", None),
            )
        else:
            sql_filter = db.or_(Server.owner_id == current_user.id)

        self.query = (
            Server.query.order_by(Server.aet)
            .filter(sql_filter)
            .options(db.load_only(Server.id, Server.aet))
        )
        super().process(formdata, data, extra_filters)


# noinspection PyUnresolvedReferences
class InlineDestination(InlineFormAdmin):
    form_overrides = {"server": ServerField}


class GroupBaseForm(BaseForm):
    special = ("user_member", "group_member")

    def __init__(self, *args, **kwargs):
        """
        Turn the fields dict into an OrderedDict and put the fields in the 'special' list at the end.
        This is done to ensure those fields are at the bottom of the rendered form.
        """
        super().__init__(*args, **kwargs)
        fields = OrderedDict()
        for key, value in self._fields.items():
            if key not in self.special:
                fields[key] = value
        for key in self.special:
            if key in self._fields:
                fields[key] = self._fields[key]
        self._fields = fields


class GroupBaseView(LdapAjaxGroupMemberLookupMixin, MyModelView):
    def __init__(self, model, *args, **kwargs):
        self._add_inline_models(
            (
                InlineUserMember(UserMembership),
                InlineGroupMember(GroupMembership, parent_model_class=model),
            ),
            append=True,
        )
        super().__init__(model, *args, **kwargs)

    def _add_inline_models(self, my_inline_models, append=True):
        if self.inline_models:
            if append:
                self.inline_models += my_inline_models
            else:
                self.inline_models = my_inline_models + self.inline_models
        else:
            self.inline_models = my_inline_models

    form_base_class = GroupBaseForm

    form_columns = (
        "user_member",
        "group_member",
    )
    column_labels = dict(
        user_member=gettext("User Members"),
        group_member=gettext("Group Members"),
    )

    form_args = {
        "group_member": {"validators": [UniqueMemberValidator()]},
        "user_member": {"validators": [UniqueMemberValidator()]},
    }


class GroupView(GroupBaseView):
    """
    Customized view of Group
    """

    column_default_sort = "name"

    column_searchable_list = (
        Group.name,
        Group.description,
        "id",
    )
    column_filters = (
        Group.name,
        Group.description,
        "principal.name",
        "project.name",
    )
    column_list = (
        "name",
        "description",
        "type",
        "cn",
    )
    form_columns = (
        "name",
        "description",
    ) + GroupBaseView.form_columns

    column_labels = dict(
        GroupBaseView.column_labels,
        description=gettext("Description"),
        type=gettext("Type"),
    )

    @expose("/delete/", methods=("POST",))
    def delete_view(self):
        # Cannot delete groups associated with principals or projects
        form = self.delete_form()
        group_id = form.id.data
        model = self.get_one(group_id)
        if model.is_dicom:
            flash(
                gettext("Cannot delete groups associated with principals or projects."),
                "warning",
            )
            return redirect(get_redirect_target() or self.get_url(".index_view"))

        return super().delete_view()

    @expose("/edit/", methods=("GET", "POST"))
    def edit_view(self):
        """
        Allow URLs with name argument instead of id by looking up the row by name
        """
        name = get_mdict_item_or_list(request.args, "name")
        if name:
            try:
                model = self.model.query.filter(self.model.name == name).one_or_none()
                if model:
                    return redirect(url_for(".edit_view", id=model.id))
            except AttributeError:
                pass

        return super(GroupView, self).edit_view()


class GroupUserView(UserAccess, GroupView):
    can_delete = False
    can_create = False

    def __init__(self, *args, **kwargs):
        self.form_widget_args = dict(super().form_widget_args or dict())
        self.form_widget_args["name"] = {"readonly": ""}
        super(GroupUserView, self).__init__(*args, **kwargs)

    def get_query(self):
        """
        Restrict to :class:`Group` of which user is a member
        """

        # Project groups of a principal which the user is a direct full member or owner
        # Principal groups of which the user is a direct full member or owner

        # Get all direct user memberships
        member_of = UserMembership.member_of(current_user)

        principal_alias = db.aliased(Principal)
        return (
            Group.query.outerjoin(Group.project)
            .outerjoin(Project.principal)
            .outerjoin(principal_alias, principal_alias.group_id == Group.id)
            .filter(
                db.func.coalesce(Principal.group_id, principal_alias.group_id).in_(
                    member_of
                )
            )
        )

    def get_count_query(self):
        """
        Count the number of items in the query
        """
        return self.get_query().with_entities(func.count(Group.id))


class GroupDatacareView(DatacareAccess, GroupView):
    can_delete = False
    can_create = False

    def get_query(self):
        """
        Restrict to :class:`Group` of which user is a member
        """
        # Only allow Datacare to see DICOM groups
        q1 = Group.query.join(Group.project)
        q2 = Group.query.join(Group.principal)
        return q1.union(q2)

    def get_count_query(self):
        """
        Count the number of items in the query
        """
        return self.get_query().with_entities(func.count(Group.id))


class GroupAdminView(AdminAccess, GroupView):
    """
    Customized view of Group for administrators
    """

    column_list = (
        "name",
        "description",
        "type",
        "cn",
        "id",
    )
    column_sortable_list = (
        "name",
        "description",
        "id",
    )
    form_columns = (
        "name",
        "description",
    )

    column_filters = (
        Group.name,
        Group.description,
        Principal.name,
        Project.name,
        Group.id,
        Group.is_project,
        Group.is_principal,
        Group.is_dicom,
    )


class PrincipalView(DatacareAdminAccess, GroupBaseView):
    """
    Customized view for :class:`Principal` for administrators
    """

    column_default_sort = "name"
    column_searchable_list = (
        Principal.id,
        Principal.name,
        User.username,
        Group.name,
    )
    column_filters = (
        Principal.name,
        User.username,
        Group.name,
    )
    form_columns = (
        "name",
        "user",
    ) + GroupBaseView.form_columns
    column_list = (
        "id",
        "name",
        "user",
        "group",
    )

    column_descriptions = {"user": gettext("Principal owner")}

    column_formatters = {"group": _group_formatter}


class ProjectView(GroupBaseView):
    """
    Customize view for projects
    """

    def __init__(self, *args, **kwargs):
        self._add_inline_models((InlineDestination(Destination),), append=False)
        super().__init__(*args, **kwargs)

    column_default_sort = "name"
    column_select_related_list = (Project.principal,)
    column_searchable_list = (
        Project.id,
        Project.name,
        Project.description,
        Principal.name,
    )
    column_filters = (Project.name, Project.description, Project.active, Principal.name)
    form_columns = (
        "name",
        "description",
        "participant_description",
        "principal",
        "destinations",
    ) + GroupBaseView.form_columns

    column_descriptions = {
        "name": gettext("Project name (Exam from Siemens Exam Explorer)"),
        "participant_description": gettext(
            "Study/project description intended for study participants "
            "and other non-technical audiences"
        ),
        "principal": gettext(
            "Principal associated with this project. Cannot be modified after Project creation"
        ),
        "default_rate": gettext(
            "Rate to be selected by default in Scheduler entry create/edit page when this "
            "Project is selected"
        ),
        "default_operator": gettext(
            "Default operator to be pre-filled in Scheduler entry create/edit page."
        ),
        "speedcode": gettext("Default billing speedcode. 64 characters or less."),
        "destinations": gettext(
            "Additional destination DICOM servers for DICOM studies associated with this Project."
        ),
    }

    column_labels = dict(
        GroupBaseView.column_labels,
        participant_description=gettext("Description (for participants)"),
    )

    column_list = (
        "name",
        "description",
        "principal",
        "group",
    )

    # noinspection PyTypeChecker
    form_widget_args = dict(
        description={"style": "width: 100%"},
        study={"readonly": ""},
        group={"readonly": ""},
    )

    column_formatters = {"group": _group_formatter}

    def get_edit_form(self):
        form = super().get_edit_form()
        disable_field(form.principal)
        return form


class ProjectUserView(UserAccess, ProjectView):
    """
    Customized User view for projects
    """

    can_create = False
    can_delete = False

    # noinspection PyTypeChecker
    form_widget_args = dict(
        name={"readonly": ""},
        description={"style": "width: 100%"},
        study={"readonly": ""},
        group={"readonly": ""},
    )

    def get_query(self):
        """
        Restrict to :class:`Project` belonging to :class:`Group` of which
        the user is a member
        """
        principals = Principal.accessible_to(current_user).with_entities(Principal.id)
        return Project.query.filter(Project.principal_id.in_(principals)).filter(
            Project.active
        )

    def get_count_query(self):
        """
        Count the items in the query
        """
        return self.get_query().with_entities(func.count(Project.id))


class ProjectDatacareView(DatacareAdminAccess, ProjectView):
    """
    Customize view for projects for administrators
    """

    def __init__(self, *args, **kwargs):
        self._add_inline_models((InlineAttribute(Attribute),), append=False)
        super().__init__(*args, **kwargs)

    column_default_sort = "name"
    column_list = (
        "id",
        "name",
        "description",
        "principal",
        "group",
        "default_operator",
        "default_rate",
        "speedcode",
        "active",
    )
    form_columns = (
        "name",
        "description",
        "participant_description",
        "principal",
        "default_operator",
        "default_rate",
        "speedcode",
        "attributes",
        "destinations",
        "active",
    ) + GroupBaseView.form_columns


class AttributeView(AdminAccess, MyModelView):
    """
    Customized view for attributes
    """

    column_searchable_list = (
        Attribute.value,
        Tag.name,
        Project.name,
    )
    column_filters = (
        Attribute.value,
        Tag.name,
        Project.name,
        "project.principal.name",
    )
    form_columns = (
        "tag",
        "value",
        "project",
    )
    column_descriptions = dict(
        tag=gettext("DICOM Tag to match"),
        value=gettext("Tag must match value"),
    )
    form_widget_args = {
        "tag": {"style": "width: 100%"},
        "value": {"style": "width: 100%"},
    }


class TagView(AdminAccess, MyModelView):
    """
    Customized view for attributes
    """

    column_default_sort = "tagid"
    column_searchable_list = (
        Tag.name,
        Tag.tagid,
    )
    column_filters = (
        Tag.name,
        Tag.tagid,
    )
    form_columns = (
        "tagid",
        "name",
    )
    column_list = ("tagid", "name")
    form_widget_args = {
        "tagid": {"style": "width: 100%"},
        "name": {"style": "width: 100%"},
    }


# noinspection PyAbstractClass
class MyModelConverter(AdminModelConverter):
    @converts("sqlalchemy_utils.types.encrypted.encrypted_type.StringEncryptedType")
    def convert_encrypted(self, field_args, **extra):
        self._string_common(field_args=field_args, **extra)
        field = PasswordField(**field_args)
        # noinspection PyUnresolvedReferences
        field.field_class.widget.hide_value = False
        return field


class ServerView(MyModelView):
    """
    Customized view for attributes
    """

    column_default_sort = "aet"
    column_searchable_list = (
        Server.aet,
        Server.host,
        Server.description,
    )
    column_filters = (
        Server.aet,
        Server.host,
        Server.description,
        Server.port,
    )
    form_columns = (
        "aet",
        "host",
        "port",
        "description",
        "cipher",
        "pat_id_issuer",
        "acc_no_issuer",
        "user_id",
        "passwd",
        "station_name",
        "institution",
        "department",
        "installed",
        "user",
    )
    column_list = (
        "aet",
        "host",
        "port",
        "description",
        "user",
    )
    model_form_converter = MyModelConverter
    form_args = {
        "user": {
            "query_factory": lambda: User.query.order_by(User.username).options(
                db.load_only(User.id, User.username)
            )
        },
    }
    column_labels = {
        "passwd": gettext("Password"),
    }

    def handle_view_exception(self, exc):
        if isinstance(exc, ValueError):
            flash(gettext("Value error. %(message)s", message=str(exc)), "error")
            return True

        return super(ServerView, self).handle_view_exception(exc)

    def on_model_change(self, form, model, is_created):
        host = model.host
        port = model.port

        result = 0
        try:
            if model.installed and host and port:
                server = socket.gethostbyname(host)
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.settimeout(1)
                result = sock.connect_ex((server, port))
        except (socket.error, socket.herror, socket.gaierror, socket.timeout):
            result = 1

        if result != 0:
            raise ValueError(
                gettext("Cannot reach {host}:{port}").format(host=host, port=port)
            )

        return super(ServerView, self).on_model_change(form, model, is_created)


class ServerDatacareView(DatacareAdminAccess, ServerView):
    column_formatters = {}


class ServerUserView(UserAccess, ServerView):
    """
    Customized view for attributes
    """

    form_columns = (
        "aet",
        "host",
        "port",
        "description",
        "cipher",
        "pat_id_issuer",
        "acc_no_issuer",
        "station_name",
        "institution",
        "department",
        "installed",
    )

    def on_model_change(self, form, model, is_created):
        model.user = current_user
        return super(ServerUserView, self).on_model_change(form, model, is_created)

    def get_query(self):
        """
        Restrict to :class:`Server` belonging to :class:`User`
        """
        return (
            super(ServerUserView, self)
            .get_query()
            .filter(Server.owner_id == current_user.id)
        )

    def get_count_query(self):
        """
        Count the items in the query
        """
        return self.get_query().with_entities(func.count(Server.id))


class DestinationView(AdminAccess, MyModelView):
    """
    Customized view for attributes
    """

    column_default_sort = "server.aet"
    column_searchable_list = (Server.aet,)
    column_filters = (
        Server.aet,
        Destination.priority,
        Destination.delay,
        Project.name,
    )
    form_columns = ("server", "delay", "priority", "project")

    # Servers are restricted to prevent accidentally sending to MR consoles
    form_args = {
        "server": {
            "query_factory": lambda: Server.query.order_by(Server.aet)
            .filter(db.not_(Server.restricted))
            .options(db.load_only(Server.id, Server.aet))
        },
    }


class OperatorView(DatacareAdminAccess, MyModelView):
    """
    Customized view of Operators
    """

    column_default_sort = "code"
    column_searchable_list = ("code",)
    column_filters = (Operator.code, User.username)
    column_list = (
        "code",
        "name",
        "active",
        "user",
    )
    form_columns = (
        "code",
        "name",
        "active",
        "user",
        "projects",
    )
    column_descriptions = {
        "code": gettext('Operator code (Initials, e.g. "TS"). Displayed on Scheduler.'),
        "user": gettext("Operators user account."),
        "active": gettext("Check to enable selection in Scheduler."),
    }

    # noinspection PyUnusedLocal,PyMethodMayBeStatic
    def _user_formatter(self, context, model, name):
        return Markup(
            "<a href='%s'>%s</a>"
            % (url_for("dc_user.edit_view", id=model.user.id), model.user)
        )

    column_formatters = {"user": _user_formatter}


class UserBaseView(MyModelView):
    """
    Customized view of Users
    """

    column_default_sort = "username"
    column_list = (
        User.username,
        User.first_name,
        User.last_name,
        User.email,
        User.active,
    )
    column_searchable_list = ("username",)
    column_filters = (
        User.username,
        User.first_name,
        User.last_name,
        User.email,
        User.active,
    )

    form_columns = (
        User.username,
        User.first_name,
        User.last_name,
        User.email,
        User.active,
    )

    column_descriptions = {
        "username": gettext("Must be a valid existing user name"),
        "active": gettext("Check to enable Management App login."),
    }

    form_widget_args = {
        "first_name": {"readonly": ""},
        "last_name": {"readonly": ""},
        "email": {"readonly": ""},
    }

    def on_model_change(self, form, model, is_created):
        super().on_model_change(form, model, is_created)
        if is_created and not model.fs_uniquifier:
            model.fs_uniquifier = uuid.uuid4().hex

    def handle_view_exception(self, exc):
        if isinstance(exc, ValueError):
            flash(gettext("Value error. %(message)s", message=str(exc)), "error")
            return True

        return super(UserBaseView, self).handle_view_exception(exc)


class UserView(DatacareAccess, UserBaseView):
    pass


class UserAdminView(AdminAccess, UserBaseView):
    column_list = (
        User.username,
        User.first_name,
        User.last_name,
        User.email,
        User.active,
    )
    column_labels = {
        "member_group": "Membership",
    }
    inline_models = (InlineUserMembership(UserMembership),)

    column_filters = member_filters(User.membership) + [
        x for x in UserBaseView.column_filters
    ]

    form_columns = (
        User.username,
        User.first_name,
        User.last_name,
        User.email,
        User.active,
        User.last_login_at,
        User.last_login_ip,
        User.current_login_at,
        User.current_login_ip,
        User.login_count,
    )

    form_widget_args = {
        "first_name": {"readonly": ""},
        "last_name": {"readonly": ""},
        "email": {"readonly": ""},
        "last_login_at": {"readonly": "", "disabled": True},
        "current_login_at": {"readonly": "", "disabled": True},
        "last_login_ip": {"readonly": ""},
        "current_login_ip": {"readonly": ""},
        "login_count": {"readonly": ""},
    }


class RateView(DatacareAdminAccess, MyModelView):
    column_default_sort = "name"
    column_searchable_list = (
        Rate.name,
        Rate.hourly,
        Rate.fixed,
    )
    column_filters = (
        Rate.name,
        Rate.hourly,
        Rate.fixed,
        Rate.active,
    )
    column_list = (
        "name",
        "hourly",
        "fixed",
        "active",
    )

    form_columns = (
        "name",
        "hourly",
        "fixed",
        "active",
        "projects",
    )

    column_descriptions = {
        "name": gettext(
            "Descriptive name (shows up in Scheduler when creating/editing entry)"
        ),
        "hourly": gettext("Hourly rate in dollars"),
        "fixed": gettext("Fixed rate in dollars"),
        "flat": gettext(
            "If checked, the amount is not multiplied by entry duration when calculating report totals"
        ),
        "active": gettext("Rate is displayed in scheduler"),
    }


class VariableView(AdminAccess, MyModelView):
    pass


class SubjectView(DatacareAdminAccess, MyModelView):
    details_modal = True
    can_view_details = True

    column_searchable_list = (
        "given_name",
        "surname",
        "subjectid",
    )
    column_filters = (
        "given_name",
        "surname",
        "subjectid",
        "birthdate",
        "active",
    )
    column_labels = {"birthdate": gettext("Year of birth")}
    form_args = {
        "birthdate": {
            "format_": "%Y",
        }
    }
    form_widget_args = {
        "birthdate": {
            "data-role": "datepicker2",
        }
    }

    form_overrides = {"birthdate": DateField}

    # noinspection PyMethodMayBeStatic,PyUnusedLocal
    def _year_only_formatter(self, context, model, name):
        date = getattr(model, name, None)
        if date:
            date = date.strftime("%Y")
        return date

    column_formatters = {"birthdate": _year_only_formatter}


class DeviceView(DatacareAdminAccess, MyModelView):
    column_descriptions = {
        "clinical": gettext(
            "Only clinical devices can be associated with screening forms"
        )
    }


class Admin(AdminBase):
    def __init__(self, app, **kwargs):
        super().__init__(app=app, **kwargs)

        category_project = gettext("Projects")
        category_groups = gettext("Groups")

        self.add_view(
            ProjectUserView(
                Project,
                db.session,
                category=category_project,
            )
        )
        self.add_view(
            ProjectDatacareView(
                Project,
                db.session,
                category=category_project,
                endpoint="dc_project",
            )
        )
        self.add_view(
            PrincipalView(
                Principal,
                db.session,
                category=category_project,
            )
        )

        self.add_view(
            GroupUserView(
                Group,
                db.session,
                category=category_groups,
            )
        )
        self.add_view(
            GroupDatacareView(
                Group,
                db.session,
                category=category_groups,
                endpoint="dc_group",
            )
        )
        self.add_view(
            GroupAdminView(
                Group,
                db.session,
                category=category_groups,
                endpoint="admin_group",
            )
        )
        self.add_view(
            UserView(User, db.session, category=category_groups, endpoint="dc_user")
        )
        self.add_view(
            UserAdminView(
                User, db.session, category=category_groups, endpoint="admin_user"
            )
        )
        self.add_view(
            MembershipView(UserMembership, db.session, category=category_groups)
        )
        self.add_view(
            GroupMembershipView(GroupMembership, db.session, category=category_groups)
        )

        self.add_menu_item(
            DatacareAdminDividerMenu(name="Divider"), target_category=category_groups
        )

        # noinspection PyArgumentList
        self.add_view(
            NewUserView(
                name=gettext("New User"), endpoint="newuser", category=category_groups
            )
        )

        if app.extensions["arc"].enable:
            self.add_view(
                ServerUserView(
                    Server,
                    db.session,
                    category=gettext("DICOM"),
                )
            )
            self.add_view(
                ServerDatacareView(
                    Server, db.session, category=gettext("DICOM"), endpoint="dc_server"
                )
            )

            self.add_view(
                DestinationView(
                    Destination,
                    db.session,
                    category=gettext("DICOM"),
                )
            )

            self.add_view(
                AttributeView(
                    Attribute,
                    db.session,
                    category=gettext("DICOM"),
                )
            )
            self.add_view(
                TagView(
                    Tag,
                    db.session,
                    category=gettext("DICOM"),
                )
            )

            self.add_menu_item(
                DividerMenu(name="Divider"), target_category=gettext("DICOM")
            )
            self.add_links(
                LoggedInMenuLink(
                    name="Data Browser",
                    category=gettext("DICOM"),
                    url=app.config["INDEX_URL"],
                )
            )

        self.add_view(
            OperatorView(
                Operator,
                db.session,
                category=gettext("Operations"),
            )
        )
        self.add_view(
            RateView(
                Rate,
                db.session,
                category=gettext("Operations"),
            )
        )
        self.add_view(
            VariableView(
                Variable,
                db.session,
                category=gettext("Operations"),
            )
        )

        self.add_view(
            DeviceView(
                Device,
                db.session,
                category=gettext("Operations"),
            )
        )

        self.add_menu_item(
            OperationsDividerMenu(name="Divider"), target_category=gettext("Operations")
        )

        # noinspection PyArgumentList
        self.add_view(
            ReportView(
                name="Report",
                category=gettext("Operations"),
            )
        )

        self.add_view(
            ScreeningFormDatacareView(
                ScreeningForm,
                db.session,
                category=gettext("Screening"),
                endpoint="dc_screeningform",
            )
        )

        self.add_view(
            ScreeningFormCoordinatorView(
                ScreeningForm,
                db.session,
                category=gettext("Screening"),
                endpoint="screeningform",
            )
        )

        self.add_view(SubjectView(Subject, db.session, category=gettext("Screening")))

        self.add_menu_item(
            CoordinatorDividerMenu(name="Divider"), target_category=gettext("Screening")
        )

        # noinspection PyArgumentList
        self.add_view(
            CoordinatorSendView(
                name=gettext("Send Form"),
                endpoint="screening_send",
                category=gettext("Screening"),
            )
        )
