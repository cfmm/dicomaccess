import ldap3
from os import linesep

# This is OpenLDAP specific location of schema
raw_schema = """
{
    "type": "SchemaInfo",
    "schema_entry": "cn=Subschema",
    "raw": {
        "cn": [
            "mgmt"
        ],
        "entryDN": [
            "cn=mgmt,cn=schema,cn=config"
        ],
        "objectClass": [
            "olcSchemaConfig"
        ],
        "attributeTypes": [
            "( 1.2.826.0.1.3680043.9.5356.2.2.1.1 NAME 'mgmtId' DESC 'Record primary key in corresponding MGMT database' EQUALITY integerMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 )",
            "( 1.2.826.0.1.3680043.9.5356.2.2.1.2 NAME 'mgmtModel' DESC 'Model type for MGMT database record' EQUALITY caseExactMatch SUBSTR caseExactSubstringsMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )"
        ],
        "objectClasses": [
            "( 1.2.826.0.1.3680043.9.5356.2.2.2.1 NAME 'mgmtRecord' DESC 'Object generated from MGMT database record' SUP top AUXILIARY MUST ( mgmtId $ mgmtModel ) )"
        ]
    }
}
"""


def make_ldif():
    info = ldap3.SchemaInfo.from_json(raw_schema)
    entry = dict(type="searchResEntry")
    entry["dn"] = info.other["entryDN"][0]
    entry["raw_attributes"] = dict(
        objectClass=info.other["objectClass"],
        cn=info.other["cn"],
        olcAttributeTypes=[
            info.attribute_types[x].raw_definition for x in info.attribute_types
        ],
        olcObjectClasses=[
            info.object_classes[x].raw_definition for x in info.object_classes
        ],
    )

    return linesep.join(
        ldap3.protocol.rfc2849.operation_to_ldif(
            "searchResponse", [entry], None, sort_order=None
        )
    )


def schema_contains(subset, master):
    # Compare the schema. This check is LDAP server independent.
    add = False
    replace = False
    for name in subset.attribute_types:
        if name not in master.attribute_types:
            add = True
        elif (
            master.attribute_types[name].raw_definition
            != subset.attribute_types[name].raw_definition
        ):
            replace = True
            break

    for name in subset.object_classes:
        if name not in master.object_classes:
            add = True
        elif (
            master.object_classes[name].raw_definition
            != subset.object_classes[name].raw_definition
        ):
            replace = True
            break

    return add, replace


def update_schema(server, user, password):
    c = ldap3.Connection(server, user, password)

    # Unable to bind, so skip updating
    try:
        c.bind()
    except ldap3.core.exceptions.LDAPSocketOpenError:
        return

    # Unable to obtain schema, so skip updating
    if not c.bound or c.server.schema is None:
        return

    mgmt = ldap3.SchemaInfo.from_json(raw_schema)

    add, replace = schema_contains(mgmt, c.server.schema)

    # Updating the schema is LDAP server dependent and it is assumed to be OpenLDAP
    if replace:
        # Get the parent dn of the schema
        cn = mgmt.other["cn"][0]
        dn = "".join(
            x[0] + "=" + x[1] + x[2]
            for x in ldap3.utils.dn.parse_dn(mgmt.other["entryDN"][0])[1:]
        )

        # The numeric prefix for the schema is unknown and needs to be determined by a search
        if c.search(
            dn,
            f"(&(objectClass={')(objectClass='.join(mgmt.other['objectClass'])})(cn=*}}{cn}))",
            search_scope=ldap3.SUBTREE,
        ):
            dn = c.response[0]["dn"]
        else:
            raise RuntimeError("Unable to find mgmt schema DN")

        if not c.modify(
            dn,
            changes={
                "olcAttributeTypes": (
                    ldap3.MODIFY_REPLACE,
                    [
                        mgmt.attribute_types[x].raw_definition
                        for x in mgmt.attribute_types
                    ],
                ),
                "olcObjectClasses": (
                    ldap3.MODIFY_REPLACE,
                    [
                        mgmt.object_classes[x].raw_definition
                        for x in mgmt.object_classes
                    ],
                ),
            },
        ):
            raise RuntimeError("Failed to update mgmt schema")
    elif add:
        if not c.add(
            mgmt.other["entryDN"],
            object_class=mgmt.other["objectClass"],
            attributes=dict(
                cn=mgmt.other["cn"],
                olcAttributeTypes=[
                    mgmt.attribute_types[x].raw_definition for x in mgmt.attribute_types
                ],
                olcObjectClasses=[
                    mgmt.object_classes[x].raw_definition for x in mgmt.object_classes
                ],
            ),
        ):
            raise RuntimeError("Failed to add schema to Arc")


if __name__ == "__main__":
    update_schema("ldap://ldap.mgmt", "cn=admin,cn=config", "secret")
