"""
Created on March 13, 2020

Copyright 2020, The University of Western Ontario

.. codeauthor:: mklassen
"""

import typing

from flask import render_template

from .models.credentials import User, Group
from .models.dicom import Principal
from .proxy import current_ldap


class UserNotFound(Exception):
    pass


class MissingAttributes(Exception):
    pass


class Mail:
    def __init__(self, item: typing.Union[str, User]):
        if isinstance(item, User):
            if not item.email or not item.last_name:
                raise MissingAttributes

            self.mail = item.email
            self.name = item.last_name
            if item.first_name:
                self.name = f"{item.first_name} {item.last_name}"
            return
        else:
            dn = item
        attributes = ["mail", "displayName"]
        data = current_ldap.lookup_by_dn(dn, attributes=attributes)

        if data:
            fields = data[0]["attributes"]
            if all(x in fields for x in attributes):
                self.name = fields["displayName"]
                mail = fields["mail"]
                if isinstance(mail, str):
                    mail = [mail]
                self.mail = mail
                return
            else:
                raise MissingAttributes

        raise UserNotFound


class UserReport:
    @staticmethod
    def render(users):
        return render_template("mrbs/mail_list.xml", users=users)

    @staticmethod
    def add_user(mail_list, user):
        if user.username not in mail_list:
            try:
                mail_list[user.username] = Mail(user)
            except (MissingAttributes, UserNotFound):
                pass

    @staticmethod
    def group_list(mail_list, group):
        # Skip inactive project groups
        if group.is_project and group.project.active:
            return

        # Skip groups already processed
        if group in mail_list:
            return

        mail_list[group] = None
        for user in group.users:
            UserReport.add_user(mail_list, user)

        for member in group.groups:
            UserReport.group_list(mail_list, member)

        if group.is_principal:
            # Only add principals with active projects
            if group.principal.active:
                UserReport.add_user(mail_list, group.principal.user)

    def principals(self):
        mail_list = dict()

        # Query all principal users
        for principal in Principal.query.all():
            if principal.active:
                UserReport.add_user(mail_list, principal.user)

        return self.render(mail_list)

    def maintainers(self):
        mail_list = dict()

        # Query all members of principal groups
        for group in Group.query.join(Principal).all():
            UserReport.group_list(mail_list, group)

        return self.render(mail_list)

    def all_users(self):
        mail_list = dict()

        # Query all members of all groups
        for group in Group.query.all():
            if group.is_dicom:
                UserReport.group_list(mail_list, group)

        return self.render(mail_list)
