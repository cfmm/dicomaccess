import os
import re

import pydicom
from pynetdicom import AE
import psycopg
import pytest
from array import array
from mgmt.models.dicom import Project
from mgmt.models.credentials import Group


@pytest.fixture(scope="session")
def dataset():
    ds = pydicom.Dataset()
    ds.PatientName = "Patient^Test"
    ds.PatientID = "TestPatientID"
    ds.ReferencedSOPInstanceUID = "1.2.826.0.1.3680043.8.498.123"
    ds.ReferencedSOPClassUID = "1.2.826.0.1.3680043.8.498.123"
    ds.PatientBirthDate = ""
    ds.PatientBirthTime = ""
    ds.PatientSex = ""
    ds.StudyDate = ""
    ds.StudyTime = ""
    ds.AccessionNumber = ""
    ds.ReferringPhysicianName = ""
    ds.StudyID = "TestStudyID"
    ds.Modality = "MR"
    ds.SeriesNumber = 1
    ds.FrameOfReferenceUID = "1.2.826.0.1.3680043.8.498.123"
    ds.PositionReferenceIndicator = ""
    ds.Manufacturer = "TestManufacturer"
    ds.InstanceNumber = 1
    ds.ImagePositionPatient = [0.0, 0.0, 0.0]
    ds.ImageOrientationPatient = [1, 0, 0, 0, 1, 0]
    ds.PixelSpacing = [1, 1]
    ds.SamplesPerPixel = 1
    ds.SliceThickness = 1
    ds.PhotometricInterpretation = "MONOCHROME2"
    ds.Rows = 32
    ds.Columns = 32
    ds.BitsAllocated = 8
    ds.BitsStored = 8
    ds.HighBit = 7
    ds.PixelRepresentation = 0
    ds.ImageType = "OTHER"
    ds.ScanningSequence = "GR"
    ds.SequenceVariant = "NONE"
    ds.ScanOptions = ""
    ds.RepetitionTime = 1
    ds.EchoTrainLength = 1
    ds.EchoTime = 0.05
    ds.StudyInstanceUID = "1.2.826.0.1.3680043.8.498.1"
    ds.SeriesInstanceUID = "1.2.826.0.1.3680043.8.498.1.1"
    ds.SOPClassUID = "1.2.840.10008.5.1.4.1.1.4"
    ds.SOPInstanceUID = "1.2.826.0.1.3680043.8.498.1.1.1"

    image = array("b")
    for _ in range(0, 32 * 32):
        image.append(0)

    ds.PixelData = image.tobytes()
    ds.file_meta = pydicom.dataset.FileMetaDataset()
    ds.file_meta.TransferSyntaxUID = pydicom.uid.ExplicitVRLittleEndian

    yield ds


@pytest.fixture(scope="function")
def scu(arc_rest, arc):
    arc_rest.as_user("admin", "secret")

    class SCU(object):
        def __init__(self):
            calling_aetitle = "CLIENT"
            arc_rest.add_device(calling_aetitle, port=11112)
            self.ae = AE(calling_aetitle)
            self.ae.add_requested_context("1.2.840.10008.5.1.4.1.1.4")
            self.host = arc.config.archive_host
            self.port = arc.config.port + 2
            self.called_aetitle = arc.config.aetitle
            self.upload_iuids = list()

        def upload(self, dataset):
            assoc = self.ae.associate(
                self.host, self.port, ae_title=self.called_aetitle
            )
            assert assoc.is_established, "Unable to make association with DICOM server"
            assoc.send_c_store(dataset)
            assoc.release()
            self.upload_iuids.append(dataset.StudyInstanceUID)

    scu = SCU()
    yield scu
    for study_iuid in scu.upload_iuids:
        assert arc_rest.remove_study(study_iuid), f"Failed to remove study {study_iuid}"


def test_check_matching(dataset, configured, arc, scu):
    with configured.app_context():
        default_group = Group.query.filter(Group.name == "Datacare").first()
        device = f"dicomDeviceName={arc.config.device},cn=Devices,cn=DICOM Configuration,{arc.config.base}"
        entity = "DefaultDatacare"
        arc.add(
            dn=f"cn={entity},{device}",
            object_class=["dcmStoreAccessControlIDRule"],
            attributes={
                "cn": entity,
                "dcmStoreAccessControlID": default_group.id,
                "dcmRulePriority": 0,
            },
        )

    with configured.app_context():
        project = Project.query.first()
        principal_group = project.principal.group
        project_group = project.group
        updater = configured.extensions["arc"]
        updater.sync()
        updater.reload()

    database = os.getenv("POSTGRES_DB")
    database_user = os.getenv("POSTGRES_USER")
    database_password = os.getenv("POSTGRES_PASSWORD")

    count = 0

    def upload(description, group: Group):
        nonlocal count
        count += 1

        dataset.StudyInstanceUID = f"1.2.826.0.1.3680043.8.498.{count}"
        dataset.SeriesInstanceUID = f"1.2.826.0.1.3680043.8.498.{count}.1"
        dataset.SOPInstanceUID = f"1.2.826.0.1.3680043.8.498.{count}.1.1"
        dataset.StudyDescription = description

        scu.upload(dataset)
        with psycopg.connect(
            dbname=database,
            user=database_user,
            password=database_password,
            host="db",
            port=5432,
        ) as connection:
            cursor = connection.cursor()

            cursor.execute(
                f"SELECT access_control_id, study_desc FROM study WHERE study_iuid = '{dataset.StudyInstanceUID}';"
            )

            # Fetch all rows from database
            records = cursor.fetchall()

        assert len(records) == 1, "Data not uploaded"
        access_control_id = records[0][0]
        assert access_control_id != "*", "All uploads should have an access_control_id"
        access_control_id = int(access_control_id)
        assert (
            access_control_id == group.id
        ), f"Wrong access control id for study description '{description}'"

    upload(project.study, project_group)
    upload(f"pre{project.study}", default_group)
    if re.compile(rf"{project.principal.name}[\^ ]{project.name}", re.IGNORECASE).match(
        project.study
    ):
        upload(f"{project.study}post", principal_group)
    else:
        upload(f"{project.study}post", default_group)

    upload(f"{project.principal.name} {project.name}", project_group)
    upload(f"{project.principal.name} {project.name}post", principal_group)
    upload(f"pre{project.principal.name} {project.name}post", default_group)
    upload(f"{project.principal.name}^{project.name}", project_group)
    upload(f"{project.principal.name}^{project.name}post", principal_group)
    upload(f"pre{project.principal.name}^{project.name}post", default_group)
    upload(f"{project.principal.name}", principal_group)
    upload(f"{project.principal.name}^", principal_group)
    upload(f"{project.principal.name}^post", principal_group)
    upload(f"{project.principal.name} post", principal_group)
    upload(f"pre{project.principal.name} ", default_group)
    upload(f"pre{project.principal.name}^", default_group)
    upload(f"{project.principal.name} ", principal_group)
