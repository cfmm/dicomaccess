import datetime
import pytest
from uuid import uuid4
from requests import Response
from mgmt.models.dicom import Principal, Project, Server, Destination
from mgmt.models.credentials import (
    User,
    Group,
    UserMembership,
    GroupMembership,
)
from mgmt.models import db
from .conftest import reset_mock_ldap
from cfmm_keycloak.configure import env_config

"""
Create:
    Group
    Project
    Principal
    Server
    Destination
    UserMembership
        with expires
    GroupMembership
        with expires
Delete:
    Project
    Principal
    Server
    Destination
    Group
    UserMembership
    GroupMembership
Change:
    Group
        id
        name
        description
        project
        principal
        user_member
        group_member
    UserMembership
        level
        expires
        group_id
        member_id
    GroupMembership
        expires
        group_id
        member_id
    Principal
        id
        name
        user_id
        group_id
    Project
        id
        name
        study
        attributes
        principal_id
        group_id
        destinations
    Destination
        delay
        server_id
        project_id
    Attribute
    Server
        aet
        host
        port
        cipher
        description
        installed
        destinations
"""


@pytest.fixture(scope="module")
def mock_config(module_mocker):
    data = {
        "ldapUrl": "ldap://ldap.fake.server:389",
        "userDN": env_config.arc.user,
        "userPassword": env_config.arc.password,
        "deviceDN": f"dicomDeviceName={env_config.arc.device},cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        "devicesDN": f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        "configurationDN": f"cn=DICOM Configuration,{env_config.arc.base}",
        "aetsRegistryDN": f"cn=Unique AE Titles Registry,cn=DICOM Configuration,{env_config.arc.base}",
    }
    module_mocker.patch(
        "mgmt.arc.ArcConfig._get_config",
        return_value=data,
    )

    token = {"access_token": str(uuid4())}

    module_mocker.patch(
        "mgmt.arc.ArcConfig.token",
        new_callable=module_mocker.PropertyMock,
        return_value=token,
    )

    return {
        "token": token,
    }

    r = Response()
    r.status_code = 204
    module_mocker.patch("mgmt.arc.ArcConfig.reload", return_value=r)


def test_create_principal(configured, mock_ldap, mock_config):
    with configured.app_context():
        try:
            principal = Principal(
                name="PrincipalTEST", user=User.query.order_by(User.id).first()
            )

            db.session.add(principal)
            db.session.commit()
            principal_id = principal.id
            obj_id = principal.group.id
            identifier = principal.group.identifier
        finally:
            db.session.delete(principal)
            db.session.commit()

    assert mock_ldap["add"].call_count == 2

    assert (
        mock_ldap["add"].mock_calls[0].args[0]
        == f"cn=PrincipalTEST,ou={configured.config['USERMGMT']['dicom_ou']},{configured.extensions['ldap'].group_base}"
    )
    assert mock_ldap["add"].mock_calls[0].kwargs == {
        "object_class": ["group", "top"],
        "attributes": {
            "groupType": [b"-2147483644"],
            "sAMAccountName": [identifier.encode("ascii")],
            "gidNumber": [str(obj_id).encode("ascii")],
            "displayName": [b"PrincipalTEST"],
            "cn": [b"PrincipalTEST"],
        },
    }

    assert (
        mock_ldap["add"].mock_calls[1].args[0]
        == f"cn=mgmtPrincipal{principal_id},dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[1].kwargs == {
        "attributes": {
            "cn": [f"mgmtPrincipal{principal_id}"],
            "mgmtId": [principal_id],
            "mgmtModel": "principal",
            "objectClass": ["dcmStoreAccessControlIDRule", "mgmtRecord"],
            "dcmStoreAccessControlID": str(obj_id),
            "dcmRulePriority": 5,
            "dcmProperty": ["StudyDescription=(?i)^\\QPrincipalTEST\\E([\\^ ].*$|$)"],
        }
    }


def test_create_project(configured, mock_ldap, mock_config):
    with configured.app_context():
        try:
            project = Project(
                name="ProjectTEST",
                principal=Principal.query.order_by(Principal.id).first(),
            )

            db.session.add(project)
            db.session.commit()
            identifier = project.group.identifier
            gid = project.group.id
            project_id = project.id
        finally:
            db.session.delete(project)
            db.session.commit()

    assert mock_ldap["add"].call_count == 2

    assert (
        mock_ldap["add"].mock_calls[0].args[0]
        == f"cn=Principal1-ProjectTEST,ou={configured.config['USERMGMT']['dicom_ou']},"
        f"{configured.extensions['ldap'].group_base}"
    )
    assert mock_ldap["add"].mock_calls[0].kwargs == {
        "object_class": ["group", "top"],
        "attributes": {
            "groupType": [b"-2147483644"],
            "sAMAccountName": [str(identifier).encode("utf-8")],
            "gidNumber": [str(gid).encode("utf-8")],
            "displayName": [b"Principal1-ProjectTEST"],
            "cn": [b"Principal1-ProjectTEST"],
            "description": [b"Group for Principal1^ProjectTEST"],
        },
    }

    assert (
        mock_ldap["add"].mock_calls[1].args[0]
        == f"cn=mgmtProject{project_id},dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[1].kwargs == {
        "attributes": {
            "cn": [f"mgmtProject{project_id}"],
            "mgmtId": [project_id],
            "mgmtModel": "project",
            "objectClass": ["dcmStoreAccessControlIDRule", "mgmtRecord"],
            "dcmStoreAccessControlID": str(gid),
            "dcmRulePriority": 10,
            "dcmProperty": [
                "StudyDescription=(?i)^(\\QPrincipal1^ProjectTEST\\E|\\QPrincipal1\\E[\\^ ]\\QProjectTEST\\E)$"
            ],
        }
    }


def test_create_server(configured, mock_ldap, mock_config):
    with configured.app_context():
        try:
            server = Server(
                aet="MYAETITLE",
                host="localhost",
                installed=True,
                user=User.query.first(),
            )
            db.session.add(server)
            db.session.commit()
        finally:
            db.session.delete(server)
            db.session.commit()

    assert mock_ldap["add"].call_count == 5

    assert [x.args[0] for x in mock_ldap["add"].mock_calls[:5]] == [
        f"dicomDeviceName=MYAETITLE@localhost,cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        f"dcmExporterID=MYAETITLE@localhost,dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        f"dicomAETitle=MYAETITLE,cn=Unique AE Titles Registry,cn=DICOM Configuration,{env_config.arc.base}",
        f"cn=dicom,dicomDeviceName=MYAETITLE@localhost,cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        f"dicomAETitle=MYAETITLE,dicomDeviceName=MYAETITLE@localhost,"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
    ]

    # Actual content is tested in test_arc


def test_create_destination(configured, mock_ldap, mock_config):
    server = destination = None
    with configured.app_context():
        try:
            server = Server(
                aet="MYAETITLE",
                host="localhost",
                installed=True,
                user=User.query.first(),
            )
            db.session.add(server)
            db.session.commit()

            reset_mock_ldap(mock_ldap)

            server = Server.query.filter(Server.aet == "MYAETITLE").first()
            destination = Destination(server=server)
            db.session.add(destination)
            db.session.commit()
            destination_id = destination.id
        finally:
            if destination:
                db.session.delete(destination)
            if server:
                db.session.delete(server)
            db.session.commit()

    assert mock_ldap["add"].call_count == 1

    assert [x.args[0] for x in mock_ldap["add"].mock_calls[:1]] == [
        f"cn=mgmtDestination{destination_id},dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    ]

    # Actual content is tested in test_arc


def test_create_group(configured, mock_ldap, mock_config):
    with configured.app_context():
        try:
            group = Group(name="GroupTEST")
            db.session.add(group)
            db.session.commit()
            gid = group.id
            identifier = group.identifier
        finally:
            db.session.delete(group)
            db.session.commit()

    assert mock_ldap["add"].call_count == 1

    assert [x.args[0] for x in mock_ldap["add"].mock_calls[:1]] == [
        f"cn=GroupTEST,ou={configured.config['USERMGMT']['group_ou']},{configured.extensions['ldap'].group_base}"
    ]

    assert mock_ldap["add"].mock_calls[0].kwargs == {
        "object_class": ["group", "top"],
        "attributes": {
            "groupType": [b"-2147483644"],
            "sAMAccountName": [str(identifier).encode("utf-8")],
            "gidNumber": [str(gid).encode("utf-8")],
            "displayName": [b"GroupTEST"],
            "cn": [b"GroupTEST"],
        },
    }


def test_create_usermembership(configured, mock_ldap, mocker, mock_config):
    task = mocker.patch("mgmt.tasks.update_expired.apply_async", return_value=None)

    with configured.app_context():
        try:
            membership = UserMembership(
                member_id=1,
                group_id=1,
                expires=datetime.datetime.now(datetime.timezone.utc)
                + datetime.timedelta(minutes=1),
            )
            db.session.add(membership)
            db.session.commit()
        finally:
            db.session.delete(membership)
            db.session.commit()

    assert task.call_count == 1


def test_create_usermembership2(configured, mock_ldap, mocker, mock_config):
    task = mocker.patch("mgmt.tasks.update_expired.apply_async", return_value=None)

    with configured.app_context():
        member_id = 1
        group_id = 1
        membership = UserMembership.query.filter(
            UserMembership.member_id == member_id, UserMembership.group_id == group_id
        ).first()
        if membership:
            db.session.delete(membership)
            db.session.commit()

    reset_mock_ldap(mock_ldap)

    with configured.app_context():
        try:
            membership = UserMembership(member_id=member_id, group_id=group_id)
            db.session.add(membership)
            db.session.commit()
        finally:
            db.session.delete(membership)
            db.session.commit()

    assert task.call_count == 0


def test_create_groupmembership(configured, mock_ldap, mocker, mock_config):
    task = mocker.patch("mgmt.tasks.update_expired.apply_async", return_value=None)

    with configured.app_context():
        try:
            membership = GroupMembership(
                member_id=1,
                group_id=2,
                expires=datetime.datetime.now(datetime.timezone.utc)
                + datetime.timedelta(minutes=1),
            )
            db.session.add(membership)
            db.session.commit()
        finally:
            db.session.delete(membership)
            db.session.commit()

    assert task.call_count == 1


def test_create_groupmembership2(configured, mock_ldap, mocker, mock_config):
    task = mocker.patch("mgmt.tasks.update_expired.apply_async", return_value=None)

    with configured.app_context():
        member_id = 1
        group_id = 2

        membership = GroupMembership.query.filter(
            GroupMembership.member_id == member_id, GroupMembership.group_id == group_id
        ).first()
        if membership:
            db.session.delete(membership)
            db.session.commit()

    reset_mock_ldap(mock_ldap)

    with configured.test_request_context():
        try:
            membership = GroupMembership(member_id=member_id, group_id=group_id)
            db.session.add(membership)
            db.session.commit()
        finally:
            db.session.delete(membership)
            db.session.commit()

    assert task.call_count == 0
