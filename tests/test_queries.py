import datetime
from unittest.mock import PropertyMock

from mgmt.models.credentials import (
    Group,
    GroupMembership,
    UserMembership,
    MembershipType,
)
from mgmt.models.dicom import Principal, Project
from mgmt.models.screening import ScreeningForm, ScreeningState
from mgmt.screening.admin import ScreeningFormCoordinatorView
from flask_security.core import AnonymousUser


def compare_equal(result, reference):
    result = {i for i in result}
    assert not result.symmetric_difference(
        set(reference)
    ), f"{result} not equal to {reference}"


def test_user_member_of_query(query_env):
    def member_of_equal(user_, reference, level=MembershipType.full):
        compare_equal(
            {i[0] for i in UserMembership.member_of(user_, level).all()},
            [r.group_id for r in reference],
        )

    q = query_env

    # Owner of principal and member of another principal's group
    member_of_equal(q.users[0], q.principals)

    # test adding member to principal group
    q.add_member(q.users[1], q.principals[0])
    member_of_equal(q.users[1], [q.principals[0]])

    # test adding member to project group
    q.add_member(q.users[2], q.projects[0][0])
    q.add_member(q.users[2], q.projects[0][2])

    # membership of users is not propagated via GroupMembership
    q.db.session.add(
        GroupMembership(
            member_id=q.projects[0][1].group_id, group_id=q.projects[0][0].group_id
        )
    )
    member_of_equal(q.users[2], [q.projects[0][0], q.projects[0][2]])

    # membership level
    q.add_member(q.users[2], q.projects[0][1], level=MembershipType.coordinator)
    member_of_equal(q.users[2], [q.projects[0][0], q.projects[0][2]])
    member_of_equal(
        q.users[2],
        [q.projects[0][0], q.projects[0][1], q.projects[0][2]],
        level=MembershipType.coordinator,
    )

    # if expiry is in the future, the membership included, if it's in the past it's not
    q.add_member(
        q.users[3],
        q.projects[0][0],
        expires=datetime.datetime.now(datetime.timezone.utc)
        + datetime.timedelta(minutes=1),
    )
    q.add_member(
        q.users[3],
        q.projects[0][1],
        expires=datetime.datetime.now(datetime.timezone.utc)
        - datetime.timedelta(minutes=1),
    )
    member_of_equal(
        q.users[3],
        [
            q.projects[0][0],
        ],
    )


def test_principal_accessible_to(query_env):
    def accessible_to_equal(user_, reference, level=MembershipType.full):
        compare_equal(
            {i for i in Principal.accessible_to(user_, level=level).all()}, reference
        )

    q = query_env

    # user 0 should have access to all principals
    accessible_to_equal(q.users[0], q.principals)

    # user 2 should have access to none, regardless of project membership
    accessible_to_equal(q.users[2], [])
    q.add_member(q.users[2], q.projects[1][1])
    accessible_to_equal(q.users[2], [])

    # membership of user 2 is not propagated via GroupMembership
    q.db.session.add(
        GroupMembership(
            member_id=q.projects[1][1].group_id, group_id=q.principals[0].group_id
        )
    )
    accessible_to_equal(q.users[2], [])


def test_project_accessible_to(query_env, mocker):
    def accessible_to_equal(user_, reference, level=MembershipType.full, active=True):
        compare_equal(
            {i for i in Project.accessible_to(user_, level=level, active=active).all()},
            reference,
        )

    q = query_env

    # user 0 should have access to all projects since they are member of all principals
    accessible_to_equal(q.users[0], q.projects_list)

    accessible_to_equal(q.users[1], [])
    q.add_member(q.users[1], q.principals[2])
    q.add_member(q.users[1], q.projects[0][1])
    q.add_member(q.users[1], q.projects[1][1])
    accessible_to_equal(
        q.users[1], q.projects[2] + [q.projects[0][1], q.projects[1][1]]
    )

    # membership of user is not propagated via GroupMembership
    q.db.session.add(
        GroupMembership(
            member_id=q.projects[0][1].group_id, group_id=q.principals[0].group_id
        )
    )
    accessible_to_equal(
        q.users[1], q.projects[2] + [q.projects[0][1], q.projects[1][1]]
    )

    # test membership level propagation
    q.add_member(q.users[1], q.projects[0][2], level=MembershipType.coordinator)
    accessible_to_equal(
        q.users[1], q.projects[2] + [q.projects[0][1], q.projects[1][1]]
    )
    accessible_to_equal(
        q.users[1],
        q.projects[2] + [q.projects[0][1], q.projects[1][1], q.projects[0][2]],
        level=MembershipType.coordinator,
    )

    accessible_to_equal(AnonymousUser(), [])

    # unauthenticated users see nothing
    mocker.patch(
        "mgmt.models.credentials.User.is_authenticated",
        new_callable=PropertyMock,
        return_value=False,
    )
    accessible_to_equal(q.users[1], [])
    mocker.patch(
        "mgmt.models.credentials.User.is_authenticated",
        new_callable=PropertyMock,
        return_value=True,
    )

    # datacare users see everything
    mocker.patch(
        "mgmt.models.credentials.User.is_datacare",
        new_callable=PropertyMock,
        return_value=True,
    )
    accessible_to_equal(q.users[1], Project.query.all())
    mocker.patch(
        "mgmt.models.credentials.User.is_datacare",
        new_callable=PropertyMock,
        return_value=False,
    )

    # test active
    q.projects[0][1].active = False
    accessible_to_equal(q.users[1], q.projects[2] + [q.projects[1][1]])
    accessible_to_equal(
        q.users[1], q.projects[2] + [q.projects[0][1], q.projects[1][1]], active=False
    )
    q.projects[0][1].active = True


def test_screening_forms_access(query_env, mocker):
    q = query_env

    def screening_form(user, project):
        f = ScreeningForm(
            project_id=project.id,
            create_user_id=user.id,
            create_time=datetime.datetime.now(),
            state=ScreeningState.created,
            version="20210916_01",
        )
        q.db.session.add(f)
        return f

    def query_equal(user, reference):
        mocker.patch("flask_login.utils._get_user", return_value=user)
        compare_equal({i for i in coordinator_view.get_query().all()}, reference)

    screening_forms = None
    try:
        # create one screening form per project, in an array with same dimensions as the projects array
        screening_forms = [
            [
                screening_form(q.users[0], q.projects[i][j])
                for j in range(len(q.projects[0]))
            ]
            for i in range(len(q.principals))
        ]

        coordinator_view = ScreeningFormCoordinatorView(
            ScreeningForm,
            q.db.session,
        )

        # user 0 can see all screening forms (creator of all principals)
        query_equal(q.users[0], [f for principal in screening_forms for f in principal])

        # principal group membership
        q.add_member(q.users[1], q.principals[2])
        query_equal(q.users[1], screening_forms[2])

        # project group membership (full)
        q.add_member(q.users[1], q.projects[0][1])
        query_equal(q.users[1], screening_forms[2] + [screening_forms[0][1]])

        # project group membership (coordinator)
        q.add_member(q.users[1], q.projects[0][2], level=MembershipType.coordinator)
        query_equal(
            q.users[1],
            screening_forms[2] + [screening_forms[0][1], screening_forms[0][2]],
        )

        query_equal(AnonymousUser(), [])

        # unauthenticated users see nothing
        mocker.patch(
            "mgmt.models.credentials.User.is_authenticated",
            new_callable=PropertyMock,
            return_value=False,
        )
        query_equal(q.users[1], [])
        mocker.patch(
            "mgmt.models.credentials.User.is_authenticated",
            new_callable=PropertyMock,
            return_value=True,
        )

        # datacare users see everything
        mocker.patch(
            "mgmt.models.credentials.User.is_datacare",
            new_callable=PropertyMock,
            return_value=True,
        )
        query_equal(q.users[1], [f for principal in screening_forms for f in principal])
        mocker.patch(
            "mgmt.models.credentials.User.is_datacare",
            new_callable=PropertyMock,
            return_value=False,
        )
    finally:
        if screening_forms:
            for sf in (f for principal in screening_forms for f in principal):
                q.db.session.delete(sf)
            q.db.session.commit()


def test_group_member_of_query(configured, mocker):
    mocker.patch("mgmt.events.sync_transaction", return_value=None)
    mocker.patch("mgmt.events.after_transaction_end", return_value=None)

    db = configured.extensions["sqlalchemy"]

    with configured.app_context():
        group1 = Group(name="Group1")
        group2 = Group(name="Group2")
        group3 = Group(name="Group3")
        group4 = Group(name="Group4")
        db.session.add(group1)
        db.session.add(group2)
        db.session.add(group3)
        db.session.add(group4)
        db.session.commit()

        # Nested
        membership1 = GroupMembership(member_id=group1.id, group_id=group2.id)
        membership2 = GroupMembership(member_id=group2.id, group_id=group3.id)

        # Circular
        membership3 = GroupMembership(member_id=group2.id, group_id=group1.id)

        db.session.add(membership1)
        db.session.add(membership2)
        db.session.add(membership3)
        db.session.commit()

        groups = GroupMembership.member_of(
            Group.query.with_entities(Group.id).filter(Group.id == group1.id)
        )

        result = {x[0] for x in db.session.execute(groups)}
        expected = {group1.id, group2.id, group3.id}

        assert group4.id not in result, "Unexpected group4 in member of"
        assert result.issuperset(expected), "Expected groups are not in member of"
        assert expected == result, "Unexpected group(s) in member of"
