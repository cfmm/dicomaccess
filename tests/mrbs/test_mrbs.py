import base64
import datetime
import json
import subprocess
from types import SimpleNamespace

import AdvancedHTMLParser
import pytest
import time
import requests
import re

from mgmt.models.credentials import User, MembershipType, UserMembership, Group
from mgmt.models.dicom import Project, Operator, Principal
from ..helpers import register_client

from mgmt.mrbs.models import Area, Room, Entry, OAuth2Token, Rate


def create_entry(
    create_by=None,
    project_id=1,
    operator_id=1,
    rate_id=1,
    room_id=1,
    start=None,
    duration=3600,
    description="test",
    awaiting_approval=False,
):
    if not start:
        start = (
            datetime.datetime.now()
            .replace(hour=12, minute=0, second=0, microsecond=0)
            .timestamp()
        )

    return Entry(
        project_id=project_id,
        operator_id=operator_id,
        rate_id=rate_id,
        room_id=room_id,
        start_time=start,
        end_time=start + duration,
        description=description,
        create_by=create_by,
        status=2 if awaiting_approval else 0,
    )


def wait_for_service(url, name, attempts=100, sleep_time=1):
    for _ in range(attempts):
        try:
            return requests.head(url)
        except requests.exceptions.ConnectionError:
            time.sleep(sleep_time)
    raise RuntimeError(f"Timed out waiting for {name} service to start.")


@pytest.fixture(scope="session")
def network_app(configured):
    app_process = subprocess.Popen(["flask", "run", "--host=0.0.0.0", "--port=5000"])
    yield app_process
    app_process.terminate()
    app_process.wait()


@pytest.fixture(scope="session")
def web_service(network_app):
    wait_for_service("http://localhost:5000", "web")


@pytest.fixture(scope="session")
def mrbs_service(web_service):
    wait_for_service("http://mrbs.mgmt/index.php", "mrbs")


@pytest.fixture(scope="module")
def database(configured):  # noqa: F811
    with configured.app_context():
        db = configured.extensions["sqlalchemy"]
        d = SimpleNamespace(db=db, session_objects=list())

        def _add(x):
            d.session_objects.append(x)
            db.session.add(x)
            return x

        d.add = _add
        d.area1 = d.add(Area(area_name="area1", enable_periods=0, approval_enabled=1))
        d.area2 = d.add(Area(area_name="area2", enable_periods=0, approval_enabled=0))
        db.session.commit()
        d.add_user = configured.add_user

        d.room1 = d.add(Room(room_name="area1room1", area_id=d.area1.id))
        d.room2 = d.add(Room(room_name="area2room1", area_id=d.area2.id))
        db.session.commit()

        d.entry = d.add(create_entry(room_id=d.room1.id))
        db.session.commit()

        principal1 = Principal.query.filter(Principal.name == "Principal1").one()
        d.project1 = Project.query.filter(
            db.and_(Project.principal == principal1, Project.name == "Project1")
        ).one()
        d.project2 = d.add(
            Project(
                principal=principal1,
                name="Project2",
                default_rate=db.session.get(Rate, 1),
                default_operator=db.session.get(Operator, 1),
            )
        )
        db.session.commit()

        yield d

        for obj in reversed(d.session_objects):
            db.session.delete(obj)
            db.session.commit()


def session_as(username, realm_name, password="secret"):
    with requests.session() as s:
        url = (
            f"http://keycloak.mgmt:8080/realms/{realm_name}/protocol/openid-connect/auth?response_type=code&"
            "redirect_uri=http%3A%2F%2Fmrbs.mgmt%2Fauthorize.php&"
            "client_id=MGMTTestClient&"
            "nonce=9a84689225d82a70bcee915cece8220a&state=4a057b157272eb620b0277babd8065f5&scope=roles+openid"
        )
        rsp = s.get(url)
        assert rsp.status_code == 200
        url = re.search('action="(.*)" method', rsp.text).group(1).replace("&amp;", "&")
        rsp = s.post(url, data=dict(username=username, password=password))
        assert rsp.status_code == 200

        yield s


@pytest.fixture(scope="module")
def session(database, keycloak_realm, configured):  # noqa: F811
    register_client(configured, keycloak_realm)

    yield from session_as("admin", keycloak_realm.connection.realm_name)


def test_index(database, mrbs_service):
    rsp = requests.get("http://mrbs.mgmt/index.php")
    assert rsp.status_code == 200
    assert "STUDY_NAME" in rsp.text


def test_view(session, mrbs_service):
    rsp = session.get("http://mrbs.mgmt/view_entry.php?&id=1", allow_redirects=True)
    assert rsp.status_code == 200
    assert "STUDY_NAME" in rsp.text
    assert "returl=index.php" in rsp.request.path_url


def test_expire(database, access_users, mrbs_service):  # noqa: F811
    rsp = access_users.admin_user.session.get(
        "http://mrbs.mgmt/view_entry.php?"
        "&id=1&returl=index.php%3Fview%3Dweek%26year%3D2023%26month%3D1%26day%3D31%26area%3D1%26room%3D1"
    )
    assert rsp.status_code == 200
    assert "STUDY_NAME" in rsp.text

    try:
        # Keycloak > 26.1.0
        session_id = (
            base64.b64decode(
                access_users.admin_user.session.cookies["AUTH_SESSION_ID"],
                validate=True,
            )
            .decode()
            .split(".")[0]
        )
    except (base64.binascii.Error, TypeError):
        session_id = access_users.admin_user.session.cookies["AUTH_SESSION_ID"]

    token = OAuth2Token.query.filter(OAuth2Token.sid == session_id).one()
    now = time.time()
    token.expires_at = now
    database.db.session.commit()

    rsp = access_users.admin_user.session.get(
        "http://mrbs.mgmt/view_entry.php?"
        "&id=1&returl=index.php%3Fview%3Dweek%26year%3D2023%26month%3D1%26day%3D31%26area%3D1%26room%3D1"
    )
    assert rsp.status_code == 200
    assert "STUDY_NAME" in rsp.text

    token = OAuth2Token.query.filter(OAuth2Token.sid == session_id).one()
    assert token.expires_at != now


@pytest.fixture(scope="module")
def access_users(keycloak_realm, database, mrbs_service):
    def get_user(username):
        user = User.query.filter(User.username == username).one_or_none()
        if not user:
            user = database.add_user(username)
            database.db.session.commit()
        return SimpleNamespace(
            name=username,
            user=user,
            session=next(
                session_as(
                    username=username, realm_name=keycloak_realm.connection.realm_name
                )
            ),
        )

    add_member = (
        lambda user_, entity, level=MembershipType.full, expires=None: database.add(
            UserMembership(
                group_id=entity.id,
                member_id=user_.id,
                level=level,
                expires=expires,
            )
        )
    )
    u = SimpleNamespace()
    # create cfmm_keycloak users and user memberships in the (un-synced) studymgmt db
    # they already exist on samba container
    u.admin_user = get_user("admin")
    u.power_user1 = get_user("datacare")
    u.power_user2 = get_user("user")
    u.request_user1 = get_user("schedulerequestor")
    add_member(u.request_user1.user, database.project1.group)
    u.request_user2 = get_user("schedulerequestor2")
    add_member(u.request_user2.user, database.project2.group)
    u.request_user3 = get_user("schedulerequestor3")
    add_member(u.request_user3.user, database.project2.group)
    database.db.session.commit()

    schedule_group = Group.query.filter(Group.name == "Schedule").one()
    schedule_request_group = Group.query.filter(Group.name == "Schedule-Request").one()
    add_member(u.power_user1.user, schedule_group)
    add_member(u.power_user2.user, schedule_group)
    add_member(u.request_user1.user, schedule_request_group)
    add_member(u.request_user2.user, schedule_request_group)
    add_member(u.request_user3.user, schedule_request_group)
    database.db.session.commit()

    return u


@pytest.mark.parametrize(
    "user,area,room,check_string,status_code,power_user_fields_disabled",
    [
        # power user can create entries whether areas have requests enabled or not
        ("power_user1", "area1", "room1", "addentry", 200, False),
        ("power_user1", "area2", "room2", "addentry", 200, False),
        # request user can create entries when the area has requests enabled
        # power user fields disabled
        ("request_user1", "area1", "room1", "addentry", 200, True),
        # request user cannot create entries when area has requests disabled
        ("request_user1", "area2", "room2", "approval_bookings_disabled", 200, None),
    ],
)
def test_mrbs_access_create(
    session,
    database,
    keycloak_realm,
    access_users,
    mrbs_service,
    user,
    area,
    room,
    check_string,
    status_code,
    power_user_fields_disabled,
):
    parser = AdvancedHTMLParser.AdvancedHTMLParser()

    def check_power_user_fields_disabled(html):
        parser.parseStr(html)
        return (
            len(
                parser.getElementsByXPathExpression(
                    '//select[@id="f_operator_id" and @class="cfmm-disabled"]'
                )
            )
            == 1
            and len(
                parser.getElementsByXPathExpression(
                    '//select[@id="f_rate_id" and @class="cfmm-disabled"]'
                )
            )
            == 1
            and len(
                parser.getElementsByXPathExpression(
                    '//input[@id="name" and @class="cfmm-disabled"]'
                )
            )
            == 1
        )

    rsp = getattr(access_users, user).session.get(
        f"http://mrbs.mgmt/edit_entry.php?area={getattr(database, area).id}&room={getattr(database, room).id}"
    )
    assert rsp.status_code == status_code
    assert check_string in rsp.text
    if power_user_fields_disabled is not None:
        assert check_power_user_fields_disabled(rsp.text) == power_user_fields_disabled


@pytest.mark.parametrize(
    "i,project,user,room,awaiting_approval,description",
    [
        (1, "project1", "power_user1", "room1", False, "p1e1"),
        (2, "project1", "power_user2", "room1", False, "p2e1"),
        (3, "project1", "request_user1", "room1", True, "r1e1_awaiting"),
        (4, "project1", "request_user1", "room1", False, "r1e1_approved"),
        (5, "project2", "request_user2", "room1", True, "r2e1_awaiting"),
        (6, "project2", "request_user3", "room1", True, "r3e1_awaiting"),
    ],
)
def test_mrbs_access_view_edit(
    session,
    database,
    keycloak_realm,
    access_users,
    mrbs_service,
    i,
    project,
    user,
    room,
    awaiting_approval,
    description,
):
    start = (
        datetime.datetime.now()
        .replace(hour=12, minute=0, second=0, microsecond=0)
        .timestamp()
    )
    entry = database.add(
        create_entry(
            project_id=getattr(database, project).id,
            create_by=getattr(access_users, user).name,
            room_id=getattr(database, room).id,
            start=start + 3600 * i,
            duration=3600,
            awaiting_approval=awaiting_approval,
            description=description,
        )
    )
    database.db.session.commit()

    def check_access(type_, user_, access_string, can_access=True):
        rsp_ = user_.session.get(f"http://mrbs.mgmt/{type_}_entry.php?id={entry.id}")
        assert rsp_.status_code == 200
        assert (access_string in rsp_.text) == can_access, (
            f"Invalid {type_} permission of user {user_.username}"
            f" and entry {entry.description}: expected can_{type_}={can_access}"
        )

    def check_view(user_, can_view=True):
        check_access("view", user_, "event_details", can_view)

    def check_edit(user_, can_edit=True):
        check_access("edit", user_, "editentry", can_edit)

    # admin users can view and edit all entries
    check_edit(access_users.admin_user, can_edit=True)
    check_view(access_users.admin_user, can_view=True)
    # power users can view all entries and edit their own
    for user in (access_users.power_user1, access_users.power_user2):
        check_edit(user, can_edit=(entry.create_by == user.user.username))
        check_view(user, can_view=True)
    # request users can view and edit only their own awaiting_approval entries
    for user in (access_users.request_user1, access_users.request_user2):
        check_edit(
            user, can_edit=(entry.create_by == user.user.username) and entry.status == 2
        )
        check_view(
            user,
            can_view=entry.project in Project.accessible_to(user.user).all()
            and entry.status == 2,
        )


@pytest.mark.parametrize(
    "user_type",
    [
        "admin_user",
        "power_user1",
        "power_user2",
        "request_user1",
        "request_user2",
        "request_user3",
    ],
)
def test_mrbs_access_projects(
    session, database, keycloak_realm, access_users, mrbs_service, user_type
):
    user = getattr(access_users, user_type)
    # tests for projects visibility
    from mgmt.api import get_projects, project_list

    if user.name.lower() in ("admin", "datacare"):
        projects = project_list(Project.query.order_by(Project.study))
    else:
        projects = get_projects(user.user)

    rsp = user.session.get("http://mrbs.mgmt/edit_entry.php")
    assert rsp.status_code == 200

    def compare(match, t):
        if match is None:
            return False
        return json.loads(match.group(1)) == (
            dict(projects[t]) if len(projects[t]) > 0 else projects[t]
        )

    match = re.search(r"^\s*var mrbs_project_dict =\s*(.*?);", rsp.text, re.MULTILINE)
    assert compare(match, "projects")
    match = re.search(r"^\s*var mrbs_principal_dict =\s*(.*?);", rsp.text, re.MULTILINE)
    assert compare(match, "principals")
