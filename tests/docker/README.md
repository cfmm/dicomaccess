# Importing database

```
docker exec -i mgmt-postgresql-1 sh -c 'exec psql --username="$POSTGRES_USER" "$POSTGRES_DB"' < /path/on/host/dump.sql
```

# Using production DB import scripts

1. Get production databases assuming production is using container on LXD remote host:

```bash
lxc exec [remote:]<container> -- sh -c "PGPASSWORD=<password> pg_dump -h localhost --username=mgmt -w mgmt" > dump.sql
```
pg_dump -h localhost --username=mgmt -w mgmt

2. Bring up the docker compose environment, allowing mgmt database to initialize and flask-migrate to run in the `web` container.

3. Import the database into the clean PostgreSQL server. Ensure the postgresql server contains no data, e.g. recreate the container.

```bash
docker exec -i mgmt-postgresql-1 sh -c 'exec psql --set ON_ERROR_STOP=on --single-transaction --username="$POSTGRES_USER" "$POSTGRES_DB"' < dump.sql
```

4. Log in to the Study Management app as `admin`. Synchronize both LDAP and DICOM servers.
