import os
from tests.conftest import mapping

for key, value in mapping.items():
    globals()[key] = value

SQLALCHEMY_DATABASE_URI = "postgresql+psycopg://mgmt:secret@postgresql.mgmt:5432/mgmt"
RATELIMIT_STORAGE_URI = "redis://redis.mgmt:6379/1"

USERMGMT = {
    "prefix": "PREFIX-",
    "user_prefix": "USER-",
    "dicom_ou": os.getenv("MGMT_DICOM_OU", "DICOM"),
    "group_ou": os.getenv("MGMT_MANAGED_OU", "Managed"),
}
