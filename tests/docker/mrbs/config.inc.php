<?php // -*-mode: PHP; coding:utf-8;-*-
namespace MRBS;

$dbsys = "pgsql";
$db_host = "postgresql.mgmt";
$db_database = "mgmt";
$db_login = "mgmt";
$db_password = "secret";

$db_tbl_prefix = "mrbs_";
$db_persist = FALSE;

$auth["type"] = "oidc";

$auth['oidc']['meta_url'] = 'http://keycloak.mgmt:8080/realms/MGMTTestRealm';
$auth['oidc']['client_id'] = 'MGMTTestClient';
$auth['oidc']['client_secret'] = '11111111-2222-3333-4444-555555555555';
$auth['oidc']['validate_interval'] = 30;
# access role
#$auth['oidc']['user'] = 'schedule';
$auth['oidc']['user'] = 'schedule-request';
$auth['oidc']['poweruser'] = 'schedule';

# admin role
$auth['oidc']['admin'] = 'schedule-admin';
$auth['oidc']['debug'] = False;

$auth["session"] = "oidc";

$cookie_path_override = '';

$auth["session_php"]["session_expire_time"] = (60*60*24*30);
$auth["session_php"]["inactivity_expire_time"] = (60*30);

$csrf_cookie["secret"] = 'Fake testing secret';
$refresh_rate = 0;

$mrbs_admin = "Admin";
$mrbs_admin_email = "admin@not.a.server";
$mrbs_company = "TESTING";
$mrbs_company_logo = "CFMM_Western_Logo.png";
$mrbs_company_url = "https://not.a.server/";
$mrbs_company_booking_email = "bookings@not.a.server";
$url_base = "http://mrbs.mgmt";
$mrbs_pending_limit = 5;

$mrbs_studymgmt_url = "http://mrbs_api_server.mgmt:5000";

$timezone = "Canada/Eastern";
$theme = "cfmm";
$show_plus_link = true;
$default_view = "week";
$default_room = 1;
$default_view_all = false;

$mail_settings['admin_on_bookings']      = false;
$mail_settings['room_admin_on_bookings'] = true;
#$mail_settings['on_new']    = false;
$mail_settings['on_new']    = true;
$mail_settings['details']   = true;

# Columns that are listed in the details section of the notification emails
# default columns:  "name", "description", "confirmation_status", "approval_status", "room", "start_time", "duration",
#                   "end_time", "type", "create_by", "last_updated", "rep_type", "rep_opt", "repeat_on", "rep_interval",
#                   "rep_end_date"
#                   (+ custom ones)
$mail_settings['details_columns'] = array("description", "confirmation_status", "approval_status", "room",
"start_time", "duration", "end_time", "type", "rep_type", "rep_opt", "repeat_on", "rep_interval", "rep_end_date",
"project_id");

$mail_settings['domain'] = '@not.a.server';
$mail_settings['admin_backend'] = 'smtp';
$smtp_settings['host'] = 'not.a.server';    // SMTP server
$smtp_settings['port'] = '25';      // SMTP port number
$smtp_settings['auth'] = FALSE;                      // Whether to use SMTP authentication

$mail_settings['from'] = $mrbs_admin_email;
$mail_settings['recipients'] = $mrbs_admin_email;
$mail_settings['debug'] = FALSE;

# Vocabulary
$vocab_override['en']["namebooker"]                 = "Name Override";
$vocab_override['en']["fulldescription"]            = "Description";
$vocab_override['en']["mail_subject_changed_entry"] = "MRI schedule entry has been modified.";
$vocab_override['en']["mail_subject_delete"]        = "MRI schedule entry has been deleted.";
$vocab_override['en']["mrbs"]                       = "MRI Schedule";
$vocab_override['en']["tentative"]                  = "Available";

# 'edited or deleted' --> 'accessed' (cannot be viewed)
$vocab_override['en']["cannot_change_approved_bookings"] = "Bookings that have been approved cannot be accessed";

$vocab_override['en']["cannot_access_project"] = "Cannot access this project's entries";
$vocab_override['en']["approval_bookings_disabled"] = "Bookings approval not enabled. Cannot access approved entries.";

# testing only: override strings for easier matching in tests
foreach (['addentry', 'approval_bookings_disabled', 'confirmation_status', 'cannot_access_project', 'cannot_change_approved_bookings', 'exceeded_pending', 'event_details', 'editentry'] as $var)
{
    $vocab_override['en'][$var] = $var;
}

# Additions for custom fields
$is_mandatory_field['entry.principal']      = false;
$is_mandatory_field['entry.project_id']     = true;
$is_mandatory_field['entry.operator_id']    = true;
$is_mandatory_field['entry.rate_id']        = true;
$is_mandatory_field['entry.name']           = false;

$vocab_override['en']['entry.principal']  = "Principal";
$vocab_override['en']['entry.rate_id']  = "Rate";
$vocab_override['en']['entry.rate']   = "Rate";
$vocab_override['en']['entry.operator_id']   = "Operator";
$vocab_override['en']['entry.operator']   = "Operator";
$vocab_override['en']['entry.project_id']   = "Project";
$vocab_override['en']['entry.project']   = "Project";
$vocab_override['en']['exceeded_pending']  = "Maximum pending booking exceeded";

$private_default   = true;
$private_mandatory = true;

$is_private_field['entry.principal']      = false;
$is_private_field['entry.rate_id']        = false;
$is_private_field['entry.project_id']     = false;
$is_private_field['entry.operator_id']    = false;
$is_private_field['entry.name']           = false;
$is_private_field['entry.description']    = false;
$is_private_field['entry.create_by']      = false;
$is_private_field['entry.modified_by']    = false;

# Do not display these form fields in create & edit entry pages unless the user is a power user or admin:
$is_poweruser_field['name'] = true;
$is_poweruser_field['operator_id']  = true;
$is_poweruser_field['rate_id']  = true;

$edit_entry_field_order = array('principal','project','project_id','operator','operator_id','rate','rate_id','name','description');
$text_input_max = 82;

$approved_bookings_cannot_be_changed = true;
$approval_enabled = true;
