LOAD DATABASE
    FROM mysql://mgmt:secret@127.0.0.1:3306/mgmt
    INTO postgresql://mgmt:secret@127.0.0.1:5432/mgmt

WITH quote identifiers, preserve index names

ALTER SCHEMA 'mgmt' RENAME TO 'public'

AFTER LOAD DO
 $$ INSERT INTO alembic_version (version_num) VALUES ('4fe2d5530347'); $$

CAST
    column mrbs_area.disabled to smallint drop typemod,
    column mrbs_area.default_duration_all_day to smallint drop typemod,
    column mrbs_area.private_enabled to smallint drop typemod,
    column mrbs_area.private_default to smallint drop typemod,
    column mrbs_area.private_mandatory to smallint drop typemod,
    column mrbs_area.min_create_ahead_enabled to smallint drop typemod,
    column mrbs_area.max_create_ahead_enabled to smallint drop typemod,
    column mrbs_area.min_delete_ahead_enabled to smallint drop typemod,
    column mrbs_area.max_delete_ahead_enabled to smallint drop typemod,
    column mrbs_area.max_per_day_enabled to smallint drop typemod,
    column mrbs_area.max_per_week_enabled to smallint drop typemod,
    column mrbs_area.max_per_month_enabled to smallint drop typemod,
    column mrbs_area.max_per_year_enabled to smallint drop typemod,
    column mrbs_area.max_per_future_enabled to smallint drop typemod,
    column mrbs_area.max_secs_per_day_enabled to smallint drop typemod,
    column mrbs_area.max_secs_per_week_enabled to smallint drop typemod,
    column mrbs_area.max_secs_per_month_enabled to smallint drop typemod,
    column mrbs_area.max_secs_per_year_enabled to smallint drop typemod,
    column mrbs_area.max_secs_per_future_enabled to smallint drop typemod,
    column mrbs_area.max_duration_enabled to smallint drop typemod,
    column mrbs_area.approval_enabled to smallint drop typemod,
    column mrbs_area.reminders_enabled to smallint drop typemod,
    column mrbs_area.enable_periods to smallint drop typemod,
    column mrbs_area.confirmation_enabled to smallint drop typemod,
    column mrbs_area.confirmed_default to smallint drop typemod,
    column mrbs_area.times_along_top to smallint drop typemod,

    column mrbs_entry.allow_registration to smallint drop typemod,
    column mrbs_entry.registrant_limit_enabled to smallint drop typemod,
    column mrbs_entry.registration_opens_enabled to smallint drop typemod,
    column mrbs_entry.registration_closes_enabled to smallint drop typemod,

    column mrbs_room.disabled to smallint drop typemod,

    column mrbs_sessions.access to int,

    column mrbs_repeat.rep_interval to smallint drop typemod,
    column mrbs_repeat.month_absolute to smallint drop typemod,
    column mrbs_repeat.confirmation_enabled to smallint drop typemod,
    column mrbs_repeat.confirmation_enabled to smallint drop typemod,

    column authorization.created to TIMESTAMP drop typemod,
    column authorization.modified to TIMESTAMP drop typemod,
    column group.created to TIMESTAMP drop typemod,
    column group.modified to TIMESTAMP drop typemod,
    column group_membership.expires to TIMESTAMP drop typemod,
    column group_membership.warned to TIMESTAMP drop typemod,
    column status.expiry to TIMESTAMP drop typemod,
    column user.created to TIMESTAMP drop typemod,
    column user.modified to TIMESTAMP drop typemod,
    column user_membership.expires to TIMESTAMP drop typemod,
    column user_membership.warned to TIMESTAMP drop typemod,

    type int when unsigned with extra auto_increment to bigserial drop typemod,
    type int when unsigned to bigint drop typemod;
