#!/bin/bash

# https://testdriven.io/courses/flask-celery/app-factory/

# if any of the commands in your code fails for any reason, the entire script fails
set -o errexit
# fail exit if one of your pipe command fails
set -o pipefail
# exits if any of your variables is not set
set -o nounset

python << END
from sqlalchemy import create_engine, exc
from time import sleep
import os

engine = create_engine(os.environ.get("DATABASE_URL"))

while True:
    try:
        engine.connect()
        print(f"Database {engine} is available")
        break
    except exc.OperationalError:
        print(f"Waiting for database {engine} to become available...")
        sleep(1)

END

exec "$@"
