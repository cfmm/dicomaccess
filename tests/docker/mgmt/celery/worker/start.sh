#!/bin/bash

set -o errexit
set -o nounset

celery -A mgmt.celery.app worker --loglevel=INFO
