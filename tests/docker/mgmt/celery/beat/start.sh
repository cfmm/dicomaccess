#!/bin/bash

set -o errexit
set -o nounset

rm -f './celerybeat.pid'
celery -A mgmt.celery.app beat -l info
