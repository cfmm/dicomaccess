#!/bin/bash

set -o errexit
set -o nounset

worker_ready() {
    celery -A mgmt.celery.app inspect ping
}

until worker_ready; do
  >&2 echo 'Celery workers not available'
  sleep 1
done
>&2 echo 'Celery workers is available'

celery -A mgmt.celery.app flower \
    --broker="${CELERY_BROKER_URL}" \
    --port=5555
