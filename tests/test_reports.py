from mgmt.models.credentials import User
from mgmt.user_reports import UserReport
import xml.etree.ElementTree as ET


def test_report_query(configured, mock_ldap):
    with configured.test_request_context():
        from mgmt.mrbs.reports import Report

        user = User.query.first()
        query = Report.generate_query(user=user)
        query.all()


def validate_xml(tree):
    assert len(tree) == 1
    assert tree.tag == "users"

    for element in tree:
        assert element.tag == "user"
        assert len(element) == 2
        assert element.findall(".//name")
        assert element.findall(".//mail")

    # At least all the principals must always be present
    for user in User.query.all():
        if user.principals:
            elements = tree.findall(f"./user[mail='{user.email}']")
            assert len(elements) == 1
            assert f"{user.first_name} {user.last_name}" in (
                x.text for x in elements[0].findall("./name")
            )


def test_report_principals(configured, mock_ldap):
    with configured.test_request_context():
        tree = ET.fromstring(UserReport().principals())
        validate_xml(tree)


def test_report_maintainers(configured, mock_ldap):
    with configured.test_request_context():
        tree = ET.fromstring(UserReport().maintainers())
        validate_xml(tree)


def test_report_users(configured, mock_ldap):
    with configured.test_request_context():
        tree = ET.fromstring(UserReport().all_users())
        validate_xml(tree)
