from mgmt.screening.admin import ScreeningFormAction, ScreeningState
from mgmt.models.screening import ScreeningForm
from mgmt.models.dicom import Project
import AdvancedHTMLParser
from .conftest import login


def test_participant_screening(configured, mocker):
    send_mock = mocker.patch("mgmt.screening.admin.send_email", return_value=None)

    with configured.test_request_context():
        with configured.app_context():
            project = Project.query.first()
            user = project.principal.user

            model = ScreeningForm()
            model.project = project
            model.create_user = user
            model.state = ScreeningState.created

            ScreeningFormAction(
                new_state=ScreeningState.datacare_review,
                submit_style="success",
                submit_label="Submit for MR tech review",
                notify="coordinator",
            ).send_notification(model)
        send_mock.assert_called()
        AdvancedHTMLParser.AdvancedHTMLParser().parseStr(send_mock.call_args[0][4])


def test_participant_send(client, mocker):
    send_mock = mocker.patch("mgmt.screening.views.send_email", return_value=None)

    login(client, "datacare")

    data = dict(email="invalid_subject@nowhere.com", project="1", coordinator="someone")
    client.post("/screening_send/", data=data)

    send_mock.assert_called()
    AdvancedHTMLParser.AdvancedHTMLParser().parseStr(send_mock.call_args[0][4])
