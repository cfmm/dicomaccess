from flask_cfmm.resource import get_keycloak
from keycloak import KeycloakOpenID

from mgmt.models.dicom import Principal, Project
from .helpers import register_client, impersonate_user

# noinspection PyProtectedMember
from flask_cfmm.security import _datastore
from flask_cfmm.configure import get_role


def test_client_registration(configured, keycloak_realm):
    # Test that client registration works the first time
    register_client(configured, keycloak_realm)
    # Add it also works the second time
    changed = register_client(configured, keycloak_realm, False)

    assert changed is False, "Registration changed on second call"


def test_project_query_api_invalid_status(client, keycloak_realm):
    register_client(client.application, keycloak_realm)
    rsp = client.get("/api/projects")

    assert rsp.status_code == 401

    keycloak = KeycloakOpenID(**get_keycloak(client.application))
    token = keycloak.token(
        # incorrect type specified in keycloak package
        grant_type=["client_credentials"]  # type: ignore
    )

    headers = {
        "Authorization": f"Bearer {token['access_token']}",
        "Content-Type": "application/json",
    }

    rsp = client.get("/api/projects", headers=headers, json=dict())

    assert rsp.status_code == 400


def test_project_query_api_valid_status(query_env, keycloak_realm, client):
    register_client(client.application, keycloak_realm, impersonation=True)

    token = impersonate_user(client.application, keycloak_realm, "datacare")

    headers = {
        "Authorization": f"Bearer {token['access_token']}",
        "Content-Type": "application/json",
    }

    rsp = client.get("/api/projects", headers=headers)

    assert rsp.status_code == 200


def test_project_query_api_datacare(query_env, keycloak_realm, client):
    q = query_env

    def get_projects_as_user(user_):
        token = impersonate_user(client.application, keycloak_realm, user_.username)
        headers = {
            "Authorization": f"Bearer {token['access_token']}",
            "Content-Type": "application/json",
        }

        return client.get("/api/projects", headers=headers)

    register_client(client.application, keycloak_realm, impersonation=True)
    # user has no access to projects
    user = q.users[4]

    rsp = get_projects_as_user(user)
    assert rsp.status_code == 200, "User cannot access projects"
    assert rsp.is_json and rsp.json == {
        "principals": list(),
        "projects": list(),
    }, "User should not see any projects"

    # give user the datacare role
    _datastore.add_role_to_user(
        user, _datastore.create_role(name=get_role("datacare_role"))
    )

    rsp = get_projects_as_user(user)
    assert rsp.status_code == 200, "Datacare cannot access projects"
    assert (
        rsp.is_json
        and len(rsp.json.get("principals", list())) == Principal.query.count()
        and len(rsp.json.get("projects", list())) == Project.query.count()
    ), "Datacare user does not see all projects"
