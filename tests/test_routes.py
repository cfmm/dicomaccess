import pytest
import datetime
import urllib

from mgmt.admin_base import (
    MyModelView,
    DatacareAdminAccess,
    AdminAccess,
    DatacareAccess,
    UserAccess,
    UserDatacareAccess,
)
from mgmt.models.credentials import User
from mgmt.models.dicom import Project
from mgmt.models.screening import ScreeningForm, ScreeningState
from .conftest import login
from urllib.parse import urlparse
from flask_cfmm.configure import get_role
from sqlalchemy.sql.schema import Column


def test_screening_send(client):
    login(client)
    rsp = client.get("/screening_send/", follow_redirects=False)
    assert rsp.status_code == 200


def test_report(client):
    login(client)
    rsp = client.get("/reportview/report", follow_redirects=False)
    assert rsp.status_code == 200

    data = {
        "timezone": "America/Toronto",
    }
    rsp = client.post("/reportview/report", data=data, follow_redirects=False)
    assert rsp.status_code == 200


def test_newuser(client):
    login(client)
    rsp = client.get("/newuser", follow_redirects=True)
    assert rsp.status_code == 200


def execute_view(client, view, check_list=None, check_edit=True):
    rsp = client.get(f"{view}/", follow_redirects=False)
    assert rsp.status_code == 200, f"List view error when accessing {view}"

    if check_list is not None:
        check_list(rsp)

    def evaluate_request(endpoint, request_type):
        rsp_ = client.get(f"{view}/{endpoint}", follow_redirects=False)
        if rsp_.status_code == 302:
            if urlparse(rsp_.location).path.rstrip("/") != f"{view}":
                raise ValueError("Redirect not for empty set")
        else:
            assert (
                rsp_.status_code == 200
            ), f"{request_type} view Error. Unexpected status code {rsp_.status_code}."

    if check_edit:
        evaluate_request("edit/?id=1", "Edit")
    evaluate_request("new/", "Create")


role_views_list = [
    (
        "admin_role",
        lambda v: any(
            issubclass(type(v), a) for a in (DatacareAdminAccess, AdminAccess)
        ),
    ),
    (
        "datacare_role",
        lambda v: any(
            issubclass(type(v), a)
            for a in (DatacareAdminAccess, UserDatacareAccess, DatacareAccess)
        ),
    ),
    (
        "access_role",
        lambda v: any(issubclass(type(v), a) for a in (UserDatacareAccess, UserAccess)),
    ),
]


@pytest.mark.parametrize(
    "role,role_views",
    role_views_list,
)
def test_route(configured, client, role, role_views, mock_ldap):
    """
    For each role type, access all visible views and verify expected status codes
    """
    with configured.app_context():
        login(client, role=get_role(role))
        views = [
            v
            for v in configured.extensions["admin"][0]._views
            if issubclass(type(v), MyModelView) and role_views(v)
        ]
        try:
            for view in views:
                # do not check edit view of first entry when accessing as normal user (will be forbidden)
                execute_view(client, view.url, check_edit=role != "access_role")
        except Exception as e:
            raise RuntimeError(f"Exception while trying to access {view.url}") from e


@pytest.mark.parametrize("role", ["admin_role", "datacare_role", "access_role"])
@pytest.mark.parametrize(
    "api_view,params",
    [
        ("project_search", {"principal": 1}),
        ("entry_search", {"project": 1}),
        ("entry_search", {"project": 1, "timestamp": 1200}),
        ("project_description", {"id": 1}),
    ],
)
def test_api_route(configured, client, role, api_view, params):
    with configured.app_context():
        login(client, role=get_role(role))
        rsp_ = client.get(
            f"/{api_view}?{urllib.parse.urlencode(params)}", follow_redirects=False
        )
        assert (
            rsp_.status_code == 200
        ), f"Cannot access {api_view} with {params} as {role}"


def test_screening_form_archive(configured, client):
    with configured.app_context():
        screening_form = child_form = None
        try:
            db = configured.extensions["sqlalchemy"]
            screening_form = ScreeningForm(
                project_id=Project.query.first().id,
                create_user_id=User.query.first().id,
                create_time=datetime.datetime.now(),
                state=ScreeningState.created,
                version="20210916_01",
            )
            db.session.add(screening_form)
            db.session.commit()
            login(client, role=get_role("admin_role"))
            rsp_ = client.post(
                "/dc_screeningform/action",
                follow_redirects=True,
                data={"action": "archive", "rowid": screening_form.id},
            )
            assert rsp_.status_code == 200, "Cannot post screening form action"

            child_form = ScreeningForm.query.filter(
                ScreeningForm.parent_form_id == screening_form.id
            ).one()
            assert (
                child_form.state == ScreeningState.archived
            ), "Did not successfully archive the form"
        finally:
            if child_form:
                db.session.delete(child_form)
            if screening_form:
                db.session.delete(screening_form)
            if child_form or screening_form:
                db.session.commit()


@pytest.mark.parametrize(
    "role,role_views",
    role_views_list,
)
def test_app_filters(configured, client, role, role_views):
    """
    For each role type, access all filtered views and verify expected status codes
    """
    with configured.app_context():
        login(client, role=get_role(role))
        from mgmt.screening.views import ScreeningState

        # test values to use when composing a URL for filtered details view
        test_values = {
            str: "a",
            int: "1",
            bool: "1",
            float: "1.1",
            datetime.datetime: urllib.parse.quote_plus("2000-01-01 00:00:00"),
            datetime.date: "2000-01-01",
            ScreeningState: ScreeningState.created.value,
        }

        invalid_test_values = {
            str: [],
            int: ["a", "1.1"],
            bool: ["a", "1.1"],
            float: [
                "a",
            ],
            datetime.datetime: ["15", "a", "1.1"],
            datetime.date: ["15", "a", "1.1"],
            ScreeningState: ["a", "1", "1.1"],
        }

        views_with_filters = [
            v
            for v in configured.extensions["admin"][0]._views
            if issubclass(type(v), MyModelView) and role_views(v) and v.get_filters()
        ]

        def filter_url_test(url_, status_code):
            try:
                rsp_ = client.get(url_, follow_redirects=False)
            except Exception as e_:
                raise RuntimeError(f"Exception while trying to access {url_}") from e_
            else:
                if rsp_:
                    assert (
                        rsp_.status_code == status_code
                    ), f"Invalid status code when accessing {url_}: {rsp_.data}"

        for view in views_with_filters:
            filters = view.get_filters()
            for fidx in range(len(filters)):
                flt = filters[fidx]
                if issubclass(type(flt.column), Column):
                    url = f"{view.url}/?flt0_{view.get_filter_arg(fidx, flt)}="

                    filter_url_test(
                        f"{url}{test_values[flt.column.type.python_type]}", 200
                    )

                    for value in invalid_test_values[flt.column.type.python_type]:
                        # Invalid filter values result in flash error message, but return value is still 200
                        filter_url_test(f"{url}{value}", 200)


def test_main_page(client, mock_ldap):
    rsp = client.get("/", follow_redirects=False)
    assert rsp.status_code == 200, "Error as anonymous on main page"
    login(client)
    rsp = client.get("/", follow_redirects=False)
    assert rsp.status_code == 200, "Error as admin on main page"


@pytest.mark.parametrize(
    "view",
    [
        "project",
        "group",
        "server",
    ],
)
def test_route_user(client, view, mock_ldap):
    with client.application.app_context():
        login(client, role=get_role("access_role"))
    rsp = client.get(f"/{view}/", follow_redirects=False)
    assert rsp.status_code == 200, "List view error"


def test_route_datacare_group(client, mock_ldap):
    with client.application.app_context():
        # First user is admin
        login(client, role=get_role("admin_role"))
        # Second user is datacare
        login(client, role=get_role("datacare_role"))

    # Datacare should not see admin group
    def check_list(rsp):
        assert b"Admin" not in rsp.data

    execute_view(client, "/dc_group", check_list)


def test_route_datacare_user(client, mock_ldap):
    with client.application.app_context():
        # First user is admin
        login(client, role=get_role("admin_role"))
        # Second user is datacare
        login(client, role=get_role("datacare_role"))

    execute_view(client, "/dc_user")


def test_user_member(client):
    login(client)
    rsp = client.get("/admin_group/edit/?id=1", follow_redirects=False)
    assert rsp.status_code == 200

    data = {
        "name": "Principal1",
        "descrtiption": "",
        "user_member-0-member": "",
        "user_member-0-id": "",
        "user_member-0-expires": "",
        "user_member-0-level": "",
    }
    rsp = client.post("/admin_group/edit/?id=1", data=data, follow_redirects=False)
    assert rsp.status_code == 200
    assert b"This field is required." in rsp.data
