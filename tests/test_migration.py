import flask_migrate
import pytest

from mgmt import create_app
from .conftest import mapping
from .helpers import database_access
from migrations.reset import reset


def check_migrate(
    app,
    directory=None,
    message=None,
    sql=False,
    head="head",
    splice=False,
    branch_label=None,
    version_path=None,
    rev_id=None,
    x_arg=None,
):
    config = app.extensions["migrate"].migrate.get_config(directory, x_arg=x_arg)
    config.set_main_option("revision_environment", "true")

    # noinspection PyUnusedLocal
    def process_revision_directives(context, revision, directives):
        if not directives[0].upgrade_ops.is_empty():
            raise ValueError("Migration not empty")
        directives[:] = []

    return flask_migrate.command.revision(
        config,
        message,
        autogenerate=True,
        sql=sql,
        head=head,
        splice=splice,
        branch_label=branch_label,
        version_path=version_path,
        rev_id=rev_id,
        process_revision_directives=process_revision_directives,
    )


@pytest.fixture()
def migrate_app():
    config = dict(mapping)
    # use a separate DB to avoid clobbering the main testing DB
    config["SQLALCHEMY_DATABASE_URI"] = (
        "postgresql+psycopg://mgmt:secret@postgresql.mgmt:5432/mgmt_migrate"
    )

    return create_app(mapping=config)


def test_migration(migrate_app):
    db = migrate_app.extensions["sqlalchemy"]

    if not database_access(migrate_app):
        with migrate_app.app_context():
            pytest.skip(f"Unable to connect to {db.engine.url}")

    reset(migrate_app)

    with migrate_app.app_context():
        # Non-empty migration will trigger exception
        check_migrate(migrate_app)
