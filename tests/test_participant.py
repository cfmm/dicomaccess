import datetime

from mgmt.models.credentials import User
from mgmt.models.dicom import Project
from mgmt.models.screening import ScreeningForm, ScreeningState


def test_participant(client, configured, mocker):
    mocker.patch("mgmt.screening.admin.send_email", return_value=None)

    with configured.app_context():
        try:
            form_ = ScreeningForm(
                project_id=Project.query.first().id,
                create_user_id=User.query.first().id,
                create_time=datetime.datetime.now(),
                state=ScreeningState.created,
                version="20210916_01",
            )
            db = configured.extensions["sqlalchemy"]
            db.session.add(form_)
            db.session.commit()

            token, expiry = form_.get_token(
                max_age=1000,
                extra_fields={
                    "email": "notanemail",
                    "purpose": "participant_send",
                },
            )
            rsp = client.get(f"/screening/participant/{token}")
            assert rsp.status_code == 200

            data = {
                "version": "20210916_01",
                "subject_surname": "Subject",
                "subject_given_name": "Test",
                "subject_birthdate": "2000",
                "ScreeningState.coordinator_review": 0,
            }
            for field in form_.__mapper__.column_attrs:
                if field.key.startswith("q_"):
                    data[field.key] = 0

            rsp = client.post(f"/screening/participant/{token}", data=data)
            assert rsp.status_code == 200
            assert b"Your form has been successfully saved" in rsp.data

            rsp = client.post(f"/screening/participant/{token}", data=data)
            assert rsp.status_code == 302
        finally:
            db.session.delete(form_)
            db.session.commit()
