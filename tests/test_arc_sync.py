import pytest
from keycloak import KeycloakOpenID
from flask_cfmm.resource import get_keycloak
from mgmt.arc import ArcConfig, ArcConfigurationError
from .helpers import register_client


def test_arcconfig_url(configured):
    # URL is not accessible
    obj = ArcConfig(
        {
            "url": "http://something.invalid",
            "verify": False,
        }
    )
    with configured.app_context():
        with pytest.raises(ArcConfigurationError):
            obj.connect()


def test_arcconfig_keycloak(client):
    # Keycloak is not configured and authentication will fail
    obj = ArcConfig(
        {
            "url": "http://something.invalid",
            "verify": False,
        }
    )
    with client.application.app_context():
        with pytest.raises(ArcConfigurationError):
            obj.connect()


def test_sync_route(client, configured, keycloak_realm, mock_user):
    register_client(configured, keycloak_realm)
    res = client.get("/update")
    assert res.status_code == 302
    with client.session_transaction() as session:
        flash_message = dict(session["_flashes"]).get("warning")
    assert flash_message is None


def test_api_sync_route(client, configured, keycloak_realm, mock_user):
    register_client(configured, keycloak_realm)
    res = client.get("/api/arc/sync")
    assert res.status_code == 405

    res = client.post("/api/arc/sync")
    assert res.status_code == 401

    keycloak = KeycloakOpenID(**get_keycloak(client.application))
    token = keycloak.token(grant_type=["client_credentials"])

    headers = {
        "Authorization": f"Bearer {token['access_token']}",
        "Content-Type": "application/json",
    }

    res = client.post("/api/arc/sync", headers=headers)
    assert res.status_code == 200

    res = client.post(
        "/api/arc/sync", headers=headers, json=configured.config["DCM4CHEE5"]
    )
    assert res.status_code == 200

    data = {
        "url": "http://bogus.url",
        "verify": False,
    }
    res = client.post("/api/arc/sync", headers=headers, json=data)
    assert res.status_code == 400
