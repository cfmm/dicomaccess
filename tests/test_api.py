from mgmt.models.dicom import Project


def test_project_query(query_env):
    q = query_env

    from mgmt.api import get_projects

    data = get_projects(q.users[4])

    assert "principals" in data
    assert "projects" in data
    assert data == {
        "principals": list(),
        "projects": list(),
    }, "User should not see any projects"
    q.projects[0][2].active = True
    q.add_member(q.users[4], q.projects[0][2])
    data = get_projects(q.users[4])

    expected_principals = [
        (f"0{q.projects[0][2].principal.id}", [f"0{q.projects[0][2].id}"])
    ]

    expected_projects = [
        (
            f"0{q.projects[0][2].id}",
            {
                "name": q.projects[0][2].study,
                "principal_id": f"0{q.projects[0][2].principal.id}",
                "rate_id": f"0{q.projects[0][2].default_rate.id}",
                "operator_id": f"0{q.projects[0][2].default_operator.id}",
                "active": True,
            },
        ),
    ]
    assert data["principals"] == expected_principals
    assert data["projects"] == expected_projects

    # Inactive projects should show up in the list, as inactive

    q.db.session.get(Project, q.projects[0][2].id).active = False
    expected_projects[0][1]["active"] = False
    data = get_projects(q.users[4])
    assert (
        data["principals"] == list()
    ), "Inactive projects should not show up in Principals list"
    assert data["projects"] == expected_projects

    # Adding another active object of same principal, principals list now includes this project
    q.add_member(q.users[4], q.projects[0][1])
    expected_principals = [
        (f"0{q.projects[0][1].principal.id}", [f"0{q.projects[0][1].id}"])
    ]
    data = get_projects(q.users[4])
    assert data["principals"] == expected_principals

    # Adding a project of another principal, principals list now has two principals
    q.add_member(q.users[4], q.projects[1][1])
    expected_principals.append(
        (f"0{q.projects[1][1].principal.id}", [f"0{q.projects[1][1].id}"])
    )
    data = get_projects(q.users[4])
    assert data["principals"] == expected_principals
