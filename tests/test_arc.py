import pytest
import ldap3
from requests import Response
from mgmt.arc import ReloadError, ArcUpdate, ArcConfig
from mgmt.arc_schema import update_schema, schema_contains, raw_schema, make_ldif
from mgmt.models.dicom import Server, Destination, Project, Principal, Tag
from mgmt.models.credentials import Group
from uuid import uuid4

from urllib.parse import urlparse, urlunparse
from authlib.common.errors import AuthlibBaseError
from cfmm_keycloak.configure import env_config


@pytest.fixture(scope="module")
def mock_config(module_mocker):
    data = {
        "ldapUrl": env_config.arc.host,
        "userDN": env_config.arc.user,
        "userPassword": env_config.arc.password,
        "deviceDN": f"dicomDeviceName={env_config.arc.device},cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        "devicesDN": f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}",
        "configurationDN": f"cn=DICOM Configuration,{env_config.arc.base}",
        "aetsRegistryDN": f"cn=Unique AE Titles Registry,cn=DICOM Configuration,{env_config.arc.base}",
    }
    module_mocker.patch(
        "mgmt.arc.ArcConfig._get_config",
        return_value=data,
    )

    token = {"access_token": str(uuid4())}

    module_mocker.patch(
        "mgmt.arc.ArcConfig.token",
        new_callable=module_mocker.PropertyMock,
        return_value=token,
    )

    return {
        "token": token,
    }


@pytest.fixture()
def mock_reload(mocker):
    r = Response()
    r.status_code = 204
    return mocker.patch("mgmt.arc.ArcConfig.reload", return_value=r)


@pytest.mark.parametrize("status", [200, 204, 500])
def test_arc_reload(configured, mocker, status, mock_config):
    r = Response()
    r.status_code = status
    m1 = mocker.patch("requests.post", return_value=r)
    if status == 500:
        mocker.patch("mgmt.arc.ArcConfig.token", side_effect=AuthlibBaseError)

    config = configured.config["DCM4CHEE5"][0]
    a = ArcConfig(config)

    with configured.app_context():
        if status in (200, 500):
            with pytest.raises(ReloadError):
                a.reload()
        else:
            a.reload()

    assert m1.call_count == 1, "Missing post call"

    kwargs = m1.call_args_list[0].kwargs

    url = urlparse(config["url"])
    url = urlunparse(url)
    url = "/".join(s.strip("/") for s in (url, "ctrl/reload"))

    assert kwargs["url"] == url
    assert kwargs["verify"] == config["verify"]
    if status != 500:
        assert kwargs["auth"].token == mock_config["token"]


def test_arc_config(configured, mock_ldap, mock_config):
    configs = configured.config["DCM4CHEE5"]
    ArcUpdate(None)
    ArcUpdate(configs[0])
    a = ArcUpdate(configs)

    # Prevent an actual reload
    def reload():
        return True

    def fail():
        pytest.fail("Unexpected flash")

    for server in a.servers:
        server.reload = reload
        server.flash = fail

    # Check functionality
    a.update(None, None)
    with configured.app_context():
        a.sync()

        server = Server()
        destination = Destination(server=server)
        items = [Tag(), server, destination, Project(), Principal()]
        a.update(items)


def validate(data):
    assert data["bind"].call_count == 1, "Missing bind call"

    assert data["tls"].call_count == 0, "Unexpected tls call"
    assert data["delete"].call_count == 0, "Unexpected delete call"
    assert data["modify"].call_count == 0, "Unexpected modify call"
    assert data["modify_dn"].call_count == 0, "Unexpected modify_dn call"


def test_update_tag(configured, mock_ldap):
    updater = ArcUpdate(configured.config["DCM4CHEE5"])
    with configured.app_context():
        updater.update([Tag()])

    validate(mock_ldap)
    assert mock_ldap["search"].call_count == 0, "Incorrect search calls for tag"
    assert mock_ldap["add"].call_count == 0, "Incorrect add calls for tag"


@pytest.mark.parametrize(
    "cipher,network", [(None, "dicom"), ("something", "dicom-tls")]
)
def test_update_server(configured, mock_ldap, cipher, network, mock_reload):
    updater = ArcUpdate(configured.config["DCM4CHEE5"])
    with configured.app_context():
        updater.update(
            [
                Server(
                    id=1,
                    aet="MYAETITLE",
                    installed=False,
                    host="localhost",
                    cipher=cipher,
                )
            ]
        )

    validate(mock_ldap)
    assert mock_ldap["search"].call_count == 9, "Incorrect number of search calls"
    assert mock_ldap["add"].call_count == 5, "Incorrect number of add calls"

    assert (
        mock_ldap["add"].mock_calls[4].args[0]
        == "dicomAETitle=MYAETITLE,dicomDeviceName=MYAETITLE@localhost,"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[4].kwargs["attributes"] == {
        "dicomAETitle": "MYAETITLE",
        "mgmtId": [1],
        "mgmtModel": "server",
        "objectClass": ["dicomNetworkAE", "mgmtRecord"],
        "dicomNetworkConnectionReference": [
            f"cn={network},dicomDeviceName=MYAETITLE@localhost,cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
        ],
        "dicomAssociationInitiator": False,
        "dicomAssociationAcceptor": True,
    }

    attributes = {
        "cn": [network],
        "mgmtId": [1],
        "mgmtModel": "server",
        "objectClass": ["dicomNetworkConnection", "mgmtRecord"],
        "dicomHostname": "localhost",
    }

    if cipher is not None:
        attributes["dicomTLSCipherSuite"] = [cipher]

    assert (
        mock_ldap["add"].mock_calls[3].args[0]
        == f"cn={network},dicomDeviceName=MYAETITLE@localhost,cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[3].kwargs["attributes"] == attributes

    assert (
        mock_ldap["add"].mock_calls[2].args[0]
        == f"dicomAETitle=MYAETITLE,cn=Unique AE Titles Registry,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[2].kwargs["attributes"] == {
        "dicomAETitle": "MYAETITLE",
        "mgmtId": [1],
        "mgmtModel": "server",
        "objectClass": ["dicomUniqueAETitle", "mgmtRecord"],
    }
    assert (
        mock_ldap["add"].mock_calls[1].args[0]
        == f"dcmExporterID=MYAETITLE@localhost,dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[1].kwargs["attributes"] == {
        "dcmExporterID": ["MYAETITLE@localhost"],
        "mgmtId": [1],
        "mgmtModel": "server",
        "objectClass": ["dcmExporter", "mgmtRecord"],
        "dcmURI": f"{network}:MYAETITLE",
        "dicomAETitle": "DCM4CHEE",
        "dcmQueueName": "Export",
        "dcmExportAsSourceAE": True,
    }

    assert (
        mock_ldap["add"].mock_calls[0].args[0]
        == f"dicomDeviceName=MYAETITLE@localhost,cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[0].kwargs["attributes"] == {
        "dicomDeviceName": "MYAETITLE@localhost",
        "mgmtId": [1],
        "mgmtModel": "server",
        "objectClass": ["dicomDevice", "mgmtRecord"],
        "dicomInstalled": False,
    }


def test_update_destination(configured, mock_ldap, mock_reload):
    updater = ArcUpdate(configured.config["DCM4CHEE5"])
    with configured.app_context():
        server = Server(id=1, aet="MYAETITLE", installed=True, host="localhost")
        destination = Destination(id=2, server=server)
        updater.update([destination])

    validate(mock_ldap)
    assert mock_ldap["search"].call_count == 1, "Incorrect search calls for destination"
    assert mock_ldap["add"].call_count == 1, "Incorrect add calls for destination"

    assert (
        mock_ldap["add"].mock_calls[0].args[0]
        == f"cn=mgmtDestination2,dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[0].kwargs["attributes"] == {
        "cn": ["mgmtDestination2"],
        "mgmtId": [2],
        "mgmtModel": "destination",
        "objectClass": ["dcmExportRule", "mgmtRecord"],
        "dcmEntity": "Series",
        "dcmExporterID": ["MYAETITLE@localhost"],
        "dcmDuration": "PT1M",
        "dcmExportReoccurredInstances": "REPLACE",
        "dcmExportPreviousEntity": False,
    }


def test_update_project(configured, mock_ldap, mock_reload):
    updater = ArcUpdate(configured.config["DCM4CHEE5"])
    with configured.app_context():
        group = Group(id=5, name="Principal_GROUP")
        principal = Principal(id=4, name="Principal_NAME", group=group)
        project = Project(
            id=3,
            study="DYNAMIC_STUDY_NAME",
            group=group,
            principal=principal,
            name="Project_NAME",
        )
        project.study = "DYNAMIC_STUDY_NAME"
        updater.update([project])

    validate(mock_ldap)
    assert mock_ldap["search"].call_count == 1, "Incorrect search calls for project"
    assert mock_ldap["add"].call_count == 1, "Incorrect add calls for project"

    assert (
        mock_ldap["add"].mock_calls[0].args[0]
        == f"cn=mgmtProject3,dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[0].kwargs["attributes"] == {
        "cn": ["mgmtProject3"],
        "mgmtId": [3],
        "mgmtModel": "project",
        "objectClass": ["dcmStoreAccessControlIDRule", "mgmtRecord"],
        "dcmStoreAccessControlID": "5",
        "dcmRulePriority": 10,
        "dcmProperty": [
            "StudyDescription=(?i)^(\\QDYNAMIC_STUDY_NAME\\E|\\QPrincipal_NAME\\E[\\^ ]\\QProject_NAME\\E)$"
        ],
    }


def test_update_principal(configured, mock_ldap, mock_reload):
    updater = ArcUpdate(configured.config["DCM4CHEE5"])
    with configured.app_context():
        group = Group(id=5, name="Principal_GROUP")
        updater.update([Principal(id=4, name="Principal_NAME", group=group)])

    validate(mock_ldap)
    assert mock_ldap["search"].call_count == 1, "Incorrect search calls for principal"
    assert mock_ldap["add"].call_count == 1, "Incorrect add calls for principal"

    assert (
        mock_ldap["add"].mock_calls[0].args[0]
        == f"cn=mgmtPrincipal4,dicomDeviceName={env_config.arc.device},"
        f"cn=Devices,cn=DICOM Configuration,{env_config.arc.base}"
    )
    assert mock_ldap["add"].mock_calls[0].kwargs["attributes"] == {
        "cn": ["mgmtPrincipal4"],
        "mgmtId": [4],
        "mgmtModel": "principal",
        "objectClass": ["dcmStoreAccessControlIDRule", "mgmtRecord"],
        "dcmStoreAccessControlID": "5",
        "dcmRulePriority": 5,
        "dcmProperty": ["StudyDescription=(?i)^\\QPrincipal_NAME\\E([\\^ ].*$|$)"],
    }


def test_schema_update_fail():
    server = "ldap://not.a.server"
    user = "cn=admin,cn=config"
    password = "secret"
    update_schema(server, user, password)


def test_schema_update():
    server = "ldap://ldap.mgmt"
    user = "cn=admin,cn=config"
    password = "secret"

    c = ldap3.Connection(server, user, password)
    try:
        c.bind()
    except ldap3.core.exceptions.LDAPSocketOpenError:
        pytest.skip(f"OpenLDAP server {server} is not running")

    mgmt = ldap3.SchemaInfo.from_json(raw_schema)

    def validate():
        update_schema(server, user, password)

        test_conn = ldap3.Connection(server, user, password)
        test_conn.bind()
        add, replace = schema_contains(mgmt, test_conn.server.schema)

        assert not add, "Schema missing after update"
        assert not replace, "Schema different after update"

    validate()

    # The schema must exist now, so modify the schema to not match and test update
    assert c.search(
        "cn=schema,cn=config",
        f"(&(objectClass={')(objectClass='.join(mgmt.other['objectClass'])})(cn=*}}mgmt))",
        search_scope=ldap3.SUBTREE,
    ), "Unable to find schema DN"

    assert c.modify(
        c.response[0]["dn"],
        changes={
            "olcAttributeTypes": (
                ldap3.MODIFY_REPLACE,
                [
                    "( 1.2.826.0.1.3680043.9.5356.2.2.1.1 NAME 'mgmtId' DESC 'Not the right description' "
                    "EQUALITY integerMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 )"
                ],
            ),
        },
    )

    validate()


def test_schema_route(client):
    res = client.get("/api/schema")
    assert res.status_code == 200
    assert res.data.decode("ascii") == make_ldif()
