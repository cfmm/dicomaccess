import socket
from sqlalchemy.exc import OperationalError
from mgmt.core import create_client
from flask_cfmm.resource import get_keycloak
from keycloak import KeycloakDeleteError, KeycloakOpenID, KeycloakError


def database_access(app):
    db_ = app.extensions["sqlalchemy"]
    with app.app_context():
        try:
            db_.engine.connect()
        except OperationalError:
            return False
    return True


def ldap_access(app):
    ldap = app.extensions["ldap"]
    try:
        socket.gethostbyname(ldap.conn.server.host)
    except socket.error:
        return False
    ldap.connect()
    return ldap.conn.bound


def register_client(app, admin, clean=True, impersonation=False):
    if clean:
        with app.app_context():
            configurator = app.extensions["cfmm"].configuration
            configurator.remove("registration_token")
            configurator.remove("registration_token_sync")
        try:
            admin.delete_client(
                admin.get_client_id(app.config.get("KEYCLOAK_CLIENT_ID"))
            )
            admin.delete_client(admin.get_client_id("mgmt-sync"))
        except KeycloakDeleteError:
            pass
    token = admin.create_initial_access_token(2, 5)
    changed = create_client(
        app,
        token["token"],
        ["http://test.mgmt", "http://web.mgmt:5000", "http://python.mgmt:5000"],
        redirect_urls=["http://mrbs.mgmt/authorize.php*"],
    )

    try:
        # Ensure client service account is member of RELOAD_USER_ROLE="arc-reload"
        data = get_keycloak(app)
    except IndexError:
        return changed

    reload_role = admin.realm_config["roles"]["dcm4chee_reload"]
    client_id = admin.get_client_id(client_id=data["client_id"])
    user = admin.get_client_service_account_user(client_id=client_id)
    for role in admin.get_realm_roles():
        if role["name"] == reload_role:
            admin.assign_realm_roles(user_id=user["id"], roles=[role])
            break
    else:
        assert False, f"Failed to find {reload_role} role"

    if impersonation:
        clients = admin.get_clients()

        for value in clients:
            if value["clientId"] == data["client_id"]:
                user = admin.get_client_service_account_user(value["id"])
                break
        else:
            raise Exception()

        for value in clients:
            if value["clientId"] == "realm-management":
                management_id = value["id"]
                for role in admin.get_client_roles(management_id, True):
                    if role["name"] == "impersonation":
                        admin.assign_client_role(
                            user["id"],
                            management_id,
                            [{"id": role["id"], "name": "impersonation"}],
                        )
                        break
                else:
                    raise Exception()
                break
        else:
            raise Exception()

    return changed


def impersonate_user(app, keycloak_realm, username):
    data = get_keycloak(app)
    keycloak = KeycloakOpenID(**data)
    token = keycloak.token(grant_type=["client_credentials"])

    # ensure the user exists in keycloak (possibly newly created)
    for attempt_count in range(60):
        if keycloak_realm.get_user_id(username):
            break
    else:
        raise KeycloakError("No user found")

    return keycloak.exchange_token(
        token=token["access_token"],
        audience=keycloak.client_id,
        subject=username,
        subject_token_type="urn:ietf:params:oauth:token-type:access_token",
        requested_token_type="urn:ietf:params:oauth:token-type:access_token",
    )
