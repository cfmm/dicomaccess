import logging

import os

import pytest
import sqlalchemy as sa
import warnings

from itertools import chain

from mgmt.arc import ArcConfigurationError

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    from flask_security.utils import logout_user as logout

from ldap3 import MODIFY_REPLACE
from ldap3.core.results import RESULT_SUCCESS

from mgmt import create_app
from mgmt.models.credentials import User, Group, GroupMembership
from mgmt.models.dicom import Principal, Project, Operator
from mgmt.mrbs.models import Rate
from mgmt.arc_schema import update_schema
from flask_cfmm.helpers.mock_oidc import login

from dcm4chee.clients.rest import ArcRESTClient
from dcm4chee.fixtures import arc_config_service
from keycloak import KeycloakAdmin, KeycloakDeleteError
from cfmm_keycloak.configure import env_config
from cfmm_keycloak.keycloak import ConfigureKeycloak
from cfmm_keycloak.fixtures import (
    keycloak_service,
    samba_service,
)

from types import SimpleNamespace

from mgmt.models.credentials import (
    UserMembership,
    MembershipType,
)
from mgmt.models.dicom import Server
from mgmt.models.screening import ScreeningForm

import mgmt.mrbs.entities

from flask_cfmm.datastore import create_user
from .helpers import register_client


__all__ = (logout, login, keycloak_service, samba_service, arc_config_service)

mapping = {
    "TITLE": "StudyMgmt Test",
    "KEYCLOAK_META_URL": f"{os.environ['AUTH_SERVER_URL']}/realms/{os.environ['REALM_NAME']}"
    f"/.well-known/openid-configuration",
    "KEYCLOAK_CLIENT_ID": "MGMTTestClient",
    "KEYCLOAK_CLIENT_SECRET": "11111111-2222-3333-4444-555555555555",
    "CFMM_ADMIN_ROLE": "Admin",
    "CFMM_ACCESS_ROLE": "StudyManagement",
    "CFMM_DATACARE_ROLE": "Datacare",
    "LDAPQUERY": {
        "servers": [f"ldap://{env_config.samba.host}:389"],
        "bind_dn": env_config.samba.user,
        "bind_auth": env_config.samba.password,
        "base": env_config.samba.base,
        "user_base": env_config.samba.user_base,
        "local_base": env_config.samba.managed_users,
        "group_base": env_config.samba.group_base,
    },
    "SCREENING": {
        "coordinator_emails": ["coordinator@mgmt"],
        "datacare_emails": ["datacare@mgmt"],
        "contact_email": "mgmt@mgmt",
        "token_expiry": 600,
        "email_from": "stduymgmt@mgmt",
    },
    "USERMGMT": {
        "prefix": env_config.samba.prefix,
        "user_prefix": env_config.samba.user_prefix,
        "dicom_ou": env_config.samba.dicom_ou,
        "group_ou": env_config.samba.managed_ou,
    },
    "DCM4CHEE5": [
        {
            "url": f"{os.environ['ARC_URL']}/{env_config.arc.device}",
        }
    ],
    "SQLALCHEMY_ECHO": False,
    "RATELIMIT_STORAGE_URI": "redis://redis.mgmt:6379/1",
    "RATELIMIT_ENABLED": False,
    "RATELIMIT_DEFAULT": "80000/day;500/hour;120/minute;20/second",
    "RATELIMIT_STRATEGY": "fixed-window",
    "RATELIMIT_STORAGE_OPTIONS": {"socket_connect_timeout": 30},
    "SQLALCHEMY_DATABASE_URI": "postgresql+psycopg://mgmt:secret@postgresql.mgmt:5432/mgmt",
    "DEBUG": True,
    "SECRET_KEY": "DEVELOPMENT_PUBLIC_SECRET_KEY",
    "CELERY_BROKER_URL": "redis://redis.mgmt:6379/0",
    "CELERY_RESULT_BACKEND": "redis://redis.mgmt:6379/0",
    "CELERY_TASK_ALWAYS_EAGER": False,
    "SESSION_TIMEOUT": 30,
    "TIMEZONE": "America/Toronto",
    "LINK_LIST": [{"url": "https://mrbs.mgmt", "text": "Scheduler"}],
    "MAIL_SERVER": "mail",
    "STATIC_URL_PATH": None,
    "DOCUMENTATION": {
        "path": "./sample-docs/sample_docs",
        "public": [
            "public",
            "accessing_data",
            "authentication",
        ],
    },
}


@pytest.fixture(scope="session")
def realm_config(samba_service):  # noqa F811
    config = samba_service.data

    config["ldap_url"] = f"ldap://{os.environ['SAMBA_HOST']}"
    config["roles"] = {
        "dcm4chee_access": os.getenv("AUTH_USER_ROLE", "arc-users"),
        "dcm4chee_reload": os.getenv("RELOAD_USER_ROLE", "arc-reload"),
        "dcm4chee_admin": os.getenv("SUPER_USER_ROLE", "arc-admin"),
        "mgmt_access": "StudyManagement",
        "coordinator": "Coordinators",
        "datacare": "Datacare",
        "admin": "Admin",
    }
    config["name"] = os.environ["REALM_NAME"]
    config["arc_client"] = os.environ["RS_CLIENT_ID"]
    config["arc_url"] = mapping["DCM4CHEE5"][0]["url"]

    return config


@pytest.fixture(scope="session")
def arc(arc_config_service, keycloak_realm):
    # Update the schema
    update_schema(
        arc_config_service.server,
        arc_config_service.config.schema_dn,
        arc_config_service.config.schema_pass,
    )

    if not hasattr(arc_config_service.config, "port"):
        arc_config_service.config.port = int(os.getenv("DICOM_PORT", "11112"))
    if not hasattr(arc_config_service.config, "archive_host"):
        arc_config_service.config.archive_host = os.getenv("ARCHIVE_HOST", "localhost")

    arc_config_service.ae_titles()
    arc_config_service.network(
        keycloak_uri=keycloak_realm.connection.server_url,
        realm_name=keycloak_realm.realm_config["name"],
    )
    arc_config_service.ui()
    arc_config_service.webapp()

    return arc_config_service


@pytest.fixture(scope="session")
def arc_rest(arc_config_service, keycloak_realm) -> ArcRESTClient:
    yield ArcRESTClient(
        arc_url=keycloak_realm.realm_config["arc_url"],
        auth_url=keycloak_realm.connection.server_url,
        client_id=keycloak_realm.realm_config["arc_client"],
        realm_name=keycloak_realm.realm_config["name"],
        aetitle=arc_config_service.config.aetitle,
    )


@pytest.fixture(scope="session")
def keycloak_realm(keycloak_service, samba_service, realm_config):  # noqa F811
    samba_service.make_core()

    realm_name = realm_config["name"]
    username = os.environ["KEYCLOAK_ADMIN"]
    password = os.environ["KEYCLOAK_ADMIN_PASSWORD"]

    config = ConfigureKeycloak(
        keycloak_service,
        realm_name,
        realm_config,
        username=username,
        password=password,
        user_realm="master",
    )
    try:
        config.admin.delete_realm(realm_name)
    except KeycloakDeleteError:
        pass

    config.create_realm()

    arc_client = realm_config["arc_client"]
    arc_url = realm_config["arc_url"]
    config.make_clients(arc_url, arc_client)

    admin = KeycloakAdmin(
        server_url=keycloak_service,
        username=username,
        password=password,
        realm_name=realm_name,
        user_realm_name="master",
    )

    admin.__setattr__("realm_config", realm_config)
    yield admin


@pytest.fixture()
def mock_ldap(mocker):
    def search(*args, **kwargs):
        return True, {"type": "searchResDone"}, [], True

    def add(*args, **kwargs):
        return True, dict(result=RESULT_SUCCESS), None, None

    return dict(
        tls=mocker.patch("ldap3.Connection.start_tls", return_value=None),
        bind=mocker.patch("ldap3.Connection.bind", return_value=True),
        delete=mocker.patch("ldap3.Connection.delete", return_value=None),
        modify=mocker.patch("ldap3.Connection.modify", return_value=None),
        search=mocker.patch("ldap3.Connection.search", side_effect=search),
        add=mocker.patch("ldap3.Connection.add", side_effect=add),
        modify_dn=mocker.patch("ldap3.Connection.modify_dn", return_value=None),
    )


def reset_mock_ldap(data):
    data["tls"].reset_mock()
    data["bind"].reset_mock()
    data["delete"].reset_mock()
    data["modify"].reset_mock()
    data["search"].reset_mock()
    data["add"].reset_mock()
    data["modify_dn"].reset_mock()


def depopulate(application):
    with application.app_context():
        # Remove the client registration
        configurator = application.extensions["cfmm"].configuration
        configurator.remove("registration_token")
        configurator.remove("registration_token_sync")
        session = application.extensions["sqlalchemy"].session

        # Remove all the LDAP records
        ldap = application.extensions["ldap"]
        for item in chain(
            User.query.all(),
            Group.query.all(),
        ):
            ldap.remove(item)

        # Remove all the data
        for item in chain(
            ScreeningForm.query.all(),
            Server.query.all(),
            Project.query.all(),
            Principal.query.all(),
            Operator.query.all(),
            Rate.query.all(),
            User.query.all(),
            Group.query.all(),
        ):
            session.delete(item)
        session.commit()

        # Remove all the records from Arc instance
        try:
            application.extensions["arc"].purge()
        except ArcConfigurationError:
            pass


def populate_defaults(application):
    db = application.extensions["sqlalchemy"]

    with application.test_request_context():
        db.session.remove()
        db.metadata.drop_all(db.engine)
        db.metadata.create_all(db.engine)
        entities = mgmt.mrbs.entities.get_entities(db.metadata.schema)
        for entity in entities:
            db.session.execute(next(entity.to_sql_statement_create_or_replace()))

        db.session.execute(
            sa.text(
                "INSERT INTO mrbs_variables (variable_name, variable_content) VALUES ( 'db_version', '82');"
                "INSERT INTO mrbs_variables (variable_name, variable_content) VALUES ( 'local_db_version', '1');"
            )
        )

        ldap = application.extensions["ldap"]

        def add_user(username, password="secret"):
            user_ = create_user(
                username=username,
                fs_uniquifier=username,
                first_name=username,
                last_name="User",
                email=f"{username}@example.com",
            )
            db.session.add(user_)
            user_.update()
            ldap.conn.extend.microsoft.modify_password(user_.dn, password)
            ldap.conn.modify(
                user_.dn, {"userAccountControl": [(MODIFY_REPLACE, [544])]}
            )
            return user_

        application.add_user = add_user

        def add_group(group_name):
            group_ = Group(name=group_name)
            db.session.add(group_)
            group_.update(make_group=True)
            return group_

        rate = Rate(name="Default")
        db.session.add(rate)
        normal_user = add_user("user")
        operator = Operator(name="user", code="usr", user=normal_user)
        db.session.add(operator)
        user = add_user("principal1")
        principal = Principal(user=user, name="Principal1")
        db.session.add(principal)
        db.session.commit()
        project = Project(
            principal=principal,
            name="Project1",
            participant_description="Project1 description",
            study="STUDY_NAME",
            default_rate=rate,
            default_operator=operator,
        )
        db.session.add(project)

        admin_group = add_group("Admin")
        admin_user = add_user("admin")
        datacare_group = add_group("Datacare")
        datacare_user = add_user("datacare")

        schedule_admin_group = add_group("Schedule-Admin")
        db.session.add(UserMembership(group=schedule_admin_group, member=admin_user))

        schedule_group = add_group("Schedule")
        schedule_request_group = add_group("Schedule-Request")

        # role = UserRole(name="admin")
        # db.session.add(role)

        db.session.add(UserMembership(group=admin_group, member=admin_user))
        db.session.add(UserMembership(group=datacare_group, member=datacare_user))
        db.session.add(GroupMembership(group=datacare_group, member=admin_group))
        db.session.commit()
        db.session.add(
            GroupMembership(group=schedule_group, member=schedule_admin_group)
        )
        db.session.add(
            GroupMembership(group=schedule_request_group, member=schedule_group)
        )
        db.session.commit()


@pytest.fixture(scope="session")
def configured(keycloak_realm, arc):
    mapping.setdefault("TESTING", True)
    mapping.setdefault("WTF_CSRF_ENABLED", False)

    application = create_app(mapping=mapping)

    # Convenience for intercepting exception with breakpoint
    # noinspection PyUnusedLocal
    @application.errorhandler(Exception)
    def handle_exception(err):
        return err

    application.logger.setLevel(logging.CRITICAL)

    populate_defaults(application)

    register_client(application, keycloak_realm)

    yield application

    depopulate(application)


@pytest.fixture()
def mock_user(mocker):
    class MockUser(object):
        is_admin = True
        has_access = True
        is_datacare = True
        is_authenticated = True
        fs_uniquifier = 1

    user = MockUser()
    mocker.patch(
        "flask_login.utils._get_user",
        return_value=user,
    )
    return user


@pytest.fixture()
def client(configured):
    with configured.test_client() as client:
        yield client


@pytest.fixture(scope="function")
def query_env(configured):
    db = configured.extensions["sqlalchemy"]

    q = SimpleNamespace(db=db)

    q.add_member = (
        lambda user_, entity, level=MembershipType.full, expires=None: db.session.add(
            UserMembership(
                group_id=entity.group_id,
                member_id=user_.id,
                level=level,
                expires=expires,
            )
        )
    )

    with configured.app_context():
        q.users = [configured.add_user(f"test_users{i}") for i in range(5)]

        for user in q.users:
            user.update()

        db.session.commit()

        q.operator = db.session.get(Operator, 1)
        q.rate = db.session.get(Rate, 1)
        q.principals = [
            Principal(name=f"test_principal{i}", user=q.users[0]) for i in range(3)
        ]

        for x in q.principals + [q.operator, q.rate]:
            db.session.add(x)

        # commit to allow projects to be created subsequently and synced with ldap
        db.session.commit()

        q.projects = [
            [
                Project(
                    principal=q.principals[i],
                    name=f"test_principal{i}projects{j}",
                    default_operator=q.operator,
                    default_rate=q.rate,
                )
                for j in range(3)
            ]
            for i in range(len(q.principals))
        ]
        q.projects_list = [project for principal in q.projects for project in principal]

        for x in q.projects_list:
            db.session.add(x)
        db.session.commit()
        yield q

        # clean up in reverse order to avoid DB and LDAP errors
        for objects in [q.projects_list, q.principals]:
            for obj in objects:
                db.session.delete(obj)
            db.session.commit()
        for obj in q.users:
            obj.remove()
            db.session.delete(obj)
        db.session.commit()
