import time

import pytest
from mgmt.models.tokens import InvalidToken, TokenMixin
from flask_babel import gettext

kwargs = {"one": 1, "two": 2}

_object = None


class Token(TokenMixin):
    id = 1
    __tablename__ = "token"

    @classmethod
    def get(cls, object_id):
        return _object


def test_token_generation(configured):
    with configured.app_context():
        global _object
        _object = Token()

        purpose = "some_purpose"
        token, expiry = _object.get_token(purpose=purpose, **kwargs)

        # test purpose
        with pytest.raises(
            InvalidToken, match=gettext("Invalid token purpose")
        ), configured.test_request_context():
            _ = Token.verify_token(token, purpose="some_other_purpose")
        token_contents = Token.verify_token(
            token,
            purpose=purpose,
            **kwargs,
        )
        assert token_contents == dict(token=_object, **kwargs), "Token payload mismatch"


@pytest.mark.parametrize(
    "max_age,sleep_for,should_fail", [(0, 1, True), (10, 1, False), (2, 3, True)]
)
def test_token_expiry(configured, max_age, sleep_for, should_fail):
    with configured.app_context():
        global _object
        _object = Token()
        token, expiry = _object.get_token(max_age=max_age, **kwargs)
        time.sleep(sleep_for)

        def verify():
            Token.verify_token(token, max_age=max_age, **kwargs)

        if should_fail:
            with pytest.raises(
                InvalidToken, match=gettext("Invalid or expired token")
            ), configured.test_request_context():
                verify()
        else:
            verify()
