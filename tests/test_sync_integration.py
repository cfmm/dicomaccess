import pytest
import datetime
import sqlalchemy as sa

from ldap3.core.exceptions import LDAPNoSuchObjectResult

from mgmt.models.dicom import Principal, Project, Server, Destination, Operator
from mgmt.models.credentials import User, Group, UserMembership, GroupMembership
from mgmt.mrbs.models import Rate
from mgmt.proxy import current_ldap, current_arc
from unittest.mock import patch
from ldap3.core.results import RESULT_SUCCESS
from ldap3 import BASE, SUBTREE


def check_no_ldap_writes(configured, obj):
    def add(*args, **kwargs):
        return True, dict(result=RESULT_SUCCESS), None, None

    data = dict(
        delete=patch("ldap3.Connection.delete", return_value=None),
        modify=patch("ldap3.Connection.modify", return_value=None),
        add=patch("ldap3.Connection.add", side_effect=add),
        modify_dn=patch("ldap3.Connection.modify_dn", return_value=None),
    )
    for v in data.values():
        v.start()

    try:
        with configured.app_context():
            configured.extensions["arc"].update([obj])

        assert (
            data["add"].target.add.call_count == 0
        ), f"Unexpected add for {obj} update"
        assert (
            data["delete"].target.delete.call_count == 0
        ), f"Unexpected delete for {obj} update"
        assert (
            data["modify"].target.modify.call_count == 0
        ), f"Unexpected modify for {obj} update"
        assert (
            data["modify_dn"].target.modify_dn.call_count == 0
        ), f"Unexpected modify_dn for {obj} update"
    finally:
        for v in data.values():
            v.stop()


@pytest.fixture()
def db(configured):
    return configured.extensions["sqlalchemy"]


def check_ldap(configured, dn, exists=True):
    with configured.test_request_context():
        result = current_ldap.search(dn, BASE, "(objectClass=*)")
        if exists:
            assert len(result) > 0
        else:
            assert len(result) == 0


def get_store(configured, obj):
    with configured.test_request_context():
        store = list()
        for server in current_arc.servers:
            store.append(dict(server=server, dn=obj.mgmt_record(server).dn))

    return store


def check_arc(configured, store, exists=True, msg=None):
    with configured.test_request_context():
        for item in store:
            result = item["server"].search(
                item["dn"], "(objectClass=*)", search_scope=BASE
            )
            if exists:
                assert len(result) > 0, msg
            else:
                assert len(result) == 0, msg


def test_create_principal(configured, db):
    name = "PrincipalTEST"
    with configured.test_request_context():
        principal = Principal(name=name, user=User.query.order_by(User.id).first())

        db.session.add(principal)
        db.session.commit()
        dn = principal.group.dn
        obj_id = principal.id

    check_ldap(configured, dn, True)

    with configured.test_request_context():
        obj = db.session.get(Principal, obj_id)

        check_no_ldap_writes(configured, obj)

        db.session.delete(obj)
        db.session.commit()

    check_ldap(configured, dn, False)


def test_create_project(configured, db):
    name = "ProjectTEST"
    with configured.test_request_context():
        obj = Project(name=name, principal=Principal.query.first())

        db.session.add(obj)
        db.session.commit()
        dn = obj.group.dn
        obj_id = obj.id

    check_ldap(configured, dn, True)

    with configured.test_request_context():
        obj = db.session.get(Project, obj_id)

        check_no_ldap_writes(configured, obj)

        db.session.delete(obj)
        db.session.commit()

    check_ldap(configured, dn, False)


def test_create_group(configured, db):
    name = "GroupTEST"
    with configured.test_request_context():
        obj = Group(name=name)

        db.session.add(obj)
        db.session.commit()
        dn = obj.dn
        obj_id = obj.id

    check_ldap(configured, dn, True)

    with configured.test_request_context():
        obj = db.session.get(Group, obj_id)
        db.session.delete(obj)
        db.session.commit()

    check_ldap(configured, dn, False)


def test_create_server(configured, db):
    aetitle = "SERVER"
    host = "localhost"
    with configured.test_request_context():
        obj = Server(
            aet=aetitle, host=host, installed=True, owner_id=User.query.first().id
        )

        db.session.add(obj)
        db.session.commit()

        store = get_store(configured, obj)
        obj_id = obj.id

    check_arc(configured, store, True, "Missing Server")

    with configured.test_request_context():
        obj = db.session.get(Server, obj_id)
        obj.aet = aetitle + "_NEW"
        store2 = get_store(configured, obj)
        db.session.commit()

    check_arc(configured, store2, True, "Missing Server with new AE Title")
    check_arc(configured, store, False, "Server with original AE Title still exists")

    with configured.test_request_context():
        obj = db.session.get(Server, obj_id)
        check_no_ldap_writes(configured, obj)
        db.session.delete(obj)
        db.session.commit()

    check_arc(configured, store2, False, "Server still exists with new AE Title")
    check_arc(configured, store, False, "Server still exists with original AE Title")


def test_clear_servers(configured, db):
    with configured.test_request_context():
        for server in Server.query.all():
            db.session.delete(server)
        db.session.commit()
        current_arc.sync()


def test_create_destination(configured, db):
    aetitle = "DESTINATION"
    host = "localhost"

    with configured.test_request_context():
        obj = Server(
            aet=aetitle, host=host, installed=True, owner_id=User.query.first().id
        )

        db.session.add(obj)

        obj = Destination(server=obj, project_id=Project.query.first().id)
        db.session.add(obj)
        db.session.commit()

        store = get_store(configured, obj)
        obj_id = obj.id
        server_id = obj.server_id

    check_arc(configured, store, True)

    with configured.test_request_context():
        dst = db.session.get(Destination, obj_id)
        srv = db.session.get(Server, server_id)

        check_no_ldap_writes(configured, dst)
        check_no_ldap_writes(configured, srv)

        db.session.delete(dst)
        db.session.delete(srv)
        db.session.commit()

    check_arc(configured, store, False)


def test_create_usermembership(configured, db):
    with configured.test_request_context():
        user = db.session.get(User, 1)
        group = db.session.get(Group, 1)

        member_dn = user.dn

        assert member_dn not in [x.lower() for x in current_ldap.members(group)]

        membership = UserMembership(
            member_id=user.id,
            group_id=group.id,
            expires=datetime.datetime.now(datetime.timezone.utc)
            + datetime.timedelta(minutes=1),
        )
        db.session.add(membership)
        db.session.commit()
        obj_id = membership.id

        # Check membership
        assert member_dn in [x.lower() for x in current_ldap.members(group)]
        obj = db.session.get(UserMembership, obj_id)
        db.session.delete(obj)
        db.session.commit()
        assert member_dn not in [x.lower() for x in current_ldap.members(group)]


def test_delete_usermembership(configured, db):
    with configured.app_context():
        user = configured.add_user("membertest")
        db.session.add(user)
        db.session.commit()
        membership = UserMembership(member_id=user.id, group_id=1)
        db.session.add(membership)
        db.session.commit()

    with configured.app_context():
        db.session.delete(user)
        db.session.commit()
        try:
            db.session.delete(membership)
            db.session.commit()
        except sa.orm.exc.ObjectDeletedError:
            pass


def test_create_groupmembership(configured, db):
    with configured.test_request_context():
        membership = GroupMembership(
            member_id=3,
            group_id=1,
            expires=datetime.datetime.now(datetime.timezone.utc)
            + datetime.timedelta(minutes=1),
        )
        db.session.add(membership)
        db.session.commit()
        obj_id = membership.id

    with configured.test_request_context():
        member_dn = db.session.get(Group, 3).dn
        group = db.session.get(Group, 1)
        assert member_dn in [x.lower() for x in current_ldap.members(group)]

    with configured.test_request_context():
        obj = db.session.get(GroupMembership, obj_id)
        db.session.delete(obj)
        db.session.commit()

    with configured.test_request_context():
        member_dn = db.session.get(Group, 3).dn
        group = db.session.get(Group, 1)
        assert member_dn not in [x.lower() for x in current_ldap.members(group)]


def test_samaccountname_sync(configured, db):
    with configured.test_request_context():
        project = Project(
            principal=db.session.get(Principal, 1),
            name="ProjectN",
            default_rate=db.session.get(Rate, 1),
            default_operator=db.session.get(Operator, 1),
        )

        invalid_samaccountname = "some-gibberish-value"

        try:
            current_ldap.conn.delete(
                dn=f"{project.group.path},{current_ldap.group_base}"
            )
        except LDAPNoSuchObjectResult:
            pass

        # create a group with an outdated sAMAccountName
        current_ldap.conn.add(
            dn=f"{project.group.path},{current_ldap.group_base}",
            object_class=[current_ldap.group_object_class, "top"],
            attributes={"sAMAccountName": invalid_samaccountname},
        )

        def search_for_entry():
            return current_ldap.search(
                base=current_ldap.group_base,
                scope=SUBTREE,
                query=f"(&(objectClass={current_ldap.group_object_class})(sAMAccountName={invalid_samaccountname}))",
            )

        res = search_for_entry()

        assert (
            len(res) == 1
            and res[0]["dn"].lower()
            == f"{project.group.path},{current_ldap.group_base}".lower()
        )
        # sync
        db.session.add(project)
        db.session.commit()

        # query for a group with old outdated sAMAccountName
        res = search_for_entry()
        assert len(res) == 0

        def search_for_project_entry():
            return current_ldap.search(
                base=f"{project.group.path},{current_ldap.group_base}",
                scope=BASE,
                query=f"(objectClass={current_ldap.group_object_class})",
                attr=[
                    "sAMAccountName",
                ],
            )

        res = search_for_project_entry()
        assert (
            len(res) == 1
            and res[0]["attributes"]["sAMAccountName"] == project.group.identifier
        )

        db.session.delete(project)
        db.session.commit()

        res = search_for_project_entry()
        assert len(res) == 0
