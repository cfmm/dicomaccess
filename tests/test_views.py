from sqlalchemy import inspect


def test_edit_form(client):
    for admin in client.application.extensions["admin"]:
        for view in admin._views:
            columns = list()
            if hasattr(view, "form_columns"):
                columns = view.form_columns
            elif hasattr(view, "model"):
                columns = [column.key for column in inspect(view.model).attrs]

            if not columns:
                continue

            with client.application.app_context():
                form = view.get_edit_form()
                for column in columns:
                    if not isinstance(column, str):
                        column = column.key
                    assert hasattr(form, column)
