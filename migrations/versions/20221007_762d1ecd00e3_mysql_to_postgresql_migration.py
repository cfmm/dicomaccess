# revision identifiers, used by Alembic.
revision = "762d1ecd00e3"
down_revision = None


def upgrade():
    # This revision is the state after migration from mysql to postgresql
    pass


def downgrade():
    # Cannot downgrade from a mysql to postgresql migration
    pass
