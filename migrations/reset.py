import flask_migrate
import sqlalchemy as sa
from mgmt.core import create_app

database_head_revision = "4fe2d5530347"
database_migration_revision = "0265180faa51"


def reset(application):
    # Destroy the database contents
    db = application.extensions["sqlalchemy"]
    with application.test_request_context():
        db.session.remove()
        # does nothing when the database has not been previously initialized
        try:
            # noinspection SqlResolve,SqlNoDataSourceInspection
            version = db.session.execute(
                sa.text("SELECT * FROM alembic_version")
            ).one_or_none()
            if version:
                flask_migrate.stamp(revision=database_head_revision)
        except sa.exc.ProgrammingError:
            db.session.remove()
        flask_migrate.downgrade(revision="base")
        flask_migrate.upgrade(revision=database_head_revision)
        flask_migrate.stamp(revision=database_migration_revision)
        flask_migrate.upgrade()


if __name__ == "__main__":
    config = {
        "SQLALCHEMY_DATABASE_URI": "postgresql+psycopg://mgmt:secret@postgresql.mgmt:5432/mgmt",
    }
    application = create_app(mapping=config)
    reset(application)
